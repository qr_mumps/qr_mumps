/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


#ifndef __QRM_COMMON_C_H__
#define __QRM_COMMON_C_H__

double qrm_swtime();
int qrm_glob_set_i4_c(const char *string, int val);
int qrm_glob_set_r4_c(const char *string, float val);
int qrm_glob_get_i4_c(const char *string, int *val);
int qrm_glob_get_r4_c(const char *string, float *val);
int qrm_glob_get_i8_c(const char *string, long long *val);

#define qrm_no_transp 'n'
#define qrm_transp 't'
#define qrm_conj_transp 'c'

int  qrm_init_c(int ncpu, int ngpu);
void qrm_finalize_c();

struct qrm_dscr_type_c{
  void          *h; 
};


enum icntl{ 
  qrm_ordering_=0,
  qrm_sing_,
  qrm_minamalg_,
  qrm_mb_,
  qrm_nb_,
  qrm_ib_,
  qrm_bh_,
  qrm_keeph_,
  qrm_rhsnb_};

enum rcntl{ 
  qrm_amalgthr_=0,
  qrm_mem_relax_,
  qrm_rd_eps_};

enum ords{
  qrm_auto=0,
  qrm_natural_,
  qrm_given_,
  qrm_colamd_,
  qrm_metis_,
  qrm_scotch_};

enum gstats{
  qrm_e_facto_flops_=0,
  qrm_e_nnz_r_,
  qrm_e_nnz_h_,
  qrm_facto_flops_,
  qrm_nnz_r_,
  qrm_nnz_h_,
  qrm_e_facto_mempeak_,
  qrm_rd_num_
};

enum yn{
  qrm_no_=0,
  qrm_yes_};


#define qrm_zone_c  (double _Complex)1.0 +(double _Complex)(0.0*_Complex_I)
#define qrm_zzero_c (double _Complex)0.0 +(double _Complex)(0.0*_Complex_I)
#define qrm_zmone_c (double _Complex)-1.0+(double _Complex)(0.0*_Complex_I)

#define qrm_cone_c  (float _Complex)1.0 +(float _Complex)(0.0*_Complex_I)
#define qrm_czero_c (float _Complex)0.0 +(float _Complex)(0.0*_Complex_I)
#define qrm_cmone_c (float _Complex)-1.0+(float _Complex)(0.0*_Complex_I)

#define qrm_done_c  (double)1.0
#define qrm_dzero_c (double)0.0
#define qrm_dmone_c (double)-1.0

#define qrm_sone_c  (float)1.0
#define qrm_szero_c (float)0.0
#define qrm_smone_c (float)-1.0

#define qrm_conj_transp_c 'c'
#define qrm_transp_c 't'
#define qrm_no_transp_c 'n'


int qrm_dscr_init_c(struct qrm_dscr_type_c *qrm_dscr_c);
int qrm_dscr_destroy_c(struct qrm_dscr_type_c *qrm_dscr_c);

int qrm_barrier_c();
int qrm_barrier_dscr_c(struct qrm_dscr_type_c *qrm_dscr_c);

void qrm_version_c(int *major, int *minor, int *patch);

#endif /* __QRM_COMMON_C_H__ */
