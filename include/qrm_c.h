/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


/*##############################################################################################*/
/** @file qrm_mumps.h
 * Header file for the C interface
 *
 */
/*##############################################################################################*/

#include <string.h>
#include "qrm_common_c.h"

struct zqrm_spmat_type_c{
  int              *irn, *jcn;
  double _Complex  *val;
  int               m, n, nz, sym;
  void             *h; 
};

struct zqrm_spfct_type_c{
  int            m;
  int            n;
  int            nz;
  int            sym;
  int           *cperm_in;
  int            icntl[20];
  float          rcntl[10];
  long long int  gstats[10];
  void          *h; 
};


int  zqrm_spmat_init_c(struct zqrm_spmat_type_c *qrm_spmat_c);
int  zqrm_spmat_destroy_c(struct zqrm_spmat_type_c *qrm_spmat_c);
int  zqrm_spfct_init_c(struct zqrm_spfct_type_c *qrm_spfct_c, struct zqrm_spmat_type_c *qrm_spmat_c);
int  zqrm_spfct_destroy_c(struct zqrm_spfct_type_c *qrm_spfct_c);
/* void zqrm_readmat_c(char *matfile, struct zqrm_spmat_type_c *qrm_spmat_c); */
int  zqrm_analyse_c(struct zqrm_spmat_type_c *qrm_spmat_c,
                    struct zqrm_spfct_type_c *qrm_spfct_c,
                    const char transp);
int  zqrm_analyse_async_c(struct qrm_dscr_type_c *qrm_dscr_c,
                          struct zqrm_spmat_type_c *qrm_spmat_c,
                          struct zqrm_spfct_type_c *qrm_spfct_c,
                          const char transp);
int  zqrm_factorize_c(struct zqrm_spmat_type_c *qrm_spmat_c,
                      struct zqrm_spfct_type_c *qrm_spfct_c,
                      const char transp);
int  zqrm_factorize_async_c(struct qrm_dscr_type_c *qrm_dscr_c,
                            struct zqrm_spmat_type_c *qrm_spmat_c,
                            struct zqrm_spfct_type_c *qrm_spfct_c,
                            const char transp);
int  zqrm_solve_c(struct zqrm_spfct_type_c *qrm_spfct_c, const char transp,
                  double _Complex *b, double _Complex *x, const int nrhs);
int  zqrm_apply_c(struct zqrm_spfct_type_c *qrm_spfct_c, const char transp,
                  double _Complex *b, const int nrhs);
int  zqrm_spfct_unmqr_c(struct zqrm_spfct_type_c *qrm_spfct_c, const char transp,
                        double _Complex *b, const int nrhs);
int  zqrm_spfct_orgqr_c(struct zqrm_spfct_type_c *qrm_spfct_c,
                        double _Complex *b, const int j, const int nrhs);
int  zqrm_spmat_mv_c(struct zqrm_spmat_type_c *qrm_spmat_c, const char transp,
                     const double _Complex alpha, double _Complex *x, 
                     const double _Complex beta, double _Complex *y, 
                     const int nrhs);
int  zqrm_spmat_nrm_c(struct zqrm_spmat_type_c *qrm_spmat_c, const char ntype, 
                   double *nrm);
int  zqrm_vecnrm_c(const double _Complex *x, const int n, const int nrhs, 
                   const char ntype, double *nrm);
int  zqrm_spbackslash_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *b, 
                        double _Complex *x, const int nrhs, const char transp);
int  zqrm_spfct_backslash_c(struct zqrm_spfct_type_c *qrm_spfct_c, double _Complex *b, 
                          double _Complex *x, const int nrhs, const char transp);
int  zqrm_spposv_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *b, 
                          double _Complex *x, const int nrhs);
int  zqrm_least_squares_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *b, 
                          double _Complex *x, const int nrhs, const char transp);
int  zqrm_min_norm_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *b, 
                          double _Complex *x, const int nrhs, const char transp);
int  zqrm_residual_norm_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *b, 
                          double _Complex *x, const int nrhs, double *nrm, const char transp);
int  zqrm_residual_orth_c(struct zqrm_spmat_type_c *qrm_spmat_c, double _Complex *r, 
                          const int nrhs, double *nrm, const char transp);
int  zqrm_spfct_trsm_c(struct zqrm_spfct_type_c *qrm_spfct_c, const char transp, double _Complex *b, 
                          double _Complex *x, const int nrhs);
int  zqrm_spfct_sytrs_c(struct zqrm_spfct_type_c *qrm_spfct_c, double _Complex *b, 
                          double _Complex *x, const int nrhs);


int  zqrm_spfct_set_i4_c (struct zqrm_spfct_type_c *qrm_spfct_c, const char *string, int val);
int  zqrm_spfct_set_r4_c (struct zqrm_spfct_type_c *qrm_spfct_c, const char *string, float val);
int  zqrm_spfct_get_i4_c (struct zqrm_spfct_type_c *qrm_spfct_c, const char *string, int *val);
int  zqrm_spfct_get_r4_c (struct zqrm_spfct_type_c *qrm_spfct_c, const char *string, float *val);
int  zqrm_spfct_get_i8_c(struct zqrm_spfct_type_c *qrm_spfct_c, const char *string, long long *val);
int  zqrm_spfct_get_schur_c(struct zqrm_spfct_type_c *qrm_spfct_c, double _Complex *s, int i, int j, int m, int n);

int  zqrm_spfct_get_schur_async_c(struct zqrm_spfct_type_c *qrm_spfct_c, void **hdls);

int  zqrm_spfct_get_r_c(struct zqrm_spfct_type_c *qrm_spfct_c, struct zqrm_spmat_type_c *qrm_spmat_c);

int  zqrm_spfct_get_cp_c(struct zqrm_spfct_type_c *qrm_spfct_c, int **cp);

int  zqrm_spfct_get_rp_c(struct zqrm_spfct_type_c *qrm_spfct_c, int **rp);
