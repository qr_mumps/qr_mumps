/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


#include "zqrm_c.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>

int main(){
  struct zqrm_spmat_type_c qrm_spmat;
  int i;
  double rnrm, onrm, anrm, bnrm, xnrm;
  int irn[10] = {1, 1, 1, 1, 2, 3, 3, 4, 4, 5};
  int jcn[10] = {1, 3, 4, 5, 2, 3, 5, 4, 5, 5};
  double _Complex val[10] = {53.0, 8.0,  4.0, 3.0, 10.0,
                              6.0, 8.0, 26.0, 5.0, 14.0};
  double _Complex b[5] = {108.0, 20.0, 66.0, 133.0, 117.0};
  double _Complex r[5] = {108.0, 20.0, 66.0, 133.0, 117.0};
  double _Complex xe[5] = {1.0, 2.0, 3.0, 4.0, 5.0};
  double _Complex x[5];
  
  qrm_init_c(-1, -1);

  /* initialize the matrix data structure */
  zqrm_spmat_init_c(&qrm_spmat);
  qrm_spmat.m   = 5;
  qrm_spmat.n   = 5;
  qrm_spmat.nz  = 10;
  qrm_spmat.irn = irn;
  qrm_spmat.jcn = jcn;
  qrm_spmat.val = val;
  qrm_spmat.sym = 1;

  zqrm_spposv_c(&qrm_spmat, b, x, 1);
  
  zqrm_residual_norm_c(&qrm_spmat, r, x, 1, &rnrm, 'n');
  zqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  zqrm_vecnrm_c(b, qrm_spmat.m, 1, '2', &bnrm);
  zqrm_spmat_nrm_c(&qrm_spmat, 'f', &anrm);
  
  printf("Expected result is x= 1.00000 2.00000 3.00000 4.00000 5.00000\n");
  printf("Computed result is x= ");
  for(i=0; i<5; i++){
    printf("%7.5f ",creal(x[i]));
    x[i] -= xe[i];
  }
  printf("\n");
  zqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  printf("Forward error            ||xe-x||  = %10.5e\n",xnrm);
  printf("Residual norm            ||A*x-b|| = %10.5e\n",rnrm);
  
  zqrm_spmat_destroy_c(&qrm_spmat);

  qrm_finalize_c();
  return 0;

}
