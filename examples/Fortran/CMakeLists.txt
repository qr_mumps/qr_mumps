#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT
##
## This file is part of qr_mumps.
##
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as
## published by the Free Software Foundation, either version 3 of
## the License, or (at your option) any later version.
##
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################

list(APPEND tgtfiles
  "qrm_symm_posdef_basic.F90"
  "qrm_symm_posdef_full.F90"
  "qrm_min_norm_basic.F90"
  "qrm_min_norm_full.F90"
  "qrm_least_squares_basic.F90"
  "qrm_least_squares_full.F90"
  "qrm_least_squares_async.F90"
  )


foreach(_tgtfile ${tgtfiles})
  foreach(_arith ${arithmetics})
    set(_atgtfile "${_arith}${_tgtfile}")
    get_filename_component(_atgt ${_atgtfile} NAME_WE)
    add_custom_command(
      OUTPUT ${_atgtfile}
      COMMAND ${PERL_EXECUTABLE} ${CMAKE_SOURCE_DIR}/cmake_modules/ariths.pl "${CMAKE_CURRENT_SOURCE_DIR}/${_tgtfile}" ${_arith} > ${_atgtfile}
      DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${_tgtfile}"
      )

    add_executable(${_atgt} ${_atgtfile})
    target_link_libraries(${_atgt} ${_arith}qrm)
    target_include_directories(${_atgt} PUBLIC "${CMAKE_BINARY_DIR}/include")
    target_include_directories(${_atgt} PUBLIC "${CMAKE_BINARY_DIR}/src/include")

    install(TARGETS ${_atgt} DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)
  endforeach()
endforeach()


