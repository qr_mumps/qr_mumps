!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!>  qrm_spfct_orgqr.F90
!! This file contains a subroutine for computing a Q mxn matrix
!! resulting from the QR factorization of a sprse matrix
!!
!! ##############################################################################################

#include "qrm_common.h"

!> This routine computes Q mxn matrix
!! resulting from the QR factorization of a sprse matrix

!! [in]     qrm_spfct a qrm_spfct_type data which contains the factorization result.
!!                        On output the original data will be unchanged.
!! [in,out]       q the Q matrix. A 2D array of leading dimension at least qrm_spfct%m . On output
!!                        it will contain Q

subroutine zqrm_spfct_orgqr2d(qrm_spfct, b, j, info)
  use zqrm_spmat_mod
  use zqrm_spfct_mod
  use qrm_error_mod
  use zqrm_analysis_mod
  use zqrm_factorization_mod
  use zqrm_solve_mod
  use zqrm_sdata_mod
  use qrm_dscr_mod
  implicit none

  type(zqrm_spfct_type), intent(inout) :: qrm_spfct
  complex(r64), target, intent(inout)  :: b(:,:)
  integer, optional, intent(in)        :: j
  integer, optional, intent(out)       :: info

  type(zqrm_sdata_type), allocatable   :: qrm_sdata_fwd(:)
  integer                              :: i, ij, j1, j2, f, nb, nrhs, n, gputh
  type(qrm_dscr_type)                  :: qrm_dscr
  
  ! error management
  integer                              :: err
  character(len=*), parameter          :: name='qrm_spfct_orgqr'

  err = 0

  __QRM_PRNT_DBG('("Entering the spfct_orgqr driver")')

  __QRM_IFPRESENT3(ij, j, 1)

  ! blocking to deal with multiple rhs
  call qrm_get(qrm_spfct, 'qrm_rhsnb', nb)
  nrhs = size(b,2)
  if(nb.le.0) nb = nrhs

  ! init the descriptor
  call qrm_get('qrm_gputh', gputh)
  if(min(nrhs,nb).ge.gputh) then
     call qrm_dscr_init(qrm_dscr)
  else
     call qrm_dscr_init(qrm_dscr, nocuda=.true.)
  end if

  allocate(qrm_sdata_fwd((nrhs-1)/nb+1))

  ! first, build the permutation
  if(.not.qrm_allocated(qrm_spfct%adata%rpermu)) then
     call qrm_alloc(qrm_spfct%adata%rpermu, qrm_spfct%m)
     j1=0
     j2=qrm_spfct%n
     do f=1, qrm_spfct%fdata%nfronts
        do i=1, qrm_spfct%fdata%front_list(f)%m
           if(i.le.qrm_spfct%fdata%front_list(f)%npiv) then
              j1=j1+1
              qrm_spfct%adata%rpermu(j1)=qrm_spfct%fdata%front_list(f)%rows(i)
           else if(i.gt.qrm_spfct%fdata%front_list(f)%ne) then
              j2=j2+1
              qrm_spfct%adata%rpermu(j2)=qrm_spfct%fdata%front_list(f)%rows(i)
           end if
        end do
     end do
  end if

  ! fill up b
  b = qrm_zzero
  do i=1, nrhs
     b(qrm_spfct%adata%rpermu(ij+i-1),i) = qrm_zone
  end do
  
  do i=1, (nrhs-1)/nb+1
     call zqrm_sdata_init(qrm_sdata_fwd(i), qrm_spfct, &
          b(:,(i-1)*nb+1:min(i*nb,nrhs)),              &  ! LHS
          b(:,(i-1)*nb+1:min(i*nb,nrhs)))                 ! RHS
     call zqrm_spfct_unmqr_async(qrm_dscr, qrm_spfct, qrm_no_transp, qrm_sdata_fwd(i))
  end do

  ! wait for all the tasks in the dscriptor
  call qrm_barrier(qrm_dscr, err)

  call qrm_dscr_destroy(qrm_dscr)

  do i=1, (nrhs-1)/nb+1
     call zqrm_sdata_destroy(qrm_sdata_fwd(i))
  end do

9999 continue
  if(present(info)) info = err
  return

end subroutine zqrm_spfct_orgqr2d




!> This routine computes Q mxn matrix
!! resulting from the QR factorization of a sprse matrix

!! [in]     qrm_spfct a qrm_spfct_type data which contains the factorization result.
!!                    On output the original data will be unchanged.
!! [in,out]         b A 1D array of leading dimension qrm_spfct%m. On output
!!                    it will contain one column of Q
!!
subroutine zqrm_spfct_orgqr1d(qrm_spfct, b, j, info)
  use zqrm_spfct_mod
  use qrm_error_mod
  use zqrm_utils_mod
  use zqrm_methods_mod, savesym => zqrm_spfct_orgqr1d
  implicit none

  type(zqrm_spfct_type), intent(inout) :: qrm_spfct
  complex(r64), target, intent(inout)  :: b(:)
  integer, optional, intent(in)        :: j
  integer, optional, intent(out)       :: info

  complex(r64), pointer                :: pntb(:,:)
  integer                              :: m, n
  ! error management
  integer                              :: err
  character(len=*), parameter          :: name='qrm_spfct_orgqr1d'

  m = size(b,1)
  pntb(1:m,1:1) => b(1:m)
  
  call zqrm_spfct_orgqr2d(qrm_spfct, pntb, info)

  return

end subroutine zqrm_spfct_orgqr1d
