#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT
##  
## This file is part of qr_mumps.
##  
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as 
## published by the Free Software Foundation, either version 3 of 
## the License, or (at your option) any later version.
##  
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##  
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################

list(APPEND common_sources
  "StarPU/qrm_starpu_common_mod.F90"
  "StarPU/starpu_f_wrappers.c"
  )

list(APPEND arith_sources
  "StarPU/qrm_starpu_mod.F90"
  "StarPU/qrm_starpu_perfmodel_mod.F90"
  "StarPU/qrm_starpu_perfmodel_funcs.F90"
  )
