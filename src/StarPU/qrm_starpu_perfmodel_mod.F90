!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################
  module zqrm_starpu_perfmodel_mod
  use iso_c_binding
#if defined (have_starpu)
  use zqrm_starpu_mod
  use fstarpu_mod


#if defined (have_simgrid) 
 ! Perfmodel

  ! Dense mat 
  type(c_ptr) :: zqrm_geqrt_pm   = c_null_ptr
  type(c_ptr) :: zqrm_gemqrt_pm  = c_null_ptr
  type(c_ptr) :: zqrm_tpqrt_pm   = c_null_ptr
  type(c_ptr) :: zqrm_tpmqrt_pm  = c_null_ptr
  type(c_ptr) :: zqrm_gemm_pm           = c_null_ptr
  type(c_ptr) :: zqrm_trsm_pm           = c_null_ptr
  type(c_ptr) :: zqrm_block_copy_pm     = c_null_ptr

  ! Sparse MF 
  type(c_ptr) :: zqrm_do_subtree_pm     = c_null_ptr
  type(c_ptr) :: zqrm_init_front_pm     = c_null_ptr
  type(c_ptr) :: zqrm_init_block_pm     = c_null_ptr
  type(c_ptr) :: zqrm_clean_block_pm    = c_null_ptr
  type(c_ptr) :: zqrm_clean_front_pm    = c_null_ptr
  type(c_ptr) :: zqrm_assemble_block_pm = c_null_ptr


 interface
      subroutine zqrm_geqrt_pm_params(task, pm_arg) bind(C)
       use iso_c_binding
       type(c_ptr), value            :: task
       type(c_ptr), value            :: pm_arg
     end subroutine zqrm_geqrt_pm_params

      subroutine zqrm_gemqrt_pm_params(task, pm_arg) bind(C)
       use iso_c_binding
       type(c_ptr), value            :: task
       type(c_ptr), value            :: pm_arg
     end subroutine zqrm_gemqrt_pm_params

      subroutine zqrm_tpqrt_pm_params(task, pm_arg) bind(C)
       use iso_c_binding
       type(c_ptr), value            :: task
       type(c_ptr), value            :: pm_arg
     end subroutine zqrm_tpqrt_pm_params

  end interface

  contains 

  	subroutine zqrm_starpu_init_perfmodel()

 	    use fstarpu_mod
    	use iso_c_binding
      implicit none

        character(len=:,kind=c_char), allocatable , target   :: p0,p1,p2,p3,p4,p5
        integer(kind=c_int), allocatable                     :: combi(:)
        integer(kind=c_int), allocatable                     :: combi1(:)
        integer(kind=c_int), allocatable                     :: combi2(:)
        type(c_ptr), allocatable, target                     :: pname(:)

        combi = [1,1,1,1,1,1]

        allocate(pname(1:6))

        p0 = C_CHAR_"mb"//C_NULL_CHAR
        pname(1) = c_loc(p0)
        p1 = C_CHAR_"nb"//C_NULL_CHAR
        pname(2) = c_loc(p1)
        p2 = C_CHAR_"ib"//C_NULL_CHAR
        pname(3) = c_loc(p2)
        p3 = C_CHAR_"flops"//C_NULL_CHAR
        pname(4) = c_loc(p3)
        p4 = C_CHAR_"stair"//C_NULL_CHAR
        pname(5) = c_loc(p4)
        p5 = C_CHAR_"where"//C_NULL_CHAR
        pname(6) = c_loc(p5)

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Initialize Perfmodel
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        zqrm_geqrt_pm = fstarpu_perfmodel_allocate()
        call fstarpu_perfmodel_add_symbol(zqrm_geqrt_pm, C_CHAR_"qrm_geqrt_perfmodel"//C_NULL_CHAR)
        call fstarpu_perfmodel_add_type(zqrm_geqrt_pm, FSTARPU_MULTIPLE_REGRESSION_BASED) 
        call fstarpu_perfmodel_set_nb_params(zqrm_geqrt_pm, size(combi) )
        call fstarpu_perfmodel_set_nb_combi(zqrm_geqrt_pm, 1)
        call fstarpu_perfmodel_add_parameters_name(zqrm_geqrt_pm, pname )
        ! call fstarpu_perfmodel_add_combinations(zqrm_geqrt_pm, combi, 0, size(combi) )
        call fstarpu_perfmodel_add_parameters(zqrm_geqrt_pm, C_FUNLOC(zqrm_geqrt_pm_params) )
        call fstarpu_codelet_perfmodel(zqrm_geqrt_cl, zqrm_geqrt_pm )

        zqrm_gemqrt_pm = fstarpu_perfmodel_allocate()
        call fstarpu_perfmodel_add_symbol(zqrm_gemqrt_pm, C_CHAR_"qrm_gemqrt_perfmodel"//C_NULL_CHAR)
        call fstarpu_perfmodel_add_type(zqrm_gemqrt_pm, FSTARPU_MULTIPLE_REGRESSION_BASED) 
        call fstarpu_perfmodel_set_nb_params(zqrm_gemqrt_pm, size(combi) )
        call fstarpu_perfmodel_set_nb_combi(zqrm_gemqrt_pm, 1)
        call fstarpu_perfmodel_add_parameters_name(zqrm_gemqrt_pm, pname )
        ! call fstarpu_perfmodel_add_combinations(zqrm_gemqrt_pm, combi, 0, size(combi) )
        call fstarpu_perfmodel_add_parameters(zqrm_gemqrt_pm, C_FUNLOC(zqrm_gemqrt_pm_params) )
        call fstarpu_codelet_perfmodel(zqrm_gemqrt_cl, zqrm_gemqrt_pm )

        zqrm_tpqrt_pm = fstarpu_perfmodel_allocate()
        call fstarpu_perfmodel_add_symbol(zqrm_tpqrt_pm, C_CHAR_"qrm_tpqrt_perfmodel"//C_NULL_CHAR)
        call fstarpu_perfmodel_add_type(zqrm_tpqrt_pm, FSTARPU_MULTIPLE_REGRESSION_BASED) 
        call fstarpu_perfmodel_set_nb_params(zqrm_tpqrt_pm, size(combi) )
        call fstarpu_perfmodel_set_nb_combi(zqrm_tpqrt_pm, 1)
        call fstarpu_perfmodel_add_parameters_name(zqrm_tpqrt_pm, pname )
        ! call fstarpu_perfmodel_add_combinations(zqrm_tpqrt_pm, combi, 0, size(combi) )
        call fstarpu_perfmodel_add_parameters(zqrm_tpqrt_pm, C_FUNLOC(zqrm_tpqrt_pm_params) )
        call fstarpu_codelet_perfmodel(zqrm_tpqrt_cl, zqrm_tpqrt_pm )


        zqrm_tpmqrt_pm = fstarpu_perfmodel_allocate()
        call fstarpu_perfmodel_add_symbol(zqrm_tpmqrt_pm, C_CHAR_"qrm_tpmqrt_perfmodel"//C_NULL_CHAR)
        call fstarpu_perfmodel_add_type(zqrm_tpmqrt_pm, FSTARPU_MULTIPLE_REGRESSION_BASED) 
        call fstarpu_perfmodel_set_nb_params(zqrm_tpmqrt_pm, 5)
        call fstarpu_perfmodel_set_nb_combi(zqrm_tpmqrt_pm, 1)
        call fstarpu_perfmodel_add_parameters_name(zqrm_tpmqrt_pm, pname )
        call fstarpu_perfmodel_add_combinations(zqrm_tpmqrt_pm, combi, 0, size(combi) )
        call fstarpu_perfmodel_add_parameters(zqrm_tpmqrt_pm, C_FUNLOC(zqrm_tpmqrt_pm_params) )
 

        return
        
  end subroutine zqrm_starpu_init_perfmodel
#endif
#endif
end module zqrm_starpu_perfmodel_mod
