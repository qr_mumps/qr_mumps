/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/

#if defined(have_starpu)

#include <starpu.h>
#include <starpu_cuda.h>
#include <starpu_profiling.h>
#include <limits.h>


// struct starpu_cluster_machine *starpu_f_cluster_machine()
// {
  // struct starpu_cluster_machine *clusters;
  // // clusters = starpu_cluster_machine(HWLOC_OBJ_SOCKET, 0);
  // // clusters = starpu_cluster_machine(HWLOC_OBJ_SOCKET,
  // //                                    STARPU_CLUSTER_TYPE, GNU_OPENMP_MKL,
  // //                                    0);
  // clusters = starpu_cluster_machine(HWLOC_OBJ_SOCKET, STARPU_CLUSTER_NB, 8,
  //                                 0);
//   return clusters;
// }


/* void starpu_f_uncluster_machine(struct starpu_cluster_machine *machine) */
/* { */
/*   // starpu_uncluster_machine(machine); */
/* } */


/* void starpu_f_get_buffer(void *buffers[], int num, void **A, int *m, int *n, int *lda) { */

/*   *A   = (void *)STARPU_MATRIX_GET_PTR(buffers[num]); */
/*   *m   = (int)STARPU_MATRIX_GET_NX(buffers[num]); */
/*   *n   = (int)STARPU_MATRIX_GET_NY(buffers[num]); */
/*   *lda = (int)STARPU_MATRIX_GET_LD(buffers[num]); */

/*   return; */

/* } */


/* void starpu_f_data_acquire_read(starpu_data_handle_t data) { */

/*   starpu_data_acquire(data, STARPU_R); */
  
/*   return; */

/* } */


/* int starpu_f_sched_ctx_create_c(int *workers, int nworkers, const char *name){ */
/*   unsigned ctx; */

/*   int i; */
  
/*   ctx = starpu_sched_ctx_create(workers, nworkers, name, STARPU_SCHED_CTX_POLICY_NAME, NULL, 0); */
  
/*   return (int)ctx; */
/* } */


/* void starpu_f_sched_ctx_set_context_c(int *ctx){ */

/*   starpu_sched_ctx_set_context((unsigned*)ctx); */

/* } */


/* void starpu_f_sched_ctx_display_workers_c(int ctx){ */

/*   starpu_sched_ctx_display_workers((unsigned)ctx, stderr); */

/* } */

/* void starpu_f_sched_ctx_delete_c(int ctx){ */

/*   starpu_sched_ctx_delete((unsigned)ctx); */

/* } */

/* void starpu_f_task_wait_for_all_in_ctx_c(int ctx){ */

/*   starpu_task_wait_for_all_in_ctx((unsigned)ctx); */

/* } */


/* /\* Partition the matrix in PARTS vertical slices *\/ */
/* struct starpu_data_filter qrm_bc_filter = */
  /* { */
    /* .filter_func = starpu_matrix_filter_vertical_block, */
  /* }; */


/* void * qrm_get_bc_filter_c(){ */
  /* return &qrm_bc_filter; */
/* } */


/* void qrm_filter_set_parts_c(struct starpu_data_filter *filter, int parts){ */
  /* filter->nchildren = parts; */
  /* return; */
/* } */


/* int starpu_f_data_is_partitioned_c(starpu_data_handle_t handle){ */
 /* return handle->partitioned; */
/* } */


unsigned qrm_get_nblockcolumns(STARPU_ATTRIBUTE_UNUSED struct starpu_data_filter *f,
                           starpu_data_handle_t handle)
{
  struct starpu_matrix_interface *data_interface = (struct starpu_matrix_interface *)
    starpu_data_get_interface_on_node(handle, STARPU_MAIN_RAM);	
  
  uint32_t bsize = (uint32_t) f->filter_arg;
  unsigned nc = (data_interface->ny-1)/bsize+1;
  return nc;
}

void qrm_matrix_filter_blockcolumns(void *father_interface,
                                    void *child_interface,
                                    STARPU_ATTRIBUTE_UNUSED struct starpu_data_filter *f,
                                    unsigned id,
                                    unsigned nchunks)
{
  struct starpu_matrix_interface *matrix_father = (struct starpu_matrix_interface *) father_interface;
  struct starpu_matrix_interface *matrix_child = (struct starpu_matrix_interface *) child_interface;

  uint32_t nx = matrix_father->nx;
  uint32_t ny = matrix_father->ny;
  size_t elemsize = matrix_father->elemsize;

  uint32_t bsize = (uint32_t) f->filter_arg;


  uint32_t child_nx, child_ny;
  size_t offset;

  child_nx = nx;
  child_ny = STARPU_MIN(bsize, ny-id*bsize);
	
  offset = id * bsize * matrix_father->ld * elemsize;
  /* printf("---> %d %d %d %d %zd %zd %d\n",id,bsize,child_nx,child_ny,offset,elemsize,matrix_father->ld); */
  /* update the child's interface */
  matrix_child->id        = matrix_father->id;
  matrix_child->nx        = child_nx;
  matrix_child->ny        = child_ny;
  matrix_child->elemsize  = elemsize;
  matrix_child->allocsize = matrix_child->nx * matrix_child->ny * elemsize;

  /* is the information on this node valid ? */
  if (matrix_father->dev_handle)
    {
      if (matrix_father->ptr)
        matrix_child->ptr = matrix_father->ptr + offset;
      matrix_child->ld = matrix_father->ld;
      matrix_child->dev_handle = matrix_father->dev_handle;
      matrix_child->offset = matrix_father->offset + offset;
    }
}


struct starpu_data_filter qrm_bc_filter =
  {
    .filter_func = qrm_matrix_filter_blockcolumns,
    .get_nchildren = qrm_get_nblockcolumns,
    .filter_arg = 128,
  };


void *qrm_get_bc_filter_c()
{
  return &qrm_bc_filter;
}

/* void qrm_bc_filter_set_bsize_c(int bsize){ */

  /* qrm_bc_filter.filter_arg = bsize; */
/* } */

void strtoptr(char *s, void **ptr){
  *ptr = s;
}

#if defined (have_cuda)
#include <starpu_heteroprio.h>
#include "cuda.h"
#include <starpu_cuda.h>

void qrm_heteroprio_init(unsigned ctx)
{


  /* |-----------|------|-----|-----|  */
  /* |           | Prio | CPU | GPU |  */
  /* |-----------|------|-----|-----|  */
  /* | CPU-only  |  0   |  0  |  /  |  */
  /* |-----------|------|-----|-----|  */
  /* | GEQRT     |  1   |  1  |  2  |  */
  /* | I-GEMQRT  |  2   |  2  |  1  |  */
  /* | O-GEMQRT  |  3   |  3  |  0  |  */
  /* |-----------|------|-----|-----|  */
  /* | TPQRT     |  1   |  1  |  2  |  */
  /* | I-TPMQRT  |  2   |  2  |  1  |  */
  /* | O-TPMQRT  |  3   |  3  |  0  |  */
  /* |-----------|------|-----|-----|  */

#if defined (starpu_ver14)
  // CPU uses 3 buckets and visits them in the natural order
  if (starpu_cpu_worker_get_count())
    {
      starpu_heteroprio_set_nb_prios(ctx, STARPU_CPU_WORKER, 3);
      // It uses direct mapping idx => idx
      unsigned idx;
      for(idx = 0; idx < 3; ++idx)
        {
          starpu_heteroprio_set_mapping(ctx, STARPU_CPU_WORKER, idx, idx);
          starpu_heteroprio_set_faster_arch(ctx, STARPU_CPU_WORKER, idx);
        }
    }
  
  if(starpu_cuda_worker_get_count()){
    // CUDA is enabled and uses 3 buckets
    starpu_heteroprio_set_nb_prios(ctx, STARPU_CUDA_WORKER, 3);

    // CUDA will first look at bucket 3
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_WORKER, 0, 3);
    // CUDA will then look at bucket 2
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_WORKER, 1, 2);
    // CUDA will finally look at bucket 1
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_WORKER, 2, 1);
    
    // For bucket 3 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_WORKER, 3);
    // And CPU is 25 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_WORKER, 3, 25.0f);
    
    // For bucket 2 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_WORKER, 2);
    // And CPU is 5 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_WORKER, 2, 5.0f);
    
    // For bucket 1 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_WORKER, 1);
    // And CPU is 1.5 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_WORKER, 1, 1.5f);
  }

#else
  
  // CPU uses 3 buckets and visits them in the natural order
  if (starpu_cpu_worker_get_count())
    {
      starpu_heteroprio_set_nb_prios(ctx, STARPU_CPU_IDX, 3);
      // It uses direct mapping idx => idx
      unsigned idx;
      for(idx = 0; idx < 3; ++idx)
        {
          starpu_heteroprio_set_mapping(ctx, STARPU_CPU_IDX, idx, idx);
          starpu_heteroprio_set_faster_arch(ctx, STARPU_CPU_IDX, idx);
        }
    }
  
  if(starpu_cuda_worker_get_count()){
    // CUDA is enabled and uses 3 buckets
    starpu_heteroprio_set_nb_prios(ctx, STARPU_CUDA_IDX, 3);
    // CUDA will first look at bucket 3
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_IDX, 0, 3);
    // CUDA will then look at bucket 2
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_IDX, 1, 2);
    // CUDA will finally look at bucket 1
    starpu_heteroprio_set_mapping(ctx, STARPU_CUDA_IDX, 2, 1);
    
    // For bucket 3 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_IDX, 3);
    // And CPU is 25 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_IDX, 3, 25.0f);
    
    // For bucket 2 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_IDX, 2);
    // And CPU is 5 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_IDX, 2, 5.0f);
    
    // For bucket 1 CUDA is the fastest
    starpu_heteroprio_set_faster_arch(ctx, STARPU_CUDA_IDX, 1);
    // And CPU is 1.5 times slower
    starpu_heteroprio_set_arch_slow_factor(ctx, STARPU_CPU_IDX, 1, 1.5f);
  }
#endif

}


void *qrm_get_hp_init_ptr(){
  return (void*) &qrm_heteroprio_init;
}


void qrm_pin_mem(void *ptr, size_t size){

  cudaError_t err;

  err = cudaHostRegister(ptr, size, cudaHostRegisterPortable);
  if(err!=cudaSuccess){
    printf("Error in pinning %d %zu\n",err, size);
  }

  
}


void qrm_unpin_mem(void *ptr){

  cudaError_t err;

  err = cudaHostUnregister(ptr);
  if(err!=cudaSuccess){
    printf("Error in unpinning %d\n",(int)err);
  }

  
}

void qrm_unpin_mem_info(void *ptr, int *info){

  cudaError_t err;

  err = cudaHostUnregister(ptr);
  if(err!=cudaSuccess){
    printf("Error in unpinning %d\n",(int)err);
    *info=err;
  } else {
    *info = 0;
  }

}

void qrm_starpu_pinalloc(void **ptr, size_t size, int *info){

  /* Option 1 */
  *info = starpu_malloc_flags(ptr, size, STARPU_MALLOC_PINNED);
  /* printf("allocpin %p %d\n",*ptr, *info); */

  /* Option 2 */
  /* cudaMallocManaged(ptr, size, cudaMemAttachGlobal); */

  /* Option 3 */
  /* cudaHostAlloc(ptr, size, cudaHostAllocPortable); */
  
  /* Option 4 */
  /* *ptr = (void*) malloc(size); */
  /* starpu_memory_pin(*ptr, size); */

}

void qrm_starpu_pinfree(void *ptr, size_t size, int *info){
  *info=0;
  /* Option 1 */
  /* printf("deallocpin %p\n",NULL); */
  *info = starpu_free_flags(ptr, size, STARPU_MALLOC_PINNED);
  /* Option 2 */

  /* Option 3 */
  
  /* Option 4 */
  /* starpu_memory_unpin(ptr, size); */
  /* free(ptr); */
}


#endif
#endif
