!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!!
!! This file contains a routine that computes the norm of a vector.
!!
!! ##############################################################################################


#include "qrm_common.h"

!> This subroutine computes the norm of multiple vectors
!!
!! [in]  vec   the input vectors
!!
!! [in]  n     the vector size
!!
!! [out] nrm   the output norms
!!
!! [in]  ntype the norm type. It can be one of these
!!             1  : 1-norm
!!             2  : 2-norm
!!             i  : inf-norm
!!
subroutine zqrm_vec_nrm2d(vec, n, ntype, nrm, info)

  use qrm_parameters_mod
  use qrm_string_mod
  use qrm_error_mod
  implicit none

  complex(r64), intent(in)                     :: vec(:,:)
  real(r64)                                 :: nrm(:)
  integer, intent(in)                       :: n
  character                                 :: ntype
  integer, optional                         :: info

  integer                                   :: i, j
  real(r64)                                 :: dznrm2

  ! error management
  integer                                   :: err
  character(len=*), parameter               :: name='qrm_vec_nrm'

  err = 0
  
  nrm = qrm_dzero

  if(qrm_str_tolower(ntype) .eq. 'i') then
     do j=1, size(vec,2)
        nrm(j) = maxval(abs(vec(:,j)))
     end do
  else if(qrm_str_tolower(ntype) .eq. '1') then
     do j=1, size(vec,2)
        nrm(j) = qrm_dzero
        do i=1, n
           nrm(j) = nrm(j) + abs(vec(i,j))
        end do
     end do
  else if(qrm_str_tolower(ntype) .eq. '2') then
     do j=1, size(vec,2)
        nrm(j) = dznrm2(n, vec(1,j), 1)
     end do
  else
     err = 15
     call qrm_error_print(err, name)
  end if
  
9999 continue
  if(present(info)) info = err
  return
  
end subroutine zqrm_vec_nrm2d



!> @brief This subroutine computes the norm of a vector
!!
!! @param[in]  vec   the input vector
!!
!! @param[in]  n     the vector size
!!
!! @param[out] nrm   the output norm
!!
!! @param[in]  ntype the norm type. It can be one of these
!!                  1  : 1-norm
!!                  2  : 2-norm
!!                  i  : inf-norm
!!
subroutine zqrm_vec_nrm1d(vec, n, ntype, nrm, info)
  use qrm_parameters_mod
  use qrm_string_mod
  use qrm_error_mod
  implicit none

  complex(r64), intent(in)                  :: vec(:)
  real(r64)                                 :: nrm
  integer, intent(in)                       :: n
  character                                 :: ntype
  integer, optional                         :: info

  integer                                   :: i
  real(r64)                                 :: dznrm2

  ! error management
  integer                                   :: err
  character(len=*), parameter               :: name='qrm_vec_nrm'

  err = 0
  
  nrm = qrm_dzero

  if(qrm_str_tolower(ntype) .eq. 'i') then
     nrm = maxval(abs(vec))
  else if(qrm_str_tolower(ntype) .eq. '1') then
     nrm = qrm_dzero
     do i=1, n
        nrm = nrm + abs(vec(i))
     end do
  else if(qrm_str_tolower(ntype) .eq. '2') then
     nrm = dznrm2(n, vec(1), 1)
  else
     err = 15
     call qrm_error_print(err, name)
  end if
  
9999 continue
  if(present(info)) info = err
  return
  
end subroutine zqrm_vec_nrm1d
