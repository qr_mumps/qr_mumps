!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!> qrm_spfct_unmqr_clean_block.F90
!!
!! ##############################################################################################
subroutine zqrm_spfct_unmqr_clean_block(front, front_slv, transp, br, bc, lhs, info)
  use zqrm_spfct_mod
  use zqrm_fdata_mod
  use zqrm_sdata_mod
  use qrm_string_mod
  use zqrm_solve_mod, protect => zqrm_spfct_unmqr_clean_block
  implicit none

  type(zqrm_spfct_type) :: qrm_spfct
  type(zqrm_front_type) :: front
  type(zqrm_rhs_type)   :: front_slv
  character(len=*)      :: transp
  integer               :: br, bc
  complex(r64)          :: lhs(:,:)
  integer               :: info
  
  integer               :: mb, i, n, ii, row, brn
  logical               :: bottomup

  if(min(front%m,front%n).le.0) return
  
  bottomup = qrm_str_tolower(transp(1:1)) .eq. qrm_conj_transp


  ! mb = front_slv%rhs%mb
  n  = size(lhs,2)

  if (bottomup) then

     ! rowsbu: do i=1, min(size(front_slv%rhs%blocks(br,bc)%c,1),front%m-(br-1)*mb)
     rowsbu: do i=1, min(front%m+1,front_slv%rhs%f(br+1))-front_slv%rhs%f(br)
        ! row = (br-1)*mb+i
        row = front_slv%rhs%f(br)+i-1

        if (row.gt.front%npiv .and. row.le.front%ne) cycle rowsbu

        lhs(front%rows(row),front_slv%rhs%f(bc):min(front_slv%rhs%f(bc+1)-1,n)) = &
             front_slv%rhs%blocks(br,bc)%c(i,:)
                
     end do rowsbu
     
  else

     ! only arrows must be scattered back into the rhs. This can be
     ! made more efficient: rather than looping over all anrows we can
     ! loop over the block rows skipping those that are not in anrows
     rowstd: do i=1, front%anrows
        
        row = front%arowmap(i)
        ! brn = (row-1)/mb+1
        brn = zqrm_dsmat_inblock(front%f, row)

        if(brn.ne.br) cycle rowstd

        ! ii = row - (br-1)*mb
        ii = row - front%f%f(brn)+1
        
        ! write(21,*)'new ',front%num, br,bc,row,front%rows(row),front_slv%rhs%blocks(br,bc)%c(ii,:)
        lhs(front%rows(row),front_slv%rhs%f(bc):min(front_slv%rhs%f(bc+1)-1,n)) = &
             front_slv%rhs%blocks(br,bc)%c(ii,:)

     end do rowstd
  end if

  ! call qrm_dealloc(front_slv%rhs%blocks(br,bc)%c)
  
  return
end subroutine zqrm_spfct_unmqr_clean_block
