!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

subroutine zqrm_spfct_unmqr_clean_front(front, front_slv, lhs, transp, info)
  use qrm_dscr_mod
  use zqrm_fdata_mod
  use zqrm_sdata_mod
  use zqrm_solve_mod, protect => zqrm_spfct_unmqr_clean_front
  use qrm_string_mod
  implicit none

  type(zqrm_front_type) :: front
  type(zqrm_rhs_type)   :: front_slv
  complex(r64)          :: lhs(:,:)
  character(len=*)      :: transp
  integer               :: info
  
  logical               :: bottomup

  info = 0
  
  ! write(*,*)'cleaning front ', front%num
  ! bottomup = qrm_str_tolower(transp(1:1)) .eq. qrm_conj_transp

  ! if(bottomup .and. (front%m.gt.front%n)) p(front%rows(front%n+1:front%m),:) = qrm_zzero

  ! call zqrm_dsmat_destroy(front_slv%rhs)

  return
end subroutine zqrm_spfct_unmqr_clean_front
