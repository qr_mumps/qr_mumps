#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT
##  
## This file is part of qr_mumps.
##  
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as 
## published by the Free Software Foundation, either version 3 of 
## the License, or (at your option) any later version.
##  
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##  
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################

list(APPEND arith_sources
  "solve/qrm_spfct_trsm.F90"
  "solve/qrm_spfct_trsm_task_mod.F90"
  "solve/qrm_spfct_trsm_clean_block.F90"
  "solve/qrm_spfct_trsm_init_block.F90"
  "solve/qrm_spfct_trsm_clean_front.F90"
  "solve/qrm_spfct_trsm_init_front.F90"
  "solve/qrm_spfct_trsm_subtree.F90"
  "solve/qrm_spfct_trsm_activate_front.F90"
  "solve/qrm_spfct_trsm_assemble_front.F90"
  "solve/qrm_spfct_unmqr.F90"
  "solve/qrm_spfct_unmqr_task_mod.F90"
  "solve/qrm_spfct_unmqr_clean_block.F90"
  "solve/qrm_spfct_unmqr_init_block.F90"
  "solve/qrm_spfct_unmqr_clean_front.F90"
  "solve/qrm_spfct_unmqr_init_front.F90"
  "solve/qrm_spfct_unmqr_subtree.F90"
  "solve/qrm_spfct_unmqr_activate_front.F90"
  "solve/qrm_spfct_unmqr_assemble_front.F90"
  )
