!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!> qrm_clean_block.F90
!! This file contains a routine which performs the cleaning of a block in a front
!!
!!
!! ##############################################################################################


#include "qrm_common.h"


subroutine zqrm_clean_block(qrm_spfct, fnum, br, bc, info)
  use qrm_mem_mod
  use zqrm_spfct_mod
  use zqrm_fdata_mod
  use zqrm_utils_mod
  use qrm_common_mod
  use zqrm_factorization_mod, protect => zqrm_clean_block
#if defined (perfprof)
  use qrm_perfprof_mod
#endif

  implicit none

  type(zqrm_spfct_type), target  :: qrm_spfct
  integer                        :: fnum, br, bc
  integer, optional              :: info

  integer                        :: i, j, fc, fr, lc, lr
  integer                        :: mr, nr, mh, nh, pinth
  integer(kind=8)                :: nzr, nzh
  logical                        :: storeh, storer, inh, inr, inc
  integer(kind=8)                :: ts, te
  logical                        :: dopin

  complex(r64), allocatable      :: tmp(:,:)

  type(zqrm_front_type), pointer :: front

  ! error management
  integer                        :: err
  character(len=*), parameter    :: name='qrm_clean_block'

  __QRM_PROF_START(ts)

  err = 0

  front => qrm_spfct%fdata%front_list(fnum)

  ! TODO: error checking in allocs

  if( (front%n .le. 0) .or. (front%m .le. 0)) then
     goto 9999
  end if

  storer = qrm_spfct%icntl(qrm_keeph_).ge.0
  if(qrm_spfct%sym.gt.0) then
     storeh = .false.
  else
     storeh = qrm_spfct%icntl(qrm_keeph_).ge.1
  end if

  fc = front%f%f(bc)
  lc = front%f%f(bc+1)-1
  fr = front%f%f(br)
  lr = front%f%f(br+1)-1

  inr = storer .and. ((bc.ge.br) .and. (fr.le.front%npiv))
  inh = storeh .and. (bc.le.br)
  inc = (lc.gt.fr) .and. (lr.gt.front%npiv)

  if(inr) then

     mr  = min(size(front%f%blocks(br,bc)%c,1), front%npiv-fr+1)
     nr  = size(front%f%blocks(br,bc)%c,2)
     nzr = mr*nr

     if (fc.eq.fr) nzr=nzr- ((mr*(mr-1))/2)
     call qrm_atomic_add(front%rsize, +nzr)

  end if

  if(inh) then
     mh  = size(front%f%blocks(br,bc)%c,1)
     nh  = size(front%f%blocks(br,bc)%c,2)
     if(inc) then
        nzh = mh*(mh+1)/2
     else
        nzh = mh*nh
     end if
     call qrm_atomic_add(front%hsize, +nzh)

  end if

  if((.not.inh) .and. (.not.inr)) then

     ! destroy blocks
     call qrm_block_destroy(front%f%blocks(br,bc),                &
                            pin=front%f%pin,                      &
                            seq=front%small.ne.0,                 &
                            async=.true.)

     if(qrm_spfct%sym.eq.0) then
        if(br.ge.bc) call qrm_block_destroy(front%t%blocks(br,bc),&
                               pin=front%f%pin,                   &
                               seq=front%small.ne.0,              &
                               async=.true.)
        if(size(front%t%blocks,2).ge.front%np+bc) then
           call qrm_block_destroy(front%t%blocks(br,bc+front%np), &
                                  pin=front%f%pin,                &
                                  seq=front%small.ne.0,           &
                                  async=.true.)
        end if
     end if
  end if

9999 continue
  __QRM_PROF_STOP(qrm_prof_clean_block_task_,ts,te)

  if(present(info)) info = err
  return

end subroutine zqrm_clean_block
