!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!> @file qrm_factorize.F90
!! This file contains the main factorization driver
!!
!!
!! ##############################################################################################


#include "qrm_common.h"


!> @brief This routine is the main factorization driver
!!
!! @param[in,out] qrm_mat the problem containing the matrix to be factorized.
!!
!! @param[in] transp whether to factorize the input matrix or its
!!            transpose. Accepted values are qrm_conj_transp or qrm_no_transp
!!
subroutine zqrm_factorize(qrm_mat, qrm_spfct, transp, info)

  use zqrm_spmat_mod
  use zqrm_spfct_mod
  use qrm_error_mod
  use zqrm_factorization_mod, protect => zqrm_factorize
  use qrm_dscr_mod
  implicit none

  type(zqrm_spmat_type)           :: qrm_mat
  type(zqrm_spfct_type)           :: qrm_spfct
  character, optional, intent(in) :: transp
  integer, optional               :: info

  type(qrm_dscr_type)             :: qrm_dscr

  ! error management
  integer                         :: err
  character(len=*), parameter     :: name='qrm_factorize'

  __QRM_PRNT_DBG('("Entering the factorization driver")')

  ! init the descriptor
  call qrm_dscr_init(qrm_dscr)

  ! initialize the data for the facto
  call zqrm_factorize_async(qrm_dscr, qrm_mat, qrm_spfct, transp)

  ! wait for all the tasks in the dscriptor
  call qrm_barrier(qrm_dscr, info)

  qrm_spfct%gstats(qrm_rd_num_) = qrm_spfct%fdata%rd

  call qrm_dscr_destroy(qrm_dscr)

  return

end subroutine zqrm_factorize

!> @brief This routine is the main factorization driver
!!
!! @param[in,out] qrm_mat the problem containing the matrix to be factorized.
!!
!! @param[in] transp whether to factorize the input matrix or its
!!            transpose. Accepted values are qrm_conj_transp or qrm_no_transp
!!
subroutine zqrm_factorize_async(qrm_dscr, qrm_mat, qrm_spfct, transp)

  use zqrm_spmat_mod
  use zqrm_spfct_mod
  use qrm_error_mod
  use zqrm_factorization_mod, protect => zqrm_factorize_async
  use qrm_string_mod
  use qrm_dscr_mod
#if defined(have_starpu)
  use zqrm_starpu_mod
#endif
  implicit none

  type(qrm_dscr_type)             :: qrm_dscr
  type(zqrm_spmat_type), target   :: qrm_mat
  type(zqrm_spfct_type)           :: qrm_spfct
  character, optional, intent(in) :: transp

  character                       :: itransp
  integer, pointer                :: tmp(:)
  ! error management
  integer                         :: info
  character(len=*), parameter     :: name='qrm_factorize_async'

  if(qrm_dscr%info.ne.0) return
  info = 0

  ! immediately check if the analysis was done/submitted. Otherwise push an error and return
  if(allocated(qrm_spfct%adata)) then
     if(.not. qrm_spfct%adata%ok) then
        info = 13
        call qrm_error_print(info, name)
        goto 9999
     end if
  else
     info = 13
     call qrm_error_print(info, name)
     goto 9999
  end if

  ! make sure all the tasks on this matrix (e.g., the analysis) have completed
  call zqrm_spfct_sync(qrm_spfct)
  if(qrm_dscr%info.ne.0) return
  
  ! check all the control parameters are ok
  call zqrm_spfct_check(qrm_spfct, qrm_factorize_, info)
  __QRM_INFO_CHECK(info, name,'qrm_spfct_check',9999)

  if(present(transp)) then
     itransp = qrm_str_tolower(transp)
  else
     itransp = qrm_no_transp
  end if
 
  ! initialize the data for the facto
  call zqrm_factorization_init(qrm_dscr, qrm_mat, qrm_spfct, itransp)
  __QRM_INFO_CHECK(qrm_dscr%info, name,'qrm_factorization_init',9999)

  call zqrm_spfct_sync(qrm_spfct)

  call zqrm_factorization_core(qrm_dscr, qrm_spfct)
  __QRM_INFO_CHECK(qrm_dscr%info, name,'qrm_factorization_core',9999)

  ! the factorization was succesfully performed/submitted
  qrm_spfct%fdata%ok = .true.

9999 continue
  call qrm_error_set(qrm_dscr%info, info)
  return

end subroutine zqrm_factorize_async
