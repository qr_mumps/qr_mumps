/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_gemqrt.c
**
** @date    05-04-2011
** @author  Alfredo Buttari
** @version 0.0.1
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include <stdio.h>
#include <stdlib.h>

#define    C(i,j) (C  + (i) + (j)*ldc)
#define    T(i,j) (T  + (i) + (j)*ldt )
#define    V(i,j) (V  + (i) + (j)*ldv )


void  zqrm_cuda_gemqrt(char side, 
                       char trans,
                       int m, int n, int k, int ib,
                       int *stair, int i, 
                       const cuDoubleComplex *T,  int ldt,
                             cuDoubleComplex *C,  int ldc,
                             cuDoubleComplex *Wd, int lwd){


  cublasOperation_t cublasTrans;
  cublasSideMode_t cublasSide;
  int iib;
  
  iib = MIN(ib,k);

  if (trans == 'n'){
    cublasTrans = CUBLAS_OP_N;
  }else if(trans == 'c'){
    cublasTrans = CUBLAS_OP_C;
  }else if(trans == 't'){
    cublasTrans = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_cuda_gemqrt: trans can either be 'n' or 'c' %c\n", trans);
  }

  if (side == 'l'){
    cublasSide = CUBLAS_SIDE_LEFT;
  }else{
    fprintf(stderr, "Error in zqrm_cuda_gemqrt: side can only be 'l'  %c\n", side);
  }

  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  
  zqrm_cuda_gemqrt_gemm(cublas_handle,
                        CUBLAS_SIDE_LEFT, cublasTrans,
                        m-i+1, n, k, iib,
                        stair, i-1,
                        T+i+ib-1, ldt,
                        T, ldt,
                        C+i-1, ldc,
                        Wd, lwd);

/* #ifndef STARPU_CUDA_ASYNC */
  /* cudaStream_t      stream; */
  /* cublasGetStream(cublas_handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */
  
}


void  zqrm_cuda_gemqrt_gemm(cublasHandle_t handle,
                            cublasSideMode_t side, 
                            cublasOperation_t trans,
                            int m, int n, int k, int nb,
                            int *stair, int ofsv, 
                            const cuDoubleComplex *V,  int ldv,
                            const cuDoubleComplex *T,  int ldt,
                                  cuDoubleComplex *C,  int ldc,
                                  cuDoubleComplex *Wd, int lwd){


  
  int i, j, ik, bg, en, im, in, ne;

#if defined(zprec) || defined(cprec)
    cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
    cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
    cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
#else
    double zero = 0.0;
    double one  = 1.0;
    double mone = -1.0;
#endif

  cuDoubleComplex *W1;
  cuDoubleComplex *W2;
  int ldw1; 
  int ldw2;
  if(lwd >= nb*(m+n)) {
    W1 = Wd;       ldw1 = nb;
    W2 = Wd+nb*n;  ldw2 = m;
  } else {
    printf("GEMQRT_GEMM:  Not enough workspace on device  %d %d %d %d %d\n",lwd, nb*(m+n),nb,m,n);
    return;
  }

  cublasOperation_t transV, notransV;
  notransV = CUBLAS_OP_N;
  transV   = CUBLAS_OP_C;

  ne = MIN(m,k);
  
  if(side == CUBLAS_SIDE_LEFT) {
    /* printf("in gemqrt %d %d %d %d %d %d %d\n",m,n,k,nb,ldv,ldt,ldc); */

    if((trans==CUBLAS_OP_T) || (trans==CUBLAS_OP_C)){
      bg  = 0 ;
      en  = ne ;
      in  = nb;
    } else {
      bg  = ((ne-1)/nb) * nb ;
      en  = 0 ;
      in  = -nb;
    }

    for(j=bg;  (in < 0 ? j >= en : j < en); j+=in){
      ik = MIN(nb,ne-j);
      if(stair==NULL) {
        im = m-j;
      } else {
        im = MAX(MIN(stair[j+ik-1]-ofsv,m) - j,ik);
      }
      /* printf("\nW1=V'*C : %d %d %d\n",ik, n, im); */
      /* W1 = V'*C */
      cublasZgemm(handle,
                  transV, notransV,
                  ik, n, im,
                  &one,
                  V(j,j), ldv,
                  C(j,0), ldc,
                  &zero,
                  W1,     ik);
      
      if(m<=n) {

        /* printf("W2=V*T' : %d %d %d\n",im, ik, ik); */
        /* W2 = V T' */
        cublasZgemm(handle,
                    notransV, trans,
                    im, ik, ik,
                    &one,
                    V(j,j), ldv,
                    T(0,j), ldt,
                    &zero,
                    W2,     im);
       
        /* C = C - W2 W1 = C - V T V^H C = (I - V T V^H) C = H C */
        /* printf("C-=W2*W1 : %d %d %d\n",im, n, ik); */
        cublasZgemm(handle,
                    notransV, notransV,
                    im, n, ik,
                    &mone,
                    W2,     im,
                    W1,     ik,
                    &one,
                    C(j,0), ldc);
        
      } else {

        /* W2 = T'*W1 */
        /* printf("W2 = T'*W1 : %d %d %d\n",ik, n, ik); */
        cublasZgemm(handle,
                    trans, notransV,
                    ik, n, ik,
                    &one,
                    T(0,j),  ldt,
                    W1,      ik,
                    &zero,
                    W2,      ik);
       
        /* C = C - V W2 = C - V T V^H C = (I - V T V^H) C = H C */
        /* printf("C = C - V W2 : %d %d %d\n",im, n, ik); */
        cublasZgemm(handle,
                    notransV, notransV,
                    im, n, ik,
                    &mone,
                    V(j,j), ldv,
                    W2,     ik,
                    &one,
                    C(j,0), ldc);

      }
      

    }


  } else {
    printf("Side==r not supported\n");
  }

}
