!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

! this routine fills up a dense matrix with either random, zero or
! given value using the (i,j,m,n,l) scheme

subroutine zqrm_addi(a, lda, i, j, m, n, v)

  use qrm_parameters_mod
  
  implicit none

  integer        :: lda, i, j, m, n
  complex(r64)   :: a(lda,*)
  complex(r64)   :: v

  integer        :: k
  integer        :: iseed(4)=(/1,1,1,1/)


  do k=1, min(m,n)
     a(i+k-1,j+k-1) = a(i+k-1,j+k-1)+v
  end do

  return

end subroutine zqrm_addi
