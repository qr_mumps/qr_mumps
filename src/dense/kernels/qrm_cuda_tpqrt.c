/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "zqrm_cuda_kernels.h"
#include "mangling.h"

#define    A(i,j)  (A   + (i) + (j)*lda)
#define    B(i,j)  (B   + (i) + (j)*ldb)
#define    T(i,j)  (T   + (i) + (j)*ldt)
#define    W1(i,j) (W1  + (i) + (j)*ldw1)



void zqrm_cuda_tpqrt(int m, int n, int l, int nb,
                     int *stair, 
                     cuDoubleComplex *A,  int lda,
                     cuDoubleComplex *B,  int ldb,
                     cuDoubleComplex *T,  int ldt,
                     cuDoubleComplex *W,  int lw){

#if defined (starpu_ver14)
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  cusolverDnHandle_t cusolver_handle = starpu_cusolverDn_get_local_handle();

  zqrm_cuda_tpqrt_in(cublas_handle,
                     cusolver_handle,
                     m, n, l, nb,
                     stair,
                     A, lda,
                     B, ldb,
                     T, ldt,
                     W, lw);
#endif
}

#if defined (starpu_ver14)

void zqrm_cuda_tpqrt_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        int m, int n, int l, int nb,
                        int *stair, 
                        cuDoubleComplex *A,  int lda,
                        cuDoubleComplex *B,  int ldb,
                        cuDoubleComplex *T,  int ldt,
                        cuDoubleComplex *W,  int lw){
  
  int j, js, ib, im, info, ldw1, ldw2, lw23;

#if defined(zprec) || defined(cprec)
  cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
  cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
  cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
#else
  double zero = 0.0;
  double one  = 1.0;
  double mone = -1.0;
  double tmp;
#endif

  cuDoubleComplex *tau, *W1, *W2, *W3;

  
  cublasStatus_t    stat;
  cudaStream_t      stream; /* = starpu_cuda_get_local_stream();*/
  stat = cublasGetStream(cublas_handle, &stream);

  /* printf("cuda tpqrt %d %d %d %d\n",m,n,l,nb); */
  
  tau   = W;
  W1    = W  + nb;
  W2    = W1 + nb*(nb+m);
  W3    = W2 + nb*n;
  lw23  = lw - nb - nb*(nb+m);
  ldw1  = nb+m;

  /* set W1 and T to zero */
  cudaMemsetAsync(W1, 0, nb*(nb+m)*sizeof(cuDoubleComplex), stream);
  /* cudaMemsetAsync(T , 0, ldt*n    *sizeof(cuDoubleComplex), stream); */

  if (stair==NULL) {
    js = 0;
  } else {
    for ( js=0;js<n; js+=nb){
      if(stair[MIN(js+nb-1,n-1)] > 0) break;
    }
  }

  /* printf("cuda tpqrt %4d %4d %4d %4d %4d %10d %p\n",m,n,l,nb,js,lw23,stair); */
  
  for(j=js;  j<n; j+=nb){
    ib = MIN(n-j, nb);
    if(stair==NULL){
      im = MIN(m-l+j+ib,m);
    } else {
      im = MIN(stair[j+ib-1], m);
    }

    /* copy A and B in a temporary buffer */
    cudaMemcpy2DAsync( W1(0,0) , ldw1*sizeof(cuDoubleComplex),
                       A(j,j)  , lda*sizeof(cuDoubleComplex),
                       ib*sizeof(cuDoubleComplex), ib,
                       cudaMemcpyDeviceToDevice, stream);
    cudaMemcpy2DAsync( W1(nb,0), ldw1*sizeof(cuDoubleComplex),
                       B(0,j)  , ldb*sizeof(cuDoubleComplex),
                       im*sizeof(cuDoubleComplex), ib,
                       cudaMemcpyDeviceToDevice, stream);
 
    /* int Lwork; */
    /* cusolverDnZgeqrf_bufferSize(cusolver_handle, */
                                /* nb+im, */
                                /* nb, */
                                /* W1, */
                                /* ldw1, */
                                /* &Lwork ); */

    
    cusolverDnZgeqrf(cusolver_handle,
                     nb+im, ib,
                     W1(0,0), ldw1,
                     tau,
                     W2, lw23,
                     NULL);

    /* copy A and B back */
    cudaMemcpy2DAsync( A(j,j)  , lda*sizeof(cuDoubleComplex),
                       W1(0,0) , ldw1*sizeof(cuDoubleComplex),
                       ib*sizeof(cuDoubleComplex), ib,
                       cudaMemcpyDeviceToDevice, stream);
    cudaMemcpy2DAsync( B(0,j)  , ldb*sizeof(cuDoubleComplex),
                       W1(nb,0), ldw1*sizeof(cuDoubleComplex),
                       im*sizeof(cuDoubleComplex), ib,
                       cudaMemcpyDeviceToDevice, stream);

    cusolverDnZlarft(cusolver_handle,
                     'F', 'C',
                     nb+im, MIN(nb+im,ib),
                     W1(0,0) , ldw1,
                     tau,
                     T(0,j), ldt,
                     W2);
    
    /* cudaStreamSynchronize( stream ); */

    if(n-j-ib>0){

      cudaMemcpy2DAsync( W2,        ib   * sizeof(cuDoubleComplex),
                         A(j,j+ib), lda  * sizeof(cuDoubleComplex),
                         ib              * sizeof(cuDoubleComplex), n-j-ib,
                         cudaMemcpyDeviceToDevice, stream );


    /* TODO: check if it's OP_C instead of OP_T */
      cublasZgemm(cublas_handle, CUBLAS_OP_C, CUBLAS_OP_N,
                  ib, n-j-ib, im,
                  &one,
                  B(0,j)   , ldb,
                  B(0,j+ib), ldb,
                  &one,
                  W2, ib);

      /* W3 = V * op(T) */
      cublasZgemm( cublas_handle, CUBLAS_OP_N, CUBLAS_OP_C,
                   im, ib, ib,
                   &one,
                   B(0,j), ldb,
                   T(0,j), ldt,
                   &zero,
                   W3, im );
    
      /* A = A - opt(T) * W1 */
      cublasZgemm( cublas_handle, CUBLAS_OP_C, CUBLAS_OP_N,
                   ib, n-j-ib, ib,
                   &mone,
                   T(0,j), ldt,
                   W2, ib,
                   &one,
                   A(j,j+ib), lda );

    
      /* B = B - W3 * W2 */
      cublasZgemm( cublas_handle, CUBLAS_OP_N, CUBLAS_OP_N,
                   im, n-j-ib, ib,
                   &mone,
                   W3, im,
                   W2, ib,
                   &one,
                   B(0,j+ib), ldb );
    }    
  }

  /* cudaStreamSynchronize( stream ); */
  /* printf("cuda tpqrt done\n"); */
  /* cudaStreamSynchronize( stream ); */
  /* cudaStreamSynchronize( stream ); */
  /* cudaMemcpyAsync( &tmp , T(0,0)  , sizeof(cuDoubleComplex), */
                   /* cudaMemcpyDeviceToHost, stream); */
  
  /* printf("computed T  %f\n",tmp); */
  /* printf("outta here\n"); */
    
}


#endif
















/* ============================= old stuff ================================= */

/* #if 0 */


/* void zqrm_cuda_tpqrt(int m, int n, int l, int nb, int i, */
/*                      cuDoubleComplex *A,  int lda, */
/*                      cuDoubleComplex *B,  int ldb, */
/*                      cuDoubleComplex *T,  int ldt, */
/*                      cuDoubleComplex *Wd, int lwd){ */


/*   int iib = MIN(nb,n); */

/*   int lwh=(3*nb+m)*nb; */
/*   int size = lwh*sizeof(cuDoubleComplex); */
/*   cuDoubleComplex* Wh = (cuDoubleComplex*)malloc(size); */

/*   cublasHandle_t cublas_handle = starpu_cublas_get_local_handle(); */

/*   zqrm_cuda_tpqrt_gemm(cublas_handle, */
/*                        m, n, l, iib, */
/*                        A+i-1, lda, */
/*                        B, ldb, */
/*                        T, ldt, */
/*                        Wd, lwd, */
/*                        Wh, lwh); */
/*   free(Wh); */

/*   cudaStream_t      stream; */
/*   cublasGetStream(cublas_handle, &stream); */
/*   cudaStreamSynchronize( stream ); */

/* } */







/* void zqrm_cuda_tpqrt_nogemm(cublasHandle_t handle, */
/*                      int m, int n, int l, int nb, */
/*                      cuDoubleComplex *A,  int lda, */
/*                      cuDoubleComplex *B,  int ldb, */
/*                      cuDoubleComplex *T,  int ldt, */
/*                      cuDoubleComplex *Wd, int lwd, */
/*                      cuDoubleComplex *Wh, int lwh){ */

/*   int i, j, ik, ib, im, il, rows, info; */
/*   char Up='U'; */

/* #if defined(zprec) || defined(cprec) */
/*     cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0); */
/*     cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0); */
/*     cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0); */
/* #else */
/*     double zero = 0.0; */
/*     double one  = 1.0; */
/*     double mone = -1.0; */
/* #endif */
  
/*   /\* cuDoubleComplex one  = zqrm_one_c; *\/ */
/*   /\* cuDoubleComplex mone = zqrm_mone_c; *\/ */
/*   /\* cuDoubleComplex zero = zqrm_zero_c; *\/ */
  
/*   cublasStatus_t    stat; */
/*   cudaStream_t      stream; */
/*   stat = cublasGetStream(handle, &stream); */


/*   cuDoubleComplex *Th;  */
/*   cuDoubleComplex *Ah;  */
/*   cuDoubleComplex *Bh;  */

/*   int ldth;  */
/*   int ldah;   */
/*   int ldbh;  */
    
/*   if(lwh >= (3*nb+m)*nb) { */
/*     /\* Workspaces *\/ */
/*     Th = Wh+nb*nb; ldth = nb; /\* This is meant to store T on the host *\/ */
/*     Ah = Th+nb*nb; ldah = nb; /\* This is meant to store A on the host *\/ */
/*     Bh = Ah+nb*nb; ldbh = m;  /\* This is meant to store B on the host *\/ */
/*   } else { */
/*     printf("TPQRT_GEMM:  Not enough workspace on host\n"); */
/*     return; */
/*   } */
  
/*   if(lwd < nb*n) { */
/*     printf("TPQRT_GEMM:  Not enough workspace on device\n"); */
/*     return; */
/*   } */

/*   memset(Th, 0, nb*nb*sizeof(*Th)); */
     
/*   int old_i; */
/*   int old_ib; */
/*   int old_im; */
/*   int old_il; */
   
/*   for(i=0;  i<n; i+=nb){ */
/*     ib = MIN(n-i, nb); */
/*     im = MIN(m-l+i+ib,m); */
/*     il = i+1 >= l ? 0 : im-m+l-i; */
    
/*     if(i>0) { */
/*       /\* Apply old panel to current panel *\/ */
/*       /\* printf("Apply panel %d  to column %d -- %d %d %d %d %d\n",old_i, i, m-old_i, ib, old_ib, ldt, lda); *\/ */
/*       zqrm_cuda_tprfb(handle, */
/*                       CUBLAS_SIDE_LEFT, */
/*                       CUBLAS_OP_C, */
/*                       old_im, ib, old_ib, old_il, */
/*                       (const cuDoubleComplex *) B(0,old_i),  ldb, */
/*                       (const cuDoubleComplex *) T(0,old_i),  ldt, */
/*                             (cuDoubleComplex *) A(old_i,i),  lda, */
/*                             (cuDoubleComplex *) B(0,i),      ldb, */
/*                             (cuDoubleComplex *) Wd,          lwd); */
/*     } */

/*     /\* printf("Getting panel %d %d %d %d...",i,rows,ib,ldth); *\/ */
/*     stat = cublasGetMatrixAsync(ib, ib, sizeof(*A), */
/*                          A(i,i),  lda, */
/*                          Ah(0,0), ldah, stream); */

/*     stat = cublasGetMatrixAsync(im, ib, sizeof(*B), */
/*                          B(0,i),  ldb, */
/*                          Bh(0,0), ldbh, stream); */

/*     cudaStreamSynchronize( stream ); */
/*     /\* printf("got it\n"); *\/ */

/*     if(i==0) { /\* While doing the first panel, set T to zero *\/ */
/*       /\* FIXME: Maybe not necessary *\/ */
/*       stat = cudaMemsetAsync(T(0,0), 0, ldt*n*sizeof(*T), stream); */
/*     } else { */
/*       /\* While doing panel k update A(:,k+1...n) wrt panel k-1 *\/ */
/*       if(i+ib < n){ */
/*         /\* printf("Applying panel %d to trailing %d -- %d %d %d %d\n",old_i,i+ib, *\/ */
/*                /\* old_im, n-i-ib, old_ib, old_il); *\/ */
/*         zqrm_cuda_tprfb(handle, */
/*                         CUBLAS_SIDE_LEFT, */
/*                         CUBLAS_OP_C, */
/*                         old_im, n-i-ib, old_ib, old_il, */
/*                         (const cuDoubleComplex *) B(0,old_i),    ldb, */
/*                         (const cuDoubleComplex *) T(0,old_i),    ldt, */
/*                               (cuDoubleComplex *) A(old_i,i+ib), lda, */
/*                               (cuDoubleComplex *) B(0,i+ib),     ldb, */
/*                               (cuDoubleComplex *) Wd,            lwd); */
/*       } */
/*     } */

/*     int ii, jj; */
/*     /\* Panel is on the host, can factorize it *\/ */
/*     /\* printf("Factorizing panel %4d %4d %4d %4d\n",i,im,ib,il); *\/ */
/*     /\* for (ii = 0; ii < ib; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Ah[ii+nb*jj]); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* for (ii = 0; ii < im; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Bh[ii+m*jj] ); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* printf("\n"); *\/ */

/*     /\* nvtxRangeId_t id1 = nvtxRangeStartA("cpu_tpqrt"); *\/ */
/*     QRM_GLOBAL(ztpqrt,ZTPQRT)(&im, &ib, &il, &ib, */
/*                               Ah(0,0), &ldah, */
/*                               Bh(0,0), &ldbh, */
/*                               Th(0,0), &ldth, */
/*                               Wh,      &info); */
/*     /\* nvtxRangeEnd(id1); *\/ */

/*     /\* for (ii = 0; ii < ib; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Ah[ii+nb*jj]); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* for (ii = 0; ii < im; ii++) { *\/ */
/*       /\* for (jj = 0; jj < ib; jj++) { *\/ */
/*         /\* printf("%9.6f ",Bh[ii+m*jj] ); *\/ */
/*       /\* } *\/ */
/*       /\* printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* printf("\n"); *\/ */
    
/*     /\* Copy back to GPU *\/ */
/*     /\* printf("Copying diag block %d %d %d %d...\n",i,ib,ldth,ldt); *\/ */
/*     stat = cublasSetMatrixAsync(ib, ib, sizeof(*Th), */
/*                          Th(0,0), ldth, */
/*                          T(0,i), ldt, */
/*                          stream); */

/*     stat = cublasSetMatrixAsync(ib, ib, sizeof(*Ah), */
/*                          Ah(0,0), ldah, */
/*                          A(i,i),  lda, */
/*                          stream); */

/*     stat = cublasSetMatrixAsync(im, ib, sizeof(*Bh), */
/*                          Bh(0,0), ldbh, */
/*                          B(0,i),  ldb, */
/*                          stream); */

/*     /\* printf("done\n"); *\/ */
/*     old_i  = i; */
/*     old_ib = ib; */
/*     old_im = im; */
/*     old_il = il; */
    
/*   } */

/*   /\* cudaStreamSynchronize( stream ); *\/ */
  
/*   /\* printf("outta here\n"); *\/ */
  
    
/* } */

/* void zqrm_cuda_tpqrt_gemm(cublasHandle_t handle, */
/*                           int m, int n, int l, int nb, */
/*                           cuDoubleComplex *A,  int lda, */
/*                           cuDoubleComplex *B,  int ldb, */
/*                           cuDoubleComplex *T,  int ldt, */
/*                           cuDoubleComplex *Wd, int lwd, */
/*                           cuDoubleComplex *Wh, int lwh){ */

/*   int i, j, ik, ib, im, il, rows, info; */
/*   char Up='U'; */

/* #if defined(zprec) || defined(cprec) */
/*     cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0); */
/*     cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0); */
/*     cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0); */
/* #else */
/*     double zero = 0.0; */
/*     double one  = 1.0; */
/*     double mone = -1.0; */
/* #endif */
/*   /\* cuDoubleComplex one  = zqrm_one_c; *\/ */
/*   /\* cuDoubleComplex mone = zqrm_mone_c; *\/ */
/*   /\* cuDoubleComplex zero = zqrm_zero_c; *\/ */
  
/*   cublasStatus_t    stat; */
/*   cudaStream_t      stream; */
/*   stat = cublasGetStream(handle, &stream); */

/*   cuDoubleComplex *Th;  */
/*   cuDoubleComplex *Ah;  */
/*   cuDoubleComplex *Bh;  */

/*   int ldth;  */
/*   int ldah;   */
/*   int ldbh;  */
    
/*   if(lwh >= (3*nb+m)*nb) { */
/*     /\* Workspaces *\/ */
/*     Th = Wh+nb*nb; ldth = nb; /\* This is meant to store T on the host *\/ */
/*     Ah = Th+nb*nb; ldah = nb; /\* This is meant to store A on the host *\/ */
/*     Bh = Ah+nb*nb; ldbh = m;  /\* This is meant to store B on the host *\/ */
/*   } else { */
/*     printf("TPQRT_GEMM:  Not enough workspace on host\n"); */
/*     return; */
/*   } */
  
/*   if(lwd < nb*n) { */
/*     printf("TPQRT_GEMM:  Not enough workspace on device\n"); */
/*     return; */
/*   } */
  
/*   memset(Th, 0, nb*nb*sizeof(*Th)); */
     
/*   int old_i; */
/*   int old_ib; */
/*   int old_im; */
/*   int old_il; */
   
/*   for(i=0;  i<n; i+=nb){ */
/*     ib = MIN(n-i, nb); */
/*     im = MIN(m-l+i+ib,m); */
/*     il = i+1 >= l ? 0 : im-m+l-i; */
    
/*     if(i>0) { */
/*       /\* Apply old panel to current panel *\/ */
/*       /\* printf("Apply panel %d  to column %d -- %d %d %d %d %d\n",old_i, i, m-old_i, ib, old_ib, ldt, lda); *\/ */
/*       zqrm_cuda_tprfb_gemm(handle, */
/*                            CUBLAS_SIDE_LEFT, */
/*                            CUBLAS_OP_C, */
/*                            old_im, ib, old_ib, */
/*                            (const cuDoubleComplex *) B(0,old_i),  ldb, */
/*                            (const cuDoubleComplex *) T(0,old_i),  ldt, */
/*                                  (cuDoubleComplex *) A(old_i,i),  lda, */
/*                                  (cuDoubleComplex *) B(0,i),      ldb, */
/*                                  (cuDoubleComplex *) Wd,          lwd); */
/*     } */

/*     /\* printf("Getting panel %d %d %d %d...",i,rows,ib,ldth); *\/ */
/*     stat = cublasGetMatrixAsync(ib, ib, sizeof(*A), */
/*                          A(i,i),  lda, */
/*                          Ah(0,0), ldah, stream); */

/*     stat = cublasGetMatrixAsync(im, ib, sizeof(*B), */
/*                          B(0,i),  ldb, */
/*                          Bh(0,0), ldbh, stream); */

/*     cudaStreamSynchronize( stream ); */
/*     /\* printf("got it\n"); *\/ */

/*     if(i==0) { /\* While doing the first panel, set T to zero *\/ */
/*       /\* FIXME: Maybe not necessary *\/ */
/*       stat = cudaMemsetAsync(T(0,0), 0, ldt*n*sizeof(*T), stream); */
/*     } else { */
/*       /\* While doing panel k update A(:,k+1...n) wrt panel k-1 *\/ */
/*       if(i+ib < n){ */
/*         /\* printf("Applying panel %d to trailing %d -- %d %d %d %d\n",old_i,i+ib, *\/ */
/*                /\* old_im, n-i-ib, old_ib, old_il); *\/ */
/*         zqrm_cuda_tprfb_gemm(handle, */
/*                              CUBLAS_SIDE_LEFT, */
/*                              CUBLAS_OP_C, */
/*                              old_im, n-i-ib, old_ib, */
/*                              (const cuDoubleComplex *) B(0,old_i),    ldb, */
/*                              (const cuDoubleComplex *) T(0,old_i),    ldt, */
/*                                    (cuDoubleComplex *) A(old_i,i+ib), lda, */
/*                                    (cuDoubleComplex *) B(0,i+ib),     ldb, */
/*                                    (cuDoubleComplex *) Wd,            lwd); */
/*       } */
/*     } */

/*     int ii, jj; */
/*     /\* Panel is on the host, can factorize it *\/ */
/*     /\* printf("Factorizing panel %4d %4d %4d %4d\n",i,im,ib,il); *\/ */
/*     /\* for (ii = 0; ii < ib; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Ah[ii+nb*jj]); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* for (ii = 0; ii < im; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Bh[ii+m*jj] ); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* printf("\n"); *\/ */

/*     /\* nvtxRangeId_t id1 = nvtxRangeStartA("cpu_tpqrt"); *\/ */
/*     QRM_GLOBAL(ztpqrt,ZTPQRT)(&im, &ib, &il, &ib, */
/*            Ah(0,0), &ldah, */
/*            Bh(0,0), &ldbh, */
/*            Th(0,0), &ldth, */
/*            Wh     , &info); */
/*     /\* nvtxRangeEnd(id1); *\/ */

/*     /\* Set the coefficients below the trapezoidal part ob Bh to zero *\/ */
/*     for(jj=0; jj<il-1; jj++) */
/*       memset(Bh(im-il+jj+1,jj) , 0, (il-jj-1)*sizeof(*Bh)); */
    
/*     /\* for (ii = 0; ii < ib; ii++) { *\/ */
/*     /\*   for (jj = 0; jj < ib; jj++) { *\/ */
/*     /\*     printf("%9.6f ",Ah[ii+nb*jj]); *\/ */
/*     /\*   } *\/ */
/*     /\*   printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* for (ii = 0; ii < im; ii++) { *\/ */
/*       /\* for (jj = 0; jj < ib; jj++) { *\/ */
/*         /\* printf("%9.6f ",Bh[ii+m*jj] ); *\/ */
/*       /\* } *\/ */
/*       /\* printf("\n"); *\/ */
/*     /\* } *\/ */
/*     /\* printf("\n"); *\/ */
    
/*     /\* Copy back to GPU *\/ */
/*     /\* printf("Copying diag block %d %d %d %d...\n",i,ib,ldth,ldt); *\/ */
/*     stat = cublasSetMatrixAsync(ib, ib, sizeof(*Th), */
/*                          Th(0,0), ldth, */
/*                          T(0,i), ldt, */
/*                          stream); */

/*     stat = cublasSetMatrixAsync(ib, ib, sizeof(*Ah), */
/*                          Ah(0,0), ldah, */
/*                          A(i,i),  lda, */
/*                          stream); */

/*     stat = cublasSetMatrixAsync(im, ib, sizeof(*Bh), */
/*                          Bh(0,0), ldbh, */
/*                          B(0,i),  ldb, */
/*                          stream); */

/*     /\* printf("done\n"); *\/ */
/*     old_i  = i; */
/*     old_ib = ib; */
/*     old_im = im; */
/*     old_il = il; */
    
/*   } */

/*   /\* cudaStreamSynchronize( stream ); *\/ */
  
/*   /\* printf("outta here\n"); *\/ */
  
    
/* } */





/* /\* void test_tpqrt(int m, int n, int l, int nb){ *\/ */
/* /\*     cudaError_t cudaStat;     *\/ */
/* /\*     cublasStatus_t stat; *\/ */
/* /\*     cublasHandle_t handle; *\/ */
/* /\*     int i, j, info; *\/ */
    
/* /\*     double *gpua, *gpub, *gpuwd, *gpuwh, *gput; *\/ */
/* /\*     double *cpua, *cpub, *cpuw,  *cput; *\/ */
/* /\*     double ts, tcpu, tgpu; *\/ */

/* /\*     cpua  = (double *) malloc(n  * n * sizeof (*cpua)); *\/ */
/* /\*     cpub  = (double *) malloc(m  * n * sizeof (*cpub)); *\/ */
/* /\*     cput  = (double *) malloc(nb * n * sizeof (*cput)); *\/ */
/* /\*     cpuw  = (double *) malloc(nb * n * sizeof (*cpuw)); *\/ */


/* /\*     int ISEED[4]={0,0,0,1}, IONE=1; *\/ */

/* /\*     int size = n*n; *\/ */
/* /\*     dlarnv_(&IONE, ISEED, &size, cpua); *\/ */
/* /\*     size = m*n; *\/ */
/* /\*     dlarnv_(&IONE, ISEED, &size, cpub); *\/ */

/* /\*     for (i = 0; i < n; i++) { *\/ */
/* /\*       for (j = 0; j < i; j++) { *\/ */
/* /\*         cpua[i+j*n] = 0.0; *\/ */
/* /\*       } *\/ */
/* /\*     } *\/ */
/* /\*     for (j = 0; j < n; j++) { *\/ */
/* /\*       for (i = m-l+1+j; i < m; i++) { *\/ */
/* /\*         cpub[i+j*m] = 0.0; *\/ */
/* /\*       } *\/ */
/* /\*     } *\/ */

    
/* /\*     stat = cublasCreate(&handle); *\/ */

/* /\*     int lwd = nb*(m+n); *\/ */
/* /\*     int lwh = nb*(3*nb+m); *\/ */
    
/* /\*     cudaStat = cudaMalloc ((void**)&gpua,     n*n  * sizeof(*gpua)); *\/ */
/* /\*     cudaStat = cudaMalloc ((void**)&gpub,     m*n  * sizeof(*gpub)); *\/ */
/* /\*     cudaStat = cudaMalloc ((void**)&gput,     nb*n * sizeof(*gput)); *\/ */
/* /\*     cudaStat = cudaMalloc ((void**)&gpuwd,    lwd  * sizeof(*gpuwd)); *\/ */
/* /\*     cudaStat = cudaMallocHost((void**)&gpuwh, lwh  * sizeof(*gpuwh)); *\/ */

/* /\*     stat = cublasSetMatrix (n, n, sizeof(*cpua), cpua, n, gpua, n); *\/ */
/* /\*     stat = cublasSetMatrix (m, n, sizeof(*cpub), cpub, m, gpub, m); *\/ */

/* /\*     int ib = MIN(nb,n); *\/ */

/* /\*     /\\* printf("\n"); *\\/ *\/ */

/* /\*     /\\* for (i = 0; i < n; i++) { *\\/ *\/ */
/* /\*     /\\*   for (j = 0; j < n; j++) { *\\/ *\/ */
/* /\*     /\\*     printf("%9.6f ",cpua[i+n*j]); *\\/ *\/ */
/* /\*     /\\*   } *\\/ *\/ */
/* /\*     /\\*   printf("\n"); *\\/ *\/ */
/* /\*     /\\* } *\\/ *\/ */
/* /\*     /\\* for (i = 0; i < m; i++) { *\\/ *\/ */
/* /\*     /\\*   for (j = 0; j < n; j++) { *\\/ *\/ */
/* /\*     /\\*     printf("%9.6f ",cpub[i+m*j] ); *\\/ *\/ */
/* /\*     /\\*   } *\\/ *\/ */
/* /\*     /\\*   printf("\n"); *\\/ *\/ */
/* /\*     /\\* } *\\/ *\/ */
/* /\*     /\\* printf("\n"); *\\/ *\/ */
    

/* /\*     ts = secsf(); *\/ */
/* /\*     dtpqrt_(&m, &n, &l, &ib, *\/ */
/* /\*             cpua, &n, *\/ */
/* /\*             cpub, &m, *\/ */
/* /\*             cput, &nb, *\/ */
/* /\*             cpuw, &info); *\/ */
/* /\*     tcpu = secsf()-ts; *\/ */

/* /\*     /\\* for (i = 0; i < n; i++) { *\\/ *\/ */
/* /\*     /\\*   for (j = 0; j < n; j++) { *\\/ *\/ */
/* /\*     /\\*     printf("%9.6f ",cpua[i+n*j]); *\\/ *\/ */
/* /\*     /\\*   } *\\/ *\/ */
/* /\*     /\\*   printf("\n"); *\\/ *\/ */
/* /\*     /\\* } *\\/ *\/ */
/* /\*     /\\* for (i = 0; i < m; i++) { *\\/ *\/ */
/* /\*     /\\*   for (j = 0; j < n; j++) { *\\/ *\/ */
/* /\*     /\\*     printf("%9.6f ",cpub[i+m*j] ); *\\/ *\/ */
/* /\*     /\\*   } *\\/ *\/ */
/* /\*     /\\*   printf("\n"); *\\/ *\/ */
/* /\*     /\\* } *\\/ *\/ */
/* /\*     /\\* printf("\n"); *\\/ *\/ */
    

    
/* /\*     cudaDeviceSynchronize(); *\/ */
/* /\*     cudaProfilerStart(); *\/ */
/* /\*     ts = secsf(); *\/ */
/* /\*     zqrm_cuda_tpqrt_gemm(handle, *\/ */
/* /\*                          m, n, l, ib, *\/ */
/* /\*                          gpua, n, *\/ */
/* /\*                          gpub, m, *\/ */
/* /\*                          gput, nb, *\/ */
/* /\*                          gpuwd, lwd, *\/ */
/* /\*                          gpuwh, lwh); *\/ */
/* /\*     tgpu = secsf()-ts; *\/ */
/* /\*     cudaDeviceSynchronize(); *\/ */
/* /\*     cudaProfilerStop(); *\/ */

/* /\*     double *cchk; int ldchk; *\/ */

/* /\*     cchk  = (double *)malloc (n * n * sizeof (*cchk)); ldchk = n; *\/ */
/* /\*     stat = cublasGetMatrix (n, n, sizeof(*cpua), gpua, n, cchk, ldchk); *\/ */

/* /\*     double mx=0.0, mxv=0.0, diff; *\/ */
/* /\*     for (i = 0; i < n; i++) { *\/ */
/* /\*       for (j = 0; j < n; j++) { *\/ */
/* /\*           if(i<=j) { *\/ */
/* /\*             diff = fabs(fabs(cpua[i+j*n])-fabs(*(cchk(i,j)))); *\/ */
/* /\*             mx = MAX(mx,diff); *\/ */
/* /\*             /\\* printf("%8.6f ",diff); *\\/ *\/ */
/* /\*             mxv = MAX(mxv, fabs(cpua[i+j*n])); *\/ */
/* /\*           } else { *\/ */
/* /\*             /\\* printf("          "); *\\/ *\/ */
/* /\*           } *\/ */
/* /\*         } *\/ */
/* /\*         /\\* printf("\n"); *\\/ *\/ */
/* /\*     } *\/ */

    

/* /\*     printf ("TPQRT  --  Time CPU: %.6f   Time GPU: %.6f    Ratio: %5.2f   Diff: %10.4e\n", tcpu, tgpu, tcpu/tgpu, mx/mxv); *\/ */

/* /\*     free(cpua); *\/ */
/* /\*     free(cpub); *\/ */
/* /\*     free(cput); *\/ */
/* /\*     free(cpuw); *\/ */
/* /\*     cudaFree(gpua ); *\/ */
/* /\*     cudaFree(gpub ); *\/ */
/* /\*     cudaFree(gput ); *\/ */
/* /\*     cudaFree(gpuwd); *\/ */
/* /\*     cudaFreeHost(gpuwh); *\/ */


/* /\* } *\/ */


/* #endif */
