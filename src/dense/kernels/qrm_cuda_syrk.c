/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file zqrm_cuda_syrk.c
**
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include <stdio.h>
#include <stdlib.h>

void  zqrm_cuda_syrk(char uplo, char trans, 
                     int n, int k,
                     double *alpha,
                     const cuDoubleComplex *A, int lda, 
                     double *beta,
                     cuDoubleComplex *C, int ldc){

  cublasOperation_t cublasTrans;
  cublasFillMode_t  cublasUplo;
  
  if (trans == 'n'){
    cublasTrans = CUBLAS_OP_N;
  }else if(trans == 'c'){
    cublasTrans = CUBLAS_OP_C;
  }else if(trans == 't'){
    cublasTrans = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_cuda_syrk: trans can either be 'n' or 'c' %c\n", trans);
    return;
  }
  
  if(uplo == 'l'){
    cublasUplo = CUBLAS_FILL_MODE_LOWER;
  }else if(uplo == 'u'){
    cublasUplo = CUBLAS_FILL_MODE_UPPER;
  }
  
/* #if defined(dprec) || defined(sprec) */
  /* cublasZsyrk(*handle, */
              /* cublasUplo, cublasTrans, */
              /* n, k, */
              /* alpha, */
              /* A, lda, */
              /* beta, */
              /* C, ldc); */
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();

/* #elif defined(zprec) || defined(cprec) */
  cublasZsyrk(cublas_handle,
              cublasUplo, cublasTrans,
              n, k,
              alpha,
              A, lda,
              beta,
              C, ldc);
/* #endif */

/* #ifndef STARPU_CUDA_ASYNC */
  /* cudaStream_t      stream; */
  /* cublasGetStream(cublas_handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */
  
  
}
  
