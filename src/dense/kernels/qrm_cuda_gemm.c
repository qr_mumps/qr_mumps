/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_gemm.c
**
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include <stdio.h>
#include <stdlib.h>

#define    A(i,j) (A  + (i) + (j)*lda)
#define    B(i,j) (B  + (i) + (j)*ldb)


void  zqrm_cuda_gemm(char transa, char transb, 
                     int m, int n, int k,
                     cuDoubleComplex *alpha,
                     const cuDoubleComplex *A, int lda, 
                     const cuDoubleComplex *B, int ldb, 
                     cuDoubleComplex *beta,
                     cuDoubleComplex *C, int ldc){
  

  cublasOperation_t cublasTransa, cublasTransb;
  
  if (transa == 'n'){
    cublasTransa = CUBLAS_OP_N;
  }else if(transa == 'c'){
    cublasTransa = CUBLAS_OP_C;
  }else if(transa == 't'){
    cublasTransa = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_gemm_cuda_func: transa can either be 'n' or 'c' %c\n", transa);
    return;
  }
  
  if (transb == 'n'){
    cublasTransb = CUBLAS_OP_N;
  }else if(transb == 'c'){
    cublasTransb = CUBLAS_OP_C;
  }else if(transb == 't'){
    cublasTransb = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_gemm_cuda_func: transb can either be 'n' or 'c' %c\n", transb);
    return;
  }

  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  
  cublasZgemm(cublas_handle,
              cublasTransa, cublasTransb,
              m, n, k,
              alpha,
              A, lda,
              B, ldb,
              beta,
              C, ldc);

/* #ifndef STARPU_CUDA_ASYNC */
  /* printf("synch-ing!!!!!!!\n"); */
  /* cudaStream_t      stream; */
  /* cublasGetStream(*handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */
  
  
}
  
