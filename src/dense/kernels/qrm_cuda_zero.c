/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_gemm.c
**
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include <stdio.h>
#include <stdlib.h>

#define    A(i,j) (A  + (i) + (j)*lda)


void  zqrm_cuda_zero(int m, int n, 
                     cuDoubleComplex *A, int lda){
  

  
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  cudaStream_t   stream;
  cublasGetStream(cublas_handle, &stream);

  cudaMemset2DAsync (A, lda*sizeof(cuDoubleComplex),
                     0, m*sizeof(cuDoubleComplex), n, stream);


/* #ifndef STARPU_CUDA_ASYNC */
  /* printf("synch-ing!!!!!!!\n"); */
  /* cudaStream_t      stream; */
  /* cublasGetStream(*handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */
  
  
}
  
