#include <stdio.h>
#include <cuda_runtime.h>
#include "cublas_v2.h"
/* #include "zqrm_cuda_kernels.h" */
/* Mostly copy/pasted from MAGMA */


#define BLK_X 64
#define BLK_Y BLK_X

__host__ __device__
static inline int ceildiv( int x, int y )
{
    return (x + y - 1)/y;
}




static __global__
void zlastrow_device(
    int m, int n,
    cuDoubleComplex all, cuDoubleComplex last,
    cuDoubleComplex *A, int lda )
{
    int ind = blockIdx.x*BLK_X + threadIdx.x;
    int iby = blockIdx.y*BLK_Y;

    /* do only rows inside matrix */
    if ( ind < m ) {
        A += ind + iby*lda;
        for( int j=0; j < BLK_Y && iby+j < n; ++j ) {
          if ( ind == m-1)
            A[j*lda] = last;
          else
            A[j*lda] = all;
        }
    }
}



/******************************************************************************/
/*
    Divides matrix into ceil( m/BLK_X ) x ceil( n/BLK_Y ) blocks.
    Each block has BLK_X threads.
    Each thread loops across one row, updating BLK_Y entries.

    Code similar to zlaset, zlacpy, zlag2c, clag2z, zgeadd.
*/
static __global__
void zlaset_full_device(
    int m, int n,
    cuDoubleComplex offdiag, cuDoubleComplex diag,
    cuDoubleComplex *A, int lda )
{
    int ind = blockIdx.x*BLK_X + threadIdx.x;
    int iby = blockIdx.y*BLK_Y;
    /* check if full block-column && (below diag || above diag || offdiag == diag) */
#if defined(zprec)
    bool full = (iby + BLK_Y <= n && (ind >= iby + BLK_Y || ind + BLK_X <= iby || (cuCreal(offdiag)==cuCreal(diag) && cuCimag(offdiag)==cuCimag(diag)) ));
#elif defined(cprec)
    bool full = (iby + BLK_Y <= n && (ind >= iby + BLK_Y || ind + BLK_X <= iby || (cuCrealf(offdiag)==cuCrealf(diag) && cuCimagf(offdiag)==cuCimagf(diag)) ));
#else
    bool full = (iby + BLK_Y <= n && (ind >= iby + BLK_Y || ind + BLK_X <= iby || offdiag==diag ));
#endif
    /* do only rows inside matrix */
    if ( ind < m ) {
        A += ind + iby*lda;
        if ( full ) {
            // full block-column, off-diagonal block or offdiag == diag
            #pragma unroll
            for( int j=0; j < BLK_Y; ++j ) {
                A[j*lda] = offdiag;
            }
        }
        else {
            // either partial block-column or diagonal block
            for( int j=0; j < BLK_Y && iby+j < n; ++j ) {
                if ( iby+j == ind )
                    A[j*lda] = diag;
                else
                    A[j*lda] = offdiag;
            }
        }
    }
}


/******************************************************************************/
/*
    Similar to zlaset_full, but updates only the diagonal and below.
    Blocks that are fully above the diagonal exit immediately.

    Code similar to zlaset, zlacpy, zlat2c, clat2z.
*/
static __global__
void zlaset_lower_device(
    int m, int n,
    cuDoubleComplex offdiag, cuDoubleComplex diag,
    cuDoubleComplex *A, int lda )
{
    int ind = blockIdx.x*BLK_X + threadIdx.x;
    int iby = blockIdx.y*BLK_Y;
    /* check if full block-column && (below diag) */
    bool full = (iby + BLK_Y <= n && (ind >= iby + BLK_Y));
    /* do only rows inside matrix, and blocks not above diag */
    if ( ind < m && ind + BLK_X > iby ) {
        A += ind + iby*lda;
        if ( full ) {
            // full block-column, off-diagonal block
            #pragma unroll
            for( int j=0; j < BLK_Y; ++j ) {
                A[j*lda] = offdiag;
            }
        }
        else {
            // either partial block-column or diagonal block
            for( int j=0; j < BLK_Y && iby+j < n; ++j ) {
                if ( iby+j == ind )
                    A[j*lda] = diag;
                else if ( ind > iby+j )
                    A[j*lda] = offdiag;
            }
        }
    }
}


/******************************************************************************/
/*
    Similar to zlaset_full, but updates only the diagonal and above.
    Blocks that are fully below the diagonal exit immediately.

    Code similar to zlaset, zlacpy, zlat2c, clat2z.
*/
static __global__
void zlaset_upper_device(
    int m, int n,
    cuDoubleComplex offdiag, cuDoubleComplex diag,
    cuDoubleComplex *A, int lda )
{
    int ind = blockIdx.x*BLK_X + threadIdx.x;
    int iby = blockIdx.y*BLK_Y;
    /* check if full block-column && (above diag) */
    bool full = (iby + BLK_Y <= n && (ind + BLK_X <= iby));
    /* do only rows inside matrix, and blocks not below diag */
    if ( ind < m && ind < iby + BLK_Y ) {
        A += ind + iby*lda;
        if ( full ) {
            // full block-column, off-diagonal block
            #pragma unroll
            for( int j=0; j < BLK_Y; ++j ) {
                A[j*lda] = offdiag;
            }
        }
        else {
            // either partial block-column or diagonal block
            for( int j=0; j < BLK_Y && iby+j < n; ++j ) {
                if ( iby+j == ind )
                    A[j*lda] = diag;
                else if ( ind < iby+j )
                    A[j*lda] = offdiag;
            }
        }
    }
}




/***************************************************************************
    Purpose
    -------
    ZLASET initializes a 2-D array A to DIAG on the diagonal and
    OFFDIAG on the off-diagonals.

    Arguments
    ---------
    @param[in]
    uplo    magma_uplo_t
            Specifies the part of the matrix dA to be set.
      -     = MagmaUpper:      Upper triangular part
      -     = MagmaLower:      Lower triangular part
      -     = MagmaFull:       All of the matrix dA

    @param[in]
    m       INTEGER
            The number of rows of the matrix dA.  M >= 0.

    @param[in]
    n       INTEGER
            The number of columns of the matrix dA.  N >= 0.

    @param[in]
    offdiag COMPLEX_16
            The scalar OFFDIAG. (In LAPACK this is called ALPHA.)

    @param[in]
    diag    COMPLEX_16
            The scalar DIAG. (In LAPACK this is called BETA.)

    @param[in]
    dA      COMPLEX_16 array, dimension (LDDA,N)
            The M-by-N matrix dA.
            If UPLO = MagmaUpper, only the upper triangle or trapezoid is accessed;
            if UPLO = MagmaLower, only the lower triangle or trapezoid is accessed.
            On exit, A(i,j) = OFFDIAG, 1 <= i <= m, 1 <= j <= n, i != j;
            and      A(i,i) = DIAG,    1 <= i <= min(m,n)

    @param[in]
    ldda    INTEGER
            The leading dimension of the array dA.  LDDA >= max(1,M).

    @param[in]
    queue   magma_queue_t
            Queue to execute in.

    @ingroup magma_laset
*******************************************************************************/
extern "C"
void zqrm_cuda_laset(char uplo, int m, int n,
                     cuDoubleComplex offdiag, cuDoubleComplex diag,
                     cuDoubleComplex *dA, int ldda,
                     cudaStream_t stream)
{
    #define dA(i_, j_) (dA + (i_) + (j_)*ldda)

    /* int info = 0; */
    /* if ( uplo != 'l' && uplo != 'u' && uplo != 'f' ) */
    /*     info = -1; */
    /* else if ( m < 0 ) */
    /*     info = -2; */
    /* else if ( n < 0 ) */
    /*     info = -3; */
    /* else if ( ldda < max(1,m) ) */
    /*     info = -7; */

    if ( m == 0 || n == 0 ) {
        return;
    }

    /* assert( BLK_X == BLK_Y ); */

    dim3 threads( BLK_X, 1 );
    dim3 grid;
    if (uplo == 'L') {
      grid.x = ceildiv( m, BLK_X );
      grid.y = ceildiv( n, BLK_Y );
      zlaset_lower_device<<< grid, threads, 0, stream >>>
        ( m, n, offdiag, diag, dA, ldda );
    }
    else if (uplo == 'U') {
      grid.x = ceildiv( m, BLK_X );
      grid.y = ceildiv( n, BLK_Y );
      zlaset_upper_device<<< grid, threads, 0, stream >>>
        ( m, n, offdiag, diag, dA, ldda );
    }
    else {
      // if continuous in memory & set to zero, cudaMemset is faster.
      // TODO: use cudaMemset2D ?
        if ( m == ldda &&
#if defined(zprec)
             cuCreal(offdiag) == 0.0  &&  cuCimag(offdiag) == 0.0 &&
             cuCreal(diag)    == 0.0  &&  cuCimag(diag)    == 0.0 
#elif defined(cprec)
             cuCrealf(offdiag) == 0.0  &&  cuCimagf(offdiag) == 0.0 &&
             cuCrealf(diag)    == 0.0  &&  cuCimagf(diag)    == 0.0 
#else
             offdiag == 0.0  &&
             diag    == 0.0  
#endif
             )
        {
          size_t size = m*n;
          cudaError_t err = cudaMemsetAsync( dA, 0, size*sizeof(cuDoubleComplex), stream );
          /* assert( err == cudaSuccess ); */
        }
        else {
          grid.x = ceildiv( m, BLK_X );
          grid.y = ceildiv( n, BLK_Y );
          /* printf("--> %d %d %d %d\n",grid.x, grid.y, threads.x, threads.y); */
          zlaset_full_device<<< grid, threads, 0, stream >>>
            ( m, n, offdiag, diag, dA, ldda );
        }
    }
}


extern "C"
void zqrm_cuda_lastrow(int m, int n,
                       cuDoubleComplex all, cuDoubleComplex last,
                       cuDoubleComplex *dA, int ldda,
                       cudaStream_t stream)
{
    #define dA(i_, j_) (dA + (i_) + (j_)*ldda)

    /* int info = 0; */
    /* if ( uplo != 'l' && uplo != 'u' && uplo != 'f' ) */
    /*     info = -1; */
    /* else if ( m < 0 ) */
    /*     info = -2; */
    /* else if ( n < 0 ) */
    /*     info = -3; */
    /* else if ( ldda < max(1,m) ) */
    /*     info = -7; */

    if ( m == 0 || n == 0 ) {
        return;
    }

    /* assert( BLK_X == BLK_Y ); */

    dim3 threads( BLK_X, 1 );
    dim3 grid;
    grid.x = ceildiv( m, BLK_X );
    grid.y = ceildiv( n, BLK_Y );
    /* printf("--> %d %d %d %d\n",grid.x, grid.y, threads.x, threads.y); */
    zlastrow_device<<< grid, threads, 0, stream >>>
      ( m, n, all, last, dA, ldda );
}

