/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_tpmqrt.c
**
** @date    05-04-2011
** @author  Alfredo Buttari
** @version 0.0.1
**
**##############################################################################################*/

#include <stdio.h>
#include <stdlib.h>
#include "zqrm_cuda_kernels.h"

#define    A(i,j) (A  + (i) + (j)*lda)
#define    B(i,j) (B  + (i) + (j)*ldb)
#define    T(i,j) (T  + (i) + (j)*ldt )
#define    V(i,j) (V  + (i) + (j)*ldv )
#define    W(i,j) (W  + (i) + (j)*ldw )


void  zqrm_cuda_tpmqrt(char side, char trans,
                       int m, int n, int k, int l, int ib,
                       int *stair, int i,
                       const cuDoubleComplex *V,  int ldv,
                       const cuDoubleComplex *T,  int ldt,
                             cuDoubleComplex *A,  int lda,
                             cuDoubleComplex *B,  int ldb,
                             cuDoubleComplex *Wd, int lwd){

  cublasOperation_t cublasTrans;
  cublasSideMode_t cublasSide;
  int iib;
  
  iib = MIN(ib,k);

  if (trans == 'n'){
    cublasTrans = CUBLAS_OP_N;
  }else if(trans == 'c'){
    cublasTrans = CUBLAS_OP_C;
  }else if(trans == 't'){
    cublasTrans = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_tpmqrt_cuda_func: trans can either be 'n' or 'c' %c\n", trans);
    return;
  }


  if (side == 'l'){
    cublasSide = CUBLAS_SIDE_LEFT;
  }else{
    fprintf(stderr, "Error in zqrm_cuda_gemqrt: side can only be 'l'  %c\n", side);
  }

  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();

  zqrm_cuda_tpmqrt_gemm(cublas_handle,
                        CUBLAS_SIDE_LEFT, cublasTrans,
                        m, n, k, l, iib,
                        stair,
                        V, ldv,
                        T, ldt,
                        A+i-1, lda,
                        B, ldb,
                        Wd, lwd);

/* #ifndef STARPU_CUDA_ASYNC */
  /* cudaStream_t      stream; */
  /* cublasGetStream(cublas_handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */



  
}


void  zqrm_cuda_tpmqrt_gemm(cublasHandle_t handle,
                            cublasSideMode_t side, 
                            cublasOperation_t trans,
                            int m, int n, int k, int l, int nb,
                            int *stair,
                            const cuDoubleComplex *V,  int ldv,
                            const cuDoubleComplex *T,  int ldt,
                                  cuDoubleComplex *A,  int lda,
                                  cuDoubleComplex *B,  int ldb,
                                  cuDoubleComplex *Wd, int lwd){



  int i, j, ik, bg, en, in, ib, mb, lb, js;

  if(lwd < nb*n) {
    printf("TPMQRT_GEMM:  Not enough workspace on device\n");
    return;
  }
  

  if (stair==NULL) {
    js = 0;
  } else {
    for ( js=0;js<k; js+=nb){
      if(stair[MIN(js+nb,k-1)] > 0) break;
    }
  }
  
 /* TODO: skip empty panels */
  
  if(side == CUBLAS_SIDE_LEFT) {

    if((trans==CUBLAS_OP_T) || (trans==CUBLAS_OP_C)){
      bg  = js ;
      en  = k ;
      in  = nb;
    } else {
      bg  = ((k-1)/nb) * nb ;
      en  = js ;
      in  = -nb;
    }
    
    for(j=bg;  (in < 0 ? j >= en : j < en); j+=in){
      ib = MIN(nb, k-j);
      if(stair==NULL){
        mb = MIN(m-l+j+ib,m);
      } else {
        mb = MIN(stair[j+ib-1], m);
      }

      if(mb>0)
        zqrm_cuda_tprfb_gemm(handle,
                             side, trans,
                             mb, n, ib,
                             V(0,j),  ldv,
                             T(0,j),  ldt,
                             A(j,0),  lda,
                             B(0,0),  ldb,
                             Wd,      lwd);

    }

  } else {
    printf("Side==r not supported\n");
  }
  

}




void  zqrm_cuda_tprfb_gemm(cublasHandle_t handle,
                           cublasSideMode_t side, 
                           cublasOperation_t trans,
                           int m, int n, int k, 
                           const cuDoubleComplex *V,   int ldv,
                           const cuDoubleComplex *T,   int ldt,
                                 cuDoubleComplex *A,   int lda,
                                 cuDoubleComplex *B,   int ldb,
                                 cuDoubleComplex *Wd,  int lwd){




#if defined(zprec) || defined(cprec)
    cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
    cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
    cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
#else
    double zero = 0.0;
    double one  = 1.0;
    double mone = -1.0;
#endif
  /* cuDoubleComplex one  = zqrm_one_c; */
  /* cuDoubleComplex mone = zqrm_mone_c; */
  /* cuDoubleComplex zero = zqrm_zero_c; */
  int j;
  
  cublasOperation_t transV, notransV;
  cublasStatus_t    stat;
  cudaStream_t      stream;
  
  notransV = CUBLAS_OP_N;
  transV   = CUBLAS_OP_C;

  cuDoubleComplex *W1, *W2;
  int ldw1;
  int ldw2;
  
  if(lwd < k*n) {
    printf("TPRFB_GEMM:  Not enough workspace on device\n");
    return;
  } else {
    W1 = Wd;
    ldw1 = k;
    if(lwd >= k*(n+m)) {
      W2 = W1 + n*k;
      ldw2 = m;
    } else {
      W2 = NULL;
    }
  }
  
  /* printf("   in cuda_tprfb %4d %4d %4d %4d\n",m,n,k,l); */


  stat = cublasGetStream(handle, &stream);


  /*
   * W = A + V' * B:
   *   W = A
   *   W = W + V' * B
   *
   */

  /* printf("in cuda_tprf_gemm %4d %4d %4d %4d %4d\n",m,n,k,ldw1,ldw2); */

  
  cudaMemcpy2DAsync( W1,  ldw1 * sizeof(cuDoubleComplex),
                     A,   lda  * sizeof(cuDoubleComplex),
                     k         * sizeof(cuDoubleComplex), n,
                     cudaMemcpyDeviceToDevice, stream );

  cublasZgemm(handle, transV, CUBLAS_OP_N,
              k, n, m,
              (const cuDoubleComplex *) &one,
              (const cuDoubleComplex *) V, ldv,
              (const cuDoubleComplex *) B, ldb,
              (const cuDoubleComplex *) &one,
              W1, ldw1);
  
  if (W2 == NULL) {
    /* W = op(T) * W */
    cublasZtrmm( handle,
                 CUBLAS_SIDE_LEFT, CUBLAS_FILL_MODE_UPPER,
                 trans, CUBLAS_DIAG_NON_UNIT,
                 k, n,
                 (const cuDoubleComplex *) &one,
                 (const cuDoubleComplex *) T, ldt,
                 (const cuDoubleComplex *) W1, ldw1,
                 W1, ldw1);


    /* A1 = A1 - W = A1 - op(T) * W */
    for(j = 0; j < n; j++) {
      cublasZaxpy(handle, k,
                  (const cuDoubleComplex *) &mone,
                  (const cuDoubleComplex *) W1+j*ldw1, 1,
                  A(0,j), 1);
    }
    
    /* B = B - op(V) * W  */
    cublasZgemm(handle, notransV, CUBLAS_OP_N,
                m, n, k,
                (const cuDoubleComplex *) &mone,
                (const cuDoubleComplex *) V, ldv,
                (const cuDoubleComplex *) W1, ldw1,
                (const cuDoubleComplex *) &one,
                B, ldb);
    
  } else {
    /* W2 = V * op(T) */
    cublasZgemm( handle, notransV, trans,
                 m, k, k,
                 (const cuDoubleComplex *) &one,
                 (const cuDoubleComplex *) V, ldv,
                 (const cuDoubleComplex *) T, ldt,
                 (const cuDoubleComplex *) &zero,
                 W2, ldw2 );
    
    /* A = A - opt(T) * W1 */
    cublasZgemm( handle, trans, CUBLAS_OP_N,
                 k, n, k,
                 (const cuDoubleComplex *) &mone,
                 (const cuDoubleComplex *) T, ldt,
                 (const cuDoubleComplex *) W1, ldw1,
                 (const cuDoubleComplex *) &one,
                 A, lda );
    
    /* B = B - W2 * W1 */
    cublasZgemm( handle, CUBLAS_OP_N, CUBLAS_OP_N,
                 m, n, k,
                 (const cuDoubleComplex *) &mone,
                 (const cuDoubleComplex *) W2, ldw2,
                 (const cuDoubleComplex *) W1, ldw1,
                 (const cuDoubleComplex *) &one,
                 B, ldb );

  }
  
}





/* ========================================= old stuff ====================== */

#if 0
void  zqrm_cuda_tpmqrt_nogemm(cublasHandle_t handle,
                              cublasSideMode_t side, 
                              cublasOperation_t trans,
                              int m, int n, int k, int l, int nb,
                              const cuDoubleComplex *V,  int ldv,
                              const cuDoubleComplex *T,  int ldt,
                                    cuDoubleComplex *A,  int lda,
                                    cuDoubleComplex *B,  int ldb,
                                    cuDoubleComplex *Wd, int lwd){



  int i, j, ik, bg, en, in, ib, mb, lb;


  if(lwd < nb*n) {
    printf("TPMQRT:  Not enough workspace on device\n");
    return;
  }
  
  /* printf("in cuda_tpmqrt %4d %4d %4d %4d %4d\n",m,n,k,l,nb); */
  
  if(side == CUBLAS_SIDE_LEFT) {

    if((trans==CUBLAS_OP_T) || (trans==CUBLAS_OP_C)){
      bg  = 0 ;
      en  = k ;
      in  = nb;
    } else {
      bg  = ((k-1)/nb) * nb ;
      en  = 0 ;
      in  = -nb;
    }
    
    for(j=bg;  (in < 0 ? j >= en : j < en); j+=in){
      ib = MIN(nb, k-j);
      mb = MIN(m-l+j+ib,m);
      lb= j+1 >= l ? 0 : mb-m+l-j;
      zqrm_cuda_tprfb(handle,
                      side, trans,
                      mb, n, ib, lb,
                      V(0,j), ldv,
                      T(0,j), ldt,
                      A(j,0), lda,
                      B(0,0), ldb,
                      Wd,     lwd);
    }
  } else {
    printf("Side==r not supported\n");
  }
  

}


void  zqrm_cuda_tprfb(cublasHandle_t handle,
                      cublasSideMode_t side, 
                      cublasOperation_t trans,
                      int m, int n, int k, int l, 
                      const cuDoubleComplex *V,  int ldv,
                      const cuDoubleComplex *T,  int ldt,
                            cuDoubleComplex *A,  int lda,
                            cuDoubleComplex *B,  int ldb,
                            cuDoubleComplex *Wd, int lwd){

#if defined(zprec) || defined(cprec)
    cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
    cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
    cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
#else
    double zero = 0.0;
    double one  = 1.0;
    double mone = -1.0;
#endif
    
  /* cuDoubleComplex one  = zqrm_one_c; */
  /* cuDoubleComplex mone = zqrm_mone_c; */
  /* cuDoubleComplex zero = zqrm_zero_c; */
  int j;
  
  cublasOperation_t transV, notransV;

  if(lwd < k*n) {
    printf("TPRFB:  Not enough workspace on device\n");
    return;
  }

  int        ldw     = k;
  cuDoubleComplex *W = Wd;
  
  notransV = CUBLAS_OP_N;
  transV   = CUBLAS_OP_C;
  
  /* printf("   in cuda_tprfb %4d %4d %4d %4d %d\n",m,n,k,l, ldw); */


  int mp = MIN(m-l+1,m);
  int kp = MIN(l+1,k);
  
  //    DO J = 1, N
  //       DO I = 1, L
  //          WORK( I, J ) = B( M-L+I, J )
  //       END DO
  //    END DO
  
  //    CALL DTRMM( 'L', 'U', 'T', 'N', L, N, ONE, V( MP, 1 ), LDV,
  //$               WORK, LDWORK )         
  cublasZtrmm(handle,
              CUBLAS_SIDE_LEFT,
              CUBLAS_FILL_MODE_UPPER,
              CUBLAS_OP_C,
              CUBLAS_DIAG_NON_UNIT,
              l, n,
              (const cuDoubleComplex*) &one,
              (const cuDoubleComplex*) V(mp-1,0), ldv,
              (const cuDoubleComplex*) B(m-l,0),  ldb,
                                   W(0,0),    ldw);
  
  //         CALL DGEMM( 'T', 'N', L, N, M-L, ONE, V, LDV, B, LDB, 
  //     $               ONE, WORK, LDWORK )

  cublasZgemm(handle,
              CUBLAS_OP_C, CUBLAS_OP_N,
              l, n, m-l,
              (const cuDoubleComplex *) &one,
              (const cuDoubleComplex *) V(0,0), ldv,
              (const cuDoubleComplex *) B(0,0), ldb,
              (const cuDoubleComplex *) &one,
                                    W(0,0), ldw);
  
  // CALL DGEMM( 'T', 'N', K-L, N, M, ONE, V( 1, KP ), LDV, 
  //             B, LDB, ZERO, WORK( KP, 1 ), LDWORK )
  
  cublasZgemm(handle,
              CUBLAS_OP_C, CUBLAS_OP_N,
              k-l, n, m,
              (const cuDoubleComplex *) &one,
              (const cuDoubleComplex *) V(0,kp-1), ldv,
              (const cuDoubleComplex *) B(0,0),     ldb,
              (const cuDoubleComplex *) &zero,
                                    W(kp-1,0), ldw);

  
  // DO J = 1, N
  //    DO I = 1, K
  //       WORK( I, J ) = WORK( I, J ) + A( I, J )
  //    END DO
  // END DO

  for(j=0; j<n; j++)
    cublasZaxpy(handle, k,
                (const cuDoubleComplex *) &one,
                (const cuDoubleComplex *) A(0,j), 1,
                W(0,j), 1);
  
  // CALL DTRMM( 'L', 'U', TRANS, 'N', K, N, ONE, T, LDT, 
  //             WORK, LDWORK )

  cublasZtrmm(handle,
              CUBLAS_SIDE_LEFT,
              CUBLAS_FILL_MODE_UPPER,
              trans,
              CUBLAS_DIAG_NON_UNIT,
              k, n,
              (const cuDoubleComplex *) &one,
              (const cuDoubleComplex *) T(0,0), ldt,
              (const cuDoubleComplex *) W(0,0), ldw,
                                    W(0,0), ldw);
  
  // DO J = 1, N
  //    DO I = 1, K
  //       A( I, J ) = A( I, J ) - WORK( I, J )
  //    END DO
  // END DO

  for(j=0; j<n; j++)
    cublasZaxpy(handle, k,
                (const cuDoubleComplex *) &mone,
                (const cuDoubleComplex *) W(0,j), 1,
                                      A(0,j), 1);

  
  // CALL DGEMM( 'N', 'N', M-L, N, K, -ONE, V, LDV, WORK, LDWORK,
  //             ONE, B, LDB )

  cublasZgemm(handle,
              CUBLAS_OP_N, CUBLAS_OP_N,
              m-l, n, k,
              (const cuDoubleComplex *) &mone,
              (const cuDoubleComplex *) V(0,0), ldv,
              (const cuDoubleComplex *) W(0,0), ldw,
              (const cuDoubleComplex *) &one,
                                    B(0,0), ldb);
  
  
  // CALL DGEMM( 'N', 'N', L, N, K-L, -ONE, V( MP, KP ), LDV,
  //             WORK( KP, 1 ), LDWORK, ONE, B( MP, 1 ),  LDB )    

  cublasZgemm(handle,
              CUBLAS_OP_N, CUBLAS_OP_N,
              l, n, k-l,
              (const cuDoubleComplex *) &mone,
              (const cuDoubleComplex *) V(mp-1,kp-1), ldv,
              (const cuDoubleComplex *) W(kp-1,0), ldw,
              (const cuDoubleComplex *) &one,
                                    B(mp-1,0), ldb);


  // CALL DTRMM( 'L', 'U', 'N', 'N', L, N, ONE, V( MP, 1 ), LDV,
  //             WORK, LDWORK )

  cublasZtrmm(handle,
              CUBLAS_SIDE_LEFT,
              CUBLAS_FILL_MODE_UPPER,
              CUBLAS_OP_N,
              CUBLAS_DIAG_NON_UNIT,
              l, n,
              (const cuDoubleComplex *) &one,
              (const cuDoubleComplex *) V(mp-1,0), ldv,
              (const cuDoubleComplex *) W(0,0), ldw,
                                    W(0,0), ldw);

  // DO J = 1, N
  //    DO I = 1, L
  //       B( M-L+I, J ) = B( M-L+I, J ) - WORK( I, J )
  //    END DO
  // END DO  

  for(j=0; j<n; j++)
    cublasZaxpy(handle, l,
                (const cuDoubleComplex *) &mone,
                (const cuDoubleComplex *) W(0,j), 1,
                B(m-l,j), 1);


  
}
  

#endif
