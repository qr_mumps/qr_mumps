/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_gemqrt.c
**
** @date    05-04-2011
** @author  Alfredo Buttari
** @version 0.0.1
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include <stdio.h>
#include <stdlib.h>

#define    A(i,j) (A  + (i) + (j)*lda)
#define    B(i,j) (B  + (i) + (j)*ldb)


void  zqrm_cuda_trsm(char side, 
                     char uplo, 
                     char trans,
                     char diag, 
                     int m, int n, int k,
                     const cuDoubleComplex *alpha,
                     const cuDoubleComplex *A, int lda,
                           cuDoubleComplex *B, int ldb){
  
  
  int rt, rs, ct, cs;

  cublasOperation_t cublasTrans;
  cublasFillMode_t  cublasUplo;
  cublasSideMode_t  cublasSide;
  cublasDiagType_t  cublasDiag;

  if(side == 'l'){
    cublasSide = CUBLAS_SIDE_LEFT;
  }else if(side == 'r'){
    cublasSide = CUBLAS_SIDE_RIGHT;
  }

  if(uplo == 'l'){
    cublasUplo = CUBLAS_FILL_MODE_LOWER;
  }else if(uplo == 'u'){
    cublasUplo = CUBLAS_FILL_MODE_UPPER;
  }

  if(diag == 'n'){
    cublasDiag = CUBLAS_DIAG_NON_UNIT;
  }else if(diag == 'u'){
    cublasDiag = CUBLAS_DIAG_UNIT;
  }

  if (trans == 'n'){
    cublasTrans = CUBLAS_OP_N;
  }else if(trans == 'c'){
    cublasTrans = CUBLAS_OP_C;
  }else if(trans == 't'){
    cublasTrans = CUBLAS_OP_T;
  }else{
    fprintf(stderr, "Error in zqrm_gemm_cuda_func: transa can either be 'n' or 'c' %c\n", trans);
    return;
  }
  
#if defined(zprec) || defined(cprec)
    cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
    cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
#else
    double one  = 1.0;
    double mone = -1.0;
#endif

  /* cuDoubleComplex mone   = zqrm_mone_c; */
  cuDoubleComplex ialpha = *alpha;
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();

  if(cublasSide == CUBLAS_SIDE_LEFT) {
    if(cublasUplo == CUBLAS_FILL_MODE_UPPER) {
      
      rt = m - MIN(m,k);
      ct = 0;
      rs = 0;
      if(k>=m) {
        cs = m;
      } else {
        cs = 0;
      }
      
      if( ((k>m) && (cublasTrans==CUBLAS_OP_N)) ||
          ((m>k) && ((cublasTrans==CUBLAS_OP_T)||(cublasTrans==CUBLAS_OP_C))) ) {
        cublasZgemm(cublas_handle,
                    cublasTrans, CUBLAS_OP_N,
                    MIN(m,k), n, abs(k-m),
                    &mone,
                    A(rs,cs), lda,
                    B(cs,0), ldb,
                    &ialpha,
                    B(rt,0), ldb);
        
        ialpha = one;
      }

      cublasZtrsm(cublas_handle,
                  cublasSide, cublasUplo, cublasTrans, cublasDiag,
                  MIN(m,k), n,
                  &ialpha,
                  A(rt,ct), lda,
                  B(rt,0), ldb);


      if( ((m>k) && (cublasTrans==CUBLAS_OP_N)) ||
          ((k>m) && ((cublasTrans==CUBLAS_OP_T)||(cublasTrans==CUBLAS_OP_C))) ) {
        cublasZgemm(cublas_handle,
                    cublasTrans, CUBLAS_OP_N,
                    abs(k-m), n, MIN(m,k),
                    &mone,
                    A(rs,cs), lda,
                    B(rt,0), ldb,
                    &ialpha,
                    B(cs,0), ldb);
      }

      
    } else {
      printf("Uplo==l not supported\n");
    }
  } else {
    printf("Side==r not supported\n");
  }

/* #ifndef STARPU_CUDA_ASYNC */
  /* cudaStream_t      stream; */
  /* cublasGetStream(cublas_handle, &stream); */
  /* cudaStreamSynchronize( stream ); */
/* #endif */

  
}
  
