/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_geqrt.c
**
** @date    05-04-2011
** @author  Alfredo Buttari
** @version 0.0.1
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include "mangling.h"
#include <stdio.h>
#include <string.h>

#define    A(i,j)  (A  + (i) + (j)*lda )


void zqrm_cuda_potrf(char uplo, int m, int k,
                     cuDoubleComplex *A, int lda,
                     cuDoubleComplex *W, int lw){

#if defined (starpu_ver14)
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  cusolverDnHandle_t cusolver_handle = starpu_cusolverDn_get_local_handle();

  /* printf("potrf on gpu %d %d %d %d\n",m,k,lda,lw); */
  
  zqrm_cuda_potrf_in(cublas_handle,
                     cusolver_handle, 
                     uplo, 
                     m, k,
                     A, lda,
                     W, lw);
#endif
}
  
#if defined (starpu_ver14)
void zqrm_cuda_potrf_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        char uplo, int m, int k,
                        cuDoubleComplex *A, int lda,
                        cuDoubleComplex *W, int lw){
  
  int i, j, ib, jj, im, info, lwi, dinfo, Lwork;
  char Up='U';

#if defined(zprec) || defined(cprec)
  cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
  cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
  cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
  cuDoubleComplex half = make_cuDoubleComplex(0.5, 0.0);
#else
  double zero = 0.0;
  double one  = 1.0;
  double mone = -1.0;
  double half = 0.5;
#endif
  
  int *devInfo = NULL; // info in gpu (device copy)

  cublasStatus_t     cublas_status;
  cusolverStatus_t   cusolver_status;
  
  cudaStream_t      stream;
  cublasGetStream(cublas_handle, &stream);

  if(uplo == 'l'){
    printf("qrm_cuda_portf with uplo=l not yet implemented\n");
    return;
  }else if(uplo == 'u'){
    
    cusolverDnZpotrf(cusolver_handle,
                     CUBLAS_FILL_MODE_UPPER, k,
                     A(0,0), lda,
                     W, lw,
                     devInfo );
    
    if(m>k){
      cublasZtrsm(cublas_handle,
                  CUBLAS_SIDE_LEFT, CUBLAS_FILL_MODE_UPPER,
                  CUBLAS_OP_C, CUBLAS_DIAG_NON_UNIT,
                  k, m-k,
                  &one,
                  A(0,0), lda,
                  A(0,k), lda);
      
      cublasZherk(cublas_handle,
                  CUBLAS_FILL_MODE_UPPER, CUBLAS_OP_C,
                  m-k, k,
                  &mone,
                  A(0,k), lda,
                  &one,
                  A(k,k), lda);
    }
  }
  
  /* cudaStreamSynchronize( stream ); */
  return;
}
#endif
