/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/



/*##############################################################################################
** @file qrm_cuda_geqrt.c
**
** @date    05-04-2011
** @author  Alfredo Buttari
** @version 0.0.1
**
**##############################################################################################*/

#include "zqrm_cuda_kernels.h"
#include "mangling.h"
#include <stdio.h>
#include <string.h>

#define    V(i,j)  (V  + (i) + (j)*ldv )
#define    T(i,j)  (T  + (i) + (j)*ldt )
#define    Th(i,j) (Th + (i) + (j)*ldth )
#define    A(i,j)  (A  + (i) + (j)*lda )


void zqrm_cuda_geqrt(int m, int n, int nb,
                     int *stair, 
                     cuDoubleComplex *A, int lda,
                     cuDoubleComplex *T, int ldt,
                     cuDoubleComplex *W, int lw){

#if defined (starpu_ver14)
  cublasHandle_t cublas_handle = starpu_cublas_get_local_handle();
  cusolverDnHandle_t cusolver_handle = starpu_cusolverDn_get_local_handle();

  /* printf("geqrt on gpu\n"); */
  
  zqrm_cuda_geqrt_in(cublas_handle,
                     cusolver_handle, 
                     m, n, nb,
                     stair,
                     A, lda,
                     T, ldt,
                     W, lw);
#endif
}

#if defined (starpu_ver14)
void zqrm_cuda_geqrt_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        int m, int n, int nb,
                        int *stair, 
                        cuDoubleComplex *A, int lda,
                        cuDoubleComplex *T, int ldt,
                        cuDoubleComplex *W, int lw){
  
  int i, j, ib, jj, im, info, lwi, dinfo, Lwork;
  char Up='U', Lo='L';

#if defined(zprec) || defined(cprec)
  cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0);
  cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0);
  cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0);
  cuDoubleComplex half = make_cuDoubleComplex(0.5, 0.0);
#else
  double zero = 0.0;
  double one  = 1.0;
  double mone = -1.0;
  double half = 0.5;
#endif
  
  int *devInfo = NULL; // info in gpu (device copy)

  cuDoubleComplex b;
  cuDoubleComplex *tau, *Wi;

  cublasStatus_t     cublas_status;
  cusolverStatus_t   cusolver_status;
  
  lwi = lw-n;
  tau = W;
  Wi  = W+n;

  cudaStream_t      stream;
  cublasGetStream(cublas_handle, &stream);

  info = 0;
  for(j=0; j<MIN(m,n); j+=nb){
    ib = MIN(nb,n-j);
    if(stair==NULL){
      im = m-j;
    } else {
      im = MIN(stair[j+ib-1], m)-j;
    }

    
    /* cusolverDnZgeqrf_bufferSize(cusolver_handle, */
    /*                             im, */
    /*                             ib, */
    /*                             A, */
    /*                             lda, */
    /*                             &Lwork ); */
    
    cusolverDnZgeqrf(cusolver_handle,
                     im, ib,
                     A(j,j), lda,
                     tau+j,
                     Wi, lwi,
                     NULL);

    /* copy V into lower part of T */
    cudaMemcpy2DAsync( T(j+nb+1,j) , ldt*sizeof(cuDoubleComplex),
                       A(j+1  ,j) , lda*sizeof(cuDoubleComplex),
                       (im-1)*sizeof(cuDoubleComplex), ib,
                       cudaMemcpyDeviceToDevice, stream);

    cusolverDnZlarft(cusolver_handle,
                     'F', 'C',
                     im, MIN(im,ib),
                     A(j,j) , lda,
                     tau+j,
                     T(0,j), ldt,
                     Wi); 
    /* assert(CUSOLVER_STATUS_SUCCESS == cusolver_status); */
    
    zqrm_cuda_lastrow(j+nb, ib,
                      zero, one,
                      T(1,j), ldt+1,
                      stream);

    
    if(n-j-ib>0){
      cublasZgemm(cublas_handle,
                  CUBLAS_OP_C, CUBLAS_OP_N,
                  ib, n-j-ib, im,
                  &one,
                  T(j+nb,j), ldt,
                  A(j,j+ib), lda,
                  &zero,
                  Wi, ib);
    
      cublasZgemm(cublas_handle,
                  CUBLAS_OP_C, CUBLAS_OP_N,
                  ib, n-j-ib, ib,
                  &one,
                  T(0,j), ldt,
                  Wi, ib,
                  &zero,
                  Wi+ib*n, ib);

      cublasZgemm(cublas_handle,
                  CUBLAS_OP_N, CUBLAS_OP_N,
                  im, n-j-ib, ib,
                  &mone,
                  T(j+nb,j), ldt,
                  Wi+ib*n, ib,
                  &one,
                  A(j,j+ib), lda);
    
    /*   /\* ==================================================================== *\/ */
    /*   /\* cusolverDnDormqr(cusolver_handle, CUBLAS_SIDE_LEFT, CUBLAS_OP_T, *\/ */
    /*   /\* m-j, n-j-ib, ib, *\/ */
    /*   /\* A(j,j), lda, *\/ */
    /*   /\* tau+j, *\/ */
    /*   /\* A(j,j+ib), lda, *\/ */
    /*   /\* Wi, lwi, *\/ */
    /*   /\* devInfo); *\/ */
    /*   /\* ==================================================================== *\/ */
    }
  }

  zqrm_cuda_laset(Lo, m-1, n,
                  zero, zero,
                  A(1,0), lda,
                  stream);
  
  /* cudaStreamSynchronize( stream ); */
}

#endif




/* #if 0 */
/* void zqrm_cuda_geqrt_old(cublasHandle_t *handle, */
/*                      int m, int n, int ib, int i, */
/*                      cuDoubleComplex *A,  int lda, */
/*                      cuDoubleComplex *T,  int ldt, */
/*                      cuDoubleComplex *Wd, int lwd){ */


/*   int iib = MIN(ib,MIN(m-i+1,n)); */


/*   int lwh=(2*ib+m)*ib; */
/*   int size = lwh*sizeof(cuDoubleComplex); */
/*   cuDoubleComplex* Wh = (cuDoubleComplex*)malloc(size); */

/*   zqrm_cuda_geqrt_gemm(*handle, */
/*                        m-i+1, n, iib, */
/*                        A+i-1, lda, */
/*                        T, ldt, */
/*                        T+ib+i-1, ldt, */
/*                        Wd, lwd, */
/*                        Wh, lwh); */
/*   free(Wh); */

/*   cudaStream_t      stream; */
/*   cublasGetStream(*handle, &stream); */
/*   cudaStreamSynchronize( stream ); */
  
/* } */

/* void zqrm_cuda_geqrt_gemm(cublasHandle_t handle, */
/*                           int m, int n, int nb, */
/*                           cuDoubleComplex *A,  int lda, */
/*                           cuDoubleComplex *T,  int ldt, */
/*                           cuDoubleComplex *V,  int ldv, */
/*                           cuDoubleComplex *Wd, int lwd, */
/*                           cuDoubleComplex *Wh, int lwh){ */

/*   int i, j, ik, ib, rows, info; */
/*   char Up='U'; */

/* #if defined(zprec) || defined(cprec) */
/*     cuDoubleComplex zero = make_cuDoubleComplex(0.0, 0.0); */
/*     cuDoubleComplex one  = make_cuDoubleComplex(1.0, 0.0); */
/*     cuDoubleComplex mone = make_cuDoubleComplex(-1.0, 0.0); */
/* #else */
/*     double zero = 0.0; */
/*     double one  = 1.0; */
/*     double mone = -1.0; */
/* #endif */
/*   /\* cuDoubleComplex one  = zqrm_one_c; *\/ */
/*   /\* cuDoubleComplex mone = zqrm_mone_c; *\/ */
/*   /\* cuDoubleComplex zero = zqrm_zero_c; *\/ */
  
/*   cublasStatus_t    stat; */
/*   cudaStream_t      stream; */
/*   stat = cublasGetStream(handle, &stream); */

/*   cuDoubleComplex *Th;  int ldth = m+nb; /\* This is meant to store T and the panel on the host *\/ */

/*   /\* Workspaces *\/ */
/*   if(lwh >= (2*nb+m)*nb) { */
/*     Th = Wh+nb*nb; ldth = m+nb; /\* This is meant to store T and the panel on the host *\/ */
/*   } else { */
/*     printf("GEQRT_GEMM:  Not enough workspace on host\n"); */
/*     return; */
/*   } */


/*   if(lwd < nb*(n+m)) { */
/*     printf("GEQRT_GEMM:  Not enough workspace on device\n"); */
/*     return; */
/*   } */

/*  /\* FIXME: must set to 0 the first nb coefs in the nb columns*\/ */
/*   for(i=0; i<nb; i++) */
/*     memset(Th(0,i), 0, nb*sizeof(cuDoubleComplex)); */

/*   int old_i  = 0; */
/*   int old_ib = nb; */
/*   int ne     = MIN(m,n); */
    
/*   for(i=0;  i<ne; i+=nb){ */
/*     ib = MIN(ne-i, nb); */
/*     rows=m-i; */
    
/*     if(i>0) { */
/*       /\* Apply old panel to current panel *\/ */
/*       /\* printf("Apply panel %d  to column %d -- %d %d %d %d %d\n",old_i, i, m-old_i, ib, old_ib, ldt, lda); *\/ */
/*       zqrm_cuda_gemqrt_gemm(handle, */
/*                             CUBLAS_SIDE_LEFT, */
/*                             CUBLAS_OP_C, */
/*                             m-old_i, ib, old_ib, old_ib, */
/*                             NULL, 0,  */
/*                             (const cuDoubleComplex *) V(old_i,old_i),    ldv, */
/*                             (const cuDoubleComplex *) T(0,old_i),        ldt, */
/*                                   (cuDoubleComplex *) A(old_i,i),        lda, */
/*                                   (cuDoubleComplex *) Wd,                lwd); */
/*     } */

/*     /\* printf("Getting panel %d %d %d %d...",i,rows,ib,ldth); *\/ */
/*     cublasGetMatrixAsync(rows, ib, sizeof(*A), */
/*                          A(i,i), lda, */
/*                          Th(nb,0), ldth, stream); */

/*     cudaStreamSynchronize( stream ); */
/*     /\* printf("got it\n"); *\/ */

/*     if(i==0) { /\* While doing the first panel, set T to zero *\/ */
/*       /\* FIXME: Maybe not necessary *\/ */
/*       cudaMemsetAsync(T(0,0), 0, ldt*ne*sizeof(*T), stream); */
/*     } else { */
/*       /\* While doing panel k update A(:,k+1...n) wrt panel k-1 *\/ */
/*       if(i+ib < n){ */
/*         /\* printf("Applying panel %d to trailing %d\n",old_i,i+ib); *\/ */
/*         zqrm_cuda_gemqrt_gemm(handle, */
/*                               CUBLAS_SIDE_LEFT, */
/*                               CUBLAS_OP_C, */
/*                               m-old_i, n-i-ib, old_ib, old_ib, */
/*                               NULL, 0,  */
/*                               (const cuDoubleComplex *) V(old_i,old_i),    ldv, */
/*                               (const cuDoubleComplex *) T(0,old_i),        ldt, */
/*                                     (cuDoubleComplex *) A(old_i,i+ib),     lda, */
/*                                     (cuDoubleComplex *) Wd,                lwd); */
         
/*       } */
/*     } */
    
/*     /\* Panel is on the host, can factorize it *\/ */
/*     /\* printf("Factorizing panel %d %d %d\n",i,rows,ib); *\/ */
/*     /\* nvtxRangeId_t id1 = nvtxRangeStartA("cpu_geqrt"); *\/ */
/*     QRM_GLOBAL(zgeqrt,ZGEQRT)(&rows, &ib, &ib, */
/*            Th(nb,0), &ldth, */
/*            Th(0,0) , &ldth, */
/*            Wh,       &info); */
/*     /\* nvtxRangeEnd(id1); *\/ */

/*     /\* FIXME: Remember, the sub-diagonal of the diagonal block must be set to */
/*        0 (in order to use gemms in the tpmqrt *\/ */
    
/*     /\* Copy diagonal block *\/ */
/*     /\* printf("Copying diag block %d ...",i); *\/ */
/*     cublasSetMatrixAsync(ib, ib, sizeof(*Th), */
/*                          Th(nb,0), ldth, */
/*                          A(i,i), lda, */
/*                          stream); */

/*     cudaStreamSynchronize( stream ); */
/*     /\* printf("done\n"); *\/ */

/*     /\* Put ones on the diagonal of V and zeros above *\/ */
/*     /\* printf("Zeroing out diag block %d...",i); *\/ */
/*     /\* nvtxRangeId_t id2 = nvtxRangeStartA("cpu_laset"); *\/ */
/*     QRM_GLOBAL(zlaset,ZLASET)(&Up, &ib, &ib, &zero, &one, Th(nb,0), &ldth); */
/*     /\* nvtxRangeEnd(id2); *\/ */
    
/*     /\* Copy Th into the T matrix on the device *\/ */
/*     cublasSetMatrixAsync(ib, ib, sizeof(*Th), */
/*                          Th(0,0), ldth, */
/*                          T(0,i),  ldt, */
/*                          stream); */

/*     /\* Copy Th into the T matrix on the device *\/ */
/*     cublasSetMatrixAsync(rows, ib, sizeof(*Th), */
/*                          Th(nb,0), ldth, */
/*                          /\* T(nb+i,i),  ldt, *\/ */
/*                          V(i,i),  ldv, */
/*                          stream); */
    
/*     /\* cudaStreamSynchronize( stream ); *\/ */

/*     /\* printf("done\n"); *\/ */
/*     old_i  = i; */
/*     old_ib = ib; */
    
/*   } */

/*   if(old_i+old_ib < n) { */
/*     /\* printf("Apply panel %d  to column %d -- %d \n",old_i, old_i+ old_ib); *\/ */
/*     zqrm_cuda_gemqrt_gemm(handle, */
/*                           CUBLAS_SIDE_LEFT, */
/*                           CUBLAS_OP_C, */
/*                           m-old_i, n-old_i-old_ib, old_ib,  old_ib, */
/*                           NULL, 0,  */
/*                           (const cuDoubleComplex *) T(nb+old_i,old_i),     ldt, */
/*                           (const cuDoubleComplex *) T(0,old_i),            ldt, */
/*                                 (cuDoubleComplex *) A(old_i,old_i+old_ib), lda, */
/*                                 (cuDoubleComplex *) Wd,                    lwd); */
/*   } */
  
/*   /\* cudaStreamSynchronize( stream ); *\/ */
  
/* } */

/* #endif */
