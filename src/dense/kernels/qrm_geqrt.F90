!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

! This routine is a variant of the LAPACK _GEQRT routine which allows
! for handling matrices that have a staircase structure

subroutine zqrm_geqrt( m, n, nb, stair, ofs, a, lda, t, ldt, work, info )
  use qrm_parameters_mod
  implicit none

  integer              :: info, m, n, nb, ofs, lda, ldt

  integer              :: stair(n)
  complex(r64)         :: a(lda,*), t(ldt,*), work(*)

  integer              :: i, j, ib, iinfo, k, im, li
  logical, parameter   :: use_recursive_qr=.true.
  integer, save        :: cnt=0
  
  cnt = cnt+1
  ! if(cnt.eq.4) return
  ! write(*,*)'geqrt  ',m,n,cnt,a(1,2)
  
  info = 0
  if( m.lt.0 ) then
     info = -1
  else if( n.lt.0 ) then
     info = -2
  end if
  if( info.ne.0 ) then
     call xerbla( '_geqrt', -info )
     return
  end if

  k = min( m-ofs+1, n )
  if( k.eq.0 ) return

  i = ofs
  do j = 1, k,  nb
     ib = min( k-j+1, nb )
     if(stair(1).lt.0) then
        im = max(m - i + 1,ib)
     else
        im = max(min(stair(j+ib-1)-i+1,m),ib)
     end if
     if(im.gt.0) then
        ! write(*,*)'   geqrt2 ', im, ib, lda, ldt
        if( use_recursive_qr ) then
           call zgeqrt3( im, ib, a(i,j), lda, t(1,j), ldt, iinfo )
        else
           call zgeqrt2( im, ib, a(i,j), lda, t(1,j), ldt, iinfo )
        end if

        if( j+ib.le.n ) then

           ! write(*,*)'   larfb1 ', im, n-j-ib+1, ib, lda, ldt, lda
           call zlarfb(qrm_left, qrm_conj_transp,&
                qrm_forward, qrm_colwise,        &
                im, n-j-ib+1, ib,                &
                a(i,j), lda,                     &
                t(1,j), ldt,                     &
                a(i,j+ib), lda,                  &
                work(1) , n )
        end if
     end if
     i = i+ib
  end do


  do j=1, k
     if(stair(1).lt.0) then
        li = m
     else
        li = min(m,stair(j))
     end if
     t(nb+ofs+j:nb+li,j)            = a(ofs+j:li,j)
     t(mod(j-1,nb)+2:nb+ofs+j-1,j)  = qrm_zzero
#if defined (have_cuda)
     ! this is necessary for tpqrt_gemm and tpmqrt_gemm CUDA kernels to work
     ! write(*,*)'fucking here',j, ofs+j, min(i+j+nb,lda)
     t(nb+ofs+j-1,j)                  = qrm_zone
     ! if(stair(1).lt.0) then
        a(ofs+j: min(i+j+nb,lda),j) = qrm_zzero
     ! end if
#endif
  end do

  return
end subroutine zqrm_geqrt
