!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

#include "qrm_common.h"

subroutine zqrm_herk_task(qrm_dscr, uplo, transa, n, k, alpha, a, beta, c, prio)

#if defined(have_starpu)
  use zqrm_starpu_mod
#endif
  use qrm_parameters_mod
  use qrm_dscr_mod
  use zqrm_dsmat_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  implicit none

  type(qrm_dscr_type), target :: qrm_dscr
  character, target           :: uplo, transa
  integer, target             :: n, k
  complex(r64), target        :: alpha, beta
  type(zqrm_block_type)       :: a, c
  integer, optional           :: prio

  type(c_ptr)                 :: sizeof
  integer                     :: lda, ldc, i
  integer(kind=8)             :: ts, te
#if defined (have_starpu)
  integer, target             :: iprio
  type(c_ptr), target         :: dscrpt
  real(c_double),target       :: flops
#endif
  
  if(qrm_dscr%info.ne.0) return

#if defined (have_starpu)
  if(qrm_dscr%seq.eq.0) then
  
#if defined(dprec)
     sizeof = FSTARPU_SZ_REAL8
#elif defined(zprec)
     sizeof = FSTARPU_SZ_COMPLEX8
#elif defined(sprec)
     sizeof = FSTARPU_SZ_REAL4
#elif defined(cprec)
     sizeof = FSTARPU_SZ_COMPLEX4
#endif

     iprio = qrm_starpu_get_prio(herk_, qrm_dscr%pr, prio)

     dscrpt = c_loc(qrm_dscr)

     ! flops = real(n)*real(n)*real(k)

     call fstarpu_insert_task((/ zqrm_herk_cl,                 &
          FSTARPU_VALUE, c_loc(dscrpt), FSTARPU_SZ_C_PTR,      &
          FSTARPU_VALUE, c_loc(uplo),   FSTARPU_SZ_CHARACTER,  &
          FSTARPU_VALUE, c_loc(transa), FSTARPU_SZ_CHARACTER,  &
          FSTARPU_VALUE, c_loc(n),      FSTARPU_SZ_C_INT,      &
          FSTARPU_VALUE, c_loc(k),      FSTARPU_SZ_C_INT,      &
          FSTARPU_VALUE, c_loc(alpha),  sizeof,                &
          FSTARPU_VALUE, c_loc(beta),   sizeof,                &
          FSTARPU_R, a%hdl,                                    &
          FSTARPU_RW, c%hdl,                                   &
          FSTARPU_PRIORITY, c_loc(iprio),                      &
          ! FSTARPU_FLOPS, c_loc(flops),                         &
          FSTARPU_SCHED_CTX, c_loc(qrm_dscr%ctx),              &
          C_NULL_PTR /))

  else
#endif

     ! make a call to trsm
     lda  = size(a%c,1)
     ldc  = size(c%c,1)

     __QRM_PROF_START(ts)
     call zherk(uplo, transa,  &
          n, k,                 &
          alpha, a%c(1,1), lda, &
          beta, c%c(1,1), ldc)
     __QRM_PROF_STOP(qrm_prof_herk_task_,ts,te)
     
#if defined (have_starpu)
  end if
#endif

  return

end subroutine zqrm_herk_task


#if defined (have_starpu)
recursive subroutine zqrm_starpu_herk_cpu_func(buffers, cl_arg) bind(C)
  use qrm_starpu_common_mod
  use qrm_dscr_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  use qrm_parameters_mod
  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers
                             
  integer(c_int), target         :: n, k
  integer                        :: na, lda, nc, ldc, info
  complex(r64), pointer          :: a(:,:), c(:,:)
  character, target              :: uplo, transa
  complex(r64), target           :: alpha, beta
  type(c_ptr), target            :: dscr_c
  type(qrm_dscr_type), pointer   :: qrm_dscr
  integer(kind=8)                :: ts, te

  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c), &
       c_loc(uplo) ,c_loc(transa),                 &
       c_loc(n),                                   & 
       c_loc(k),                                   & 
       c_loc(alpha), c_loc(beta) /))

  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)
  if(qrm_dscr%info.ne.0) return
  
  na   = fstarpu_matrix_get_ny(buffers, 0)
  lda  = fstarpu_matrix_get_ld(buffers, 0)
  nc   = fstarpu_matrix_get_ny(buffers, 1)
  ldc  = fstarpu_matrix_get_ld(buffers, 1)

  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), a, shape=(/lda,na/))
  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 1), c, shape=(/ldc,nc/))

  
  __QRM_PROF_START(ts)
  call zherk(uplo, transa,  &
       n, k,                 &
       alpha, a(1,1), lda, &
       beta, c(1,1), ldc)
  __QRM_PROF_STOP(qrm_prof_herk_task_,ts,te)

  return
end subroutine zqrm_starpu_herk_cpu_func


#if defined (have_cuda)
recursive subroutine zqrm_starpu_herk_cuda_func(buffers, cl_arg) bind(C)
  use qrm_starpu_common_mod
  use qrm_dscr_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  use qrm_parameters_mod
  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers
                             
  integer(c_int), target         :: n, k
  integer                        :: na, lda, nc, ldc, info, id
  character, target              :: uplo, transa
  complex(r64), target           :: alpha, beta
  type(c_ptr), target            :: dscr_c, handlep, ap, cp
  type(qrm_dscr_type), pointer   :: qrm_dscr
  integer(kind=8)                :: ts, te

  interface
     subroutine zqrm_cuda_herk(         &
          uplo, trans,                  &
          n, k,                         &
          alpha,                        &
          a, lda,                       &
          beta,                         &
          c, ldc) bind(C)
       use qrm_parameters_mod
       use iso_c_binding
       type(c_ptr), value       :: a, c
       character(c_char), value :: uplo, trans
       integer, value           :: n, k, lda, ldc
       complex(r64)             :: alpha, beta
     end subroutine zqrm_cuda_herk
  end interface

  
  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c), &
       c_loc(uplo) ,c_loc(transa),                 &
       c_loc(n),                                   & 
       c_loc(k),                                   & 
       c_loc(alpha), c_loc(beta) /))

  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)
  if(qrm_dscr%info.ne.0) return
  
  na   = fstarpu_matrix_get_ny(buffers, 0)
  lda  = fstarpu_matrix_get_ld(buffers, 0)
  nc   = fstarpu_matrix_get_ny(buffers, 1)
  ldc  = fstarpu_matrix_get_ld(buffers, 1)

  ap = fstarpu_matrix_get_ptr(buffers, 0)
  cp = fstarpu_matrix_get_ptr(buffers, 1)

  ! id      = fstarpu_worker_get_id();
  ! handlep = qrm_dscr%cublas_handles(id+1)
  
  __QRM_PROF_START(ts)
  call zqrm_cuda_herk(         &
       uplo, transa,           &
       n, k,                   &
       alpha,                  &
       ap, lda,                &
       beta,                   &
       cp, ldc)
  __QRM_PROF_STOP(qrm_prof_herk_task_,ts,te)

  return
end subroutine zqrm_starpu_herk_cuda_func
#endif
#endif
