!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

#include "qrm_common.h"

subroutine zqrm_higeqrt_task(qrm_dscr, m, n, nb, ib, i, a, t, work, prio)

#if defined(have_starpu)
  use zqrm_starpu_mod
  use qrm_starpu_common_mod
#endif
  use zqrm_dsmat_mod
  use qrm_dscr_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif

  implicit none

  type(qrm_dscr_type), target   :: qrm_dscr
  type(zqrm_block_type), target :: a, t
  integer                       :: i, nb
  type(zqrm_ws_type)            :: work
  integer, target               :: m, n, ib
  integer(kind=8)               :: ts, te
  integer, optional             :: prio

#if defined (have_starpu)
  type(c_ptr)                   :: hdla, hdlt
  type(c_ptr), target           :: dscrpt, strpt
  integer, target               :: iprio
  real(c_double),target         :: flops
#endif
  integer                       :: ma, na, lda, ldt, info, j, li
  integer, target               :: ii

  if(qrm_dscr%info.ne.0) return

  if(a%partitioned) then
     ii    = (i-1)*nb+1
  else
     ii    = 1
  end if

#if defined (have_starpu)
  if(qrm_dscr%seq.eq.0) then

     ! submit a geqrt task
     if(a%partitioned) then
        hdla = a%shdls(i)
        hdlt = t%shdls(i)
     else
        hdla = a%hdl
        hdlt = t%hdl
     end if
     
     ! if(qrm_allocated(a%stair)) then
        ! iprio = 0  ! FIXME: not the right thing to do
     ! else
     iprio = qrm_starpu_get_prio(geqrt_, qrm_dscr%pr, prio)
     ! end if

     dscrpt = c_loc(qrm_dscr)
     if(qrm_allocated(a%stair)) then
        strpt = c_loc(a%stair(ii))
     else
        strpt = c_null_ptr
     end if

     if(m.ge.n) then
        flops = 2.d0*real(n)*real(n)*(real(m)-real(n)/3.d0)
     else
        flops = 2.d0*real(m)*real(m)*(real(n)-real(m)/3.d0)
     end if

     call fstarpu_insert_task((/ zqrm_geqrt_cl,           &
          FSTARPU_VALUE, c_loc(dscrpt), FSTARPU_SZ_C_PTR, &
          FSTARPU_VALUE, c_loc(m),  FSTARPU_SZ_C_INT,     &
          FSTARPU_VALUE, c_loc(n),  FSTARPU_SZ_C_INT,     &
          FSTARPU_VALUE, c_loc(ii), FSTARPU_SZ_C_INT,     &
          FSTARPU_VALUE, c_loc(ib), FSTARPU_SZ_C_INT,     &
          FSTARPU_VALUE, c_loc(strpt), FSTARPU_SZ_C_PTR,  &
          FSTARPU_RW, hdla,                               &
          FSTARPU_RW, hdlt,                               &
          FSTARPU_SCRATCH, work%hdl,                      &
          FSTARPU_PRIORITY, c_loc(iprio),                 &
          ! FSTARPU_FLOPS, c_loc(flops),                    &
          FSTARPU_SCHED_CTX, c_loc(qrm_dscr%ctx),         &
          C_NULL_PTR /))
     ! call fstarpu_task_wait_for_all()
  else
#endif

     ! make a simple call to lapack geqrt
     lda = size(a%c,1)
     ldt = size(t%c,1)

     __QRM_PROF_START(ts)
     if(allocated(a%stair)) then
        call zqrm_geqrt(m, n, ib,  &
             a%stair(ii), ii,      &
             a%c(1,ii), lda,       &
             t%c(1,ii), ldt,       &
             work%c(1,1), info)
     else
        call zqrm_geqrt(m, n, ib,  &
             -1, ii,               &
             a%c(1,ii), lda,       &
             t%c(1,ii), ldt,       &
             work%c(1,1), info)
     end if
     __QRM_PROF_STOP(qrm_prof_geqrt_task_,ts,te)

#if defined (have_starpu)
  end if
#endif

  return
end subroutine zqrm_higeqrt_task

#if defined (have_starpu)
recursive subroutine zqrm_starpu_geqrt_cpu_func(buffers, cl_arg) bind(C)
  use qrm_parameters_mod
  use qrm_dscr_mod
  use qrm_starpu_common_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers

  integer(c_int), target         :: ii, ib, m, n
  type(c_ptr), target            :: dscr_c, stair_c
  integer                        :: ma, na, lda, nt, ldt, nw, ldw, info, j, li
  complex(r64), pointer          :: a(:,:), t(:,:), w(:,:)
  integer, pointer               :: stair(:)
  type(qrm_dscr_type), pointer   :: qrm_dscr
  integer(kind=8)                :: ts, te

  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c), &
                                    c_loc(m),      &
                                    c_loc(n),      &
                                    c_loc(ii),     &
                                    c_loc(ib),     &
                                    c_loc(stair_c) /))

  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)
  if(qrm_dscr%info.ne.0) return

  nw   = fstarpu_matrix_get_ny(buffers, 2)
  ldw  = fstarpu_matrix_get_ld(buffers, 2)
  ma   = fstarpu_matrix_get_nx(buffers, 0)
  na   = fstarpu_matrix_get_ny(buffers, 0)
  lda  = fstarpu_matrix_get_ld(buffers, 0)
  nt   = fstarpu_matrix_get_ny(buffers, 1)
  ldt  = fstarpu_matrix_get_ld(buffers, 1)

  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), a, shape=(/lda,na/))
  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 1), t, shape=(/ldt,nt/))
  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 2), w, shape=(/ldw,nw/))

  __QRM_PROF_START(ts)
  if(c_associated(stair_c)) then

     call c_f_pointer(stair_c, stair, shape=(/na/))
     call zqrm_geqrt(m, n, ib,   &
          stair(1),  ii,         &
          a(1,1)  , lda,         &
          t(1,1)  , ldt,         &
          w(1,1)  , info)
  else
     call zqrm_geqrt(m, n, ib,   &
          -1,  ii,               &
          a(1,1), lda,           &
          t(1,1), ldt,           &
          w(1,1), info)
  end if
  __QRM_PROF_STOP(qrm_prof_geqrt_task_,ts,te)

  ! TODO: add check for return code in info

  return
end subroutine zqrm_starpu_geqrt_cpu_func


#if defined (have_cuda) && defined (have_cusolver) && defined(have_cuda_qr_panel)
recursive subroutine zqrm_starpu_geqrt_cuda_func(buffers, cl_arg) bind(C)
  use qrm_parameters_mod
  use qrm_dscr_mod
  use qrm_starpu_common_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers

  integer(c_int), target         :: ii, ib, m, n
  type(c_ptr), target            :: dscr_c, stair_c, ap, tp, wp
  type(c_ptr), target            :: cublas_handle, cusolver_handle
  integer                        :: ma, na, lda, nt, ldt, nw, ldw, info, id, lw
  type(qrm_dscr_type), pointer   :: qrm_dscr
  integer(kind=8)                :: ts, te, tr

  interface
     subroutine zqrm_cuda_geqrt( &
          m, n, nb,              &    
          stair,                 &
          a, lda,                &    
          t, ldt,                &    
          w, lw) bind(C)
       use iso_c_binding
       ! type(c_ptr), value       :: cublas_handle, cusolver_handle
       type(c_ptr), value       :: a, t, w, stair
       integer, value           :: m, n, nb, lda, ldt, lw
     end subroutine zqrm_cuda_geqrt
  end interface

  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c), &
                                    c_loc(m),      &
                                    c_loc(n),      &
                                    c_loc(ii),     &
                                    c_loc(ib),     &
                                    c_loc(stair_c) /))

  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)
  if(qrm_dscr%info.ne.0) return

  ma   = fstarpu_matrix_get_nx(buffers, 0)
  na   = fstarpu_matrix_get_ny(buffers, 0)
  lda  = fstarpu_matrix_get_ld(buffers, 0)

  nt   = fstarpu_matrix_get_ny(buffers, 1)
  ldt  = fstarpu_matrix_get_ld(buffers, 1)

  nw   = fstarpu_matrix_get_ny(buffers, 2)
  ldw  = fstarpu_matrix_get_ld(buffers, 2)
  lw   = nw*ldw

  ap   = fstarpu_matrix_get_ptr(buffers, 0)
  tp   = fstarpu_matrix_get_ptr(buffers, 1)
  wp   = fstarpu_matrix_get_ptr(buffers, 2)
  
  ! call system_clock(ts)
  __QRM_PROF_START(ts)
  call zqrm_cuda_geqrt(         &
       m , n , ib,              &
       stair_c,                 &
       ap, lda,                 &
       tp, ldt,                 &
       wp, lw )
  __QRM_PROF_STOP(qrm_prof_geqrt_task_,ts,te)
  ! TODO: add check for return code in info
  ! call system_clock(te,tr)
  ! write(*,*)'cuda geqrt',real(te-ts)/real(tr)
  return
end subroutine zqrm_starpu_geqrt_cuda_func
#endif


#endif
