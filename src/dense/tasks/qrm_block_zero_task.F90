!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a full of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################
!! 
!! This file contains a routine for zeroing a block
!!
!! ##############################################################################################

#include "qrm_common.h"

subroutine zqrm_block_zero_task(qrm_dscr, aij, prio)
#if defined(have_starpu)
  use zqrm_starpu_mod
#endif
  use qrm_dscr_mod
  use zqrm_dsmat_mod
  use qrm_string_mod
  use iso_c_binding
#if defined (perfprof)
  use qrm_perfprof_mod
#endif
  implicit none

  type(qrm_dscr_type), target    :: qrm_dscr
  type(zqrm_block_type)          :: aij
  integer, optional              :: prio

#if defined (have_starpu)
  integer, target                :: iprio
  type(c_ptr), target            :: dscrpt
  type(c_ptr)                    :: sizeof
#endif
  integer(kind=8)                :: ts, te

  if(qrm_dscr%info.ne.0) return
  
#if defined (have_starpu)
  if(qrm_dscr%seq.eq.0) then
 
     dscrpt = c_loc(qrm_dscr)
     iprio = qrm_starpu_get_prio(zero_, qrm_dscr%pr, prio)

     call fstarpu_insert_task    ((/                                &
          zqrm_block_zero_cl,                                       &
          FSTARPU_VALUE, c_loc(dscrpt),       FSTARPU_SZ_C_PTR,     &
          FSTARPU_W, aij%hdl,                                       &
          FSTARPU_PRIORITY, c_loc(iprio),                           &
          FSTARPU_SCHED_CTX, c_loc(qrm_dscr%ctx),                   &
          C_NULL_PTR /))

  else
#endif
     __QRM_PROF_START(ts)
     aij%c = qrm_zzero
     __QRM_PROF_STOP(qrm_prof_zero_task_,ts,te)
#if defined (have_starpu)
  end if
#endif

  return
end subroutine zqrm_block_zero_task


#if defined (have_starpu)
recursive subroutine zqrm_starpu_block_zero_cpu_func(buffers, cl_arg) bind(C)
  use qrm_starpu_common_mod
  use zqrm_dsmat_mod
  use qrm_dscr_mod
#if defined (perfprof)
  use qrm_perfprof_mod
#endif

  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers

  type(c_ptr), target            :: dscr_c
  type(qrm_dscr_type), pointer   :: qrm_dscr
  complex(r64), pointer          :: a(:,:)
  integer                        :: m, n, lda
  integer(kind=8)                :: ts, te
  
  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c) /))
  
  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)

  if(qrm_dscr%info.ne.0) return

  m   = fstarpu_matrix_get_nx(buffers, 0)
  n   = fstarpu_matrix_get_ny(buffers, 0)
  lda = fstarpu_matrix_get_ld(buffers, 0)

  call c_f_pointer(fstarpu_matrix_get_ptr(buffers, 0), a, shape=(/lda,n/))

  __QRM_PROF_START(ts)
  a(1:m,1:n) = qrm_zzero
  __QRM_PROF_STOP(qrm_prof_zero_task_,ts,te)
  return

end subroutine zqrm_starpu_block_zero_cpu_func


#if defined (have_cuda)
recursive subroutine zqrm_starpu_block_zero_cuda_func(buffers, cl_arg) bind(C)
  use qrm_starpu_mod
  use zqrm_dsmat_mod
  use qrm_dscr_mod
#if defined (perfprof)
    use qrm_perfprof_mod
#endif

  implicit none

  type(c_ptr), value, intent(in) :: cl_arg
  type(c_ptr), value             :: buffers

  type(c_ptr), target            :: dscr_c, ap
  type(qrm_dscr_type), pointer   :: qrm_dscr
  complex(r64), pointer          :: a(:,:)
  integer                        :: m, n, lda
  integer(kind=8)                :: ts, te

  interface
     subroutine zqrm_cuda_zero(         &
          m, n,                         &
          a, lda) bind(C)
       use qrm_parameters_mod
       use iso_c_binding
       type(c_ptr), value       :: a
       integer, value           :: m, n, lda
     end subroutine zqrm_cuda_zero
  end interface

  
  call fstarpu_unpack_arg(cl_arg,(/ c_loc(dscr_c) /))
  
  ! until StarPU has a proper error handling method we have to check
  ! manually
  call c_f_pointer(dscr_c, qrm_dscr)

  if(qrm_dscr%info.ne.0) return

  m   = fstarpu_matrix_get_nx(buffers, 0)
  n   = fstarpu_matrix_get_ny(buffers, 0)
  lda = fstarpu_matrix_get_ld(buffers, 0)

  ap  = fstarpu_matrix_get_ptr(buffers, 0)

  __QRM_PROF_START(ts)
  call zqrm_cuda_zero(m, n, ap, lda)
  __QRM_PROF_STOP(qrm_prof_zero_task_,ts,te)
  return

end subroutine zqrm_starpu_block_zero_cuda_func



#endif
#endif
