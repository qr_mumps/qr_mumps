!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!
!! This file is part of qr_mumps.
!!
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as
!! published by the Free Software Foundation, either version 3 of
!! the License, or (at your option) any later version.
!!
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!> @file qrm_spmat_mod.F90
!! This file contains the module that implements the main qr_mumps data structure
!!
!!
!! ##############################################################################################


#include "qrm_common.h"


!> This module contains the definition of the basic sparse matrix type
!! and of the associated methods
module zqrm_spmat_mod
  use qrm_parameters_mod

  !> This type defines the data structure used to store a matrix.

  !> A matrix can be represented either in COO, CSR or CSC
  !! format. Following the qr_mumps convention any array
  !! visible/usable from the users interface will be a pointer;
  !! otherwise allocatbles will be used because they normally provide
  !! better performance.
  !!
  type zqrm_spmat_type
     !> Number of rows
     integer                               :: m=0
     !> Number of columns
     integer                               :: n=0
     !> Number of nonzero elements
     integer                               :: nz=0
     !> Storage format; can be either 'COO', 'CSR' or 'CSC'
     character(len=3)                      :: fmt='coo'
     !> Symmetry of the graph; can be 0 for unsym, >0 for sym
     integer                               :: sym
     !> Pointer to the beginning of each row in CSR format
     integer, pointer,    dimension(:)     :: iptr => null()
     !> Pointer to the beginning of each column in CSC format
     integer, pointer,    dimension(:)     :: jptr => null()
     !> Row indices
     integer, pointer,    dimension(:)     :: irn => null()
     !> Column indices
     integer, pointer,    dimension(:)     :: jcn => null()
     !> Numerical values
     complex(r64), pointer, dimension(:)   :: val => null()
  end type zqrm_spmat_type



  !> Generic interface for the zqrm_spmat_alloc routine
  interface qrm_spmat_alloc
     module procedure zqrm_spmat_alloc
  end interface

  !> Generic interface for the zqrm_spmat_alloc routine
  interface qrm_spmat_dealloc
     module procedure zqrm_spmat_dealloc
  end interface

  !> Generic interface for the zqrm_spmat_init routine
  interface qrm_spmat_init
     module procedure zqrm_spmat_init
  end interface

  !> Generic interface for the zqrm_spmat_ata routine
  interface qrm_spmat_ata
     subroutine zqrm_spmat_ata(a_csc, ata_csr, info)
       import zqrm_spmat_type
       type(zqrm_spmat_type), intent(in)  :: a_csc
       type(zqrm_spmat_type), intent(out) :: ata_csr
       integer, optional                  :: info
     end subroutine zqrm_spmat_ata
  end interface qrm_spmat_ata

  
  !> Generic interface for the zqrm_spmat_sort routine
  interface qrm_spmat_sort
     subroutine zqrm_sort_mat(qrm_mat, values, rc, info)
       import zqrm_spmat_type
       type(zqrm_spmat_type)       :: qrm_mat
       logical, optional           :: values
       character(len=3), optional  :: rc
       integer, optional           :: info
     end subroutine zqrm_sort_mat
  end interface qrm_spmat_sort
  
  !> Generic interface for the zqrm_spmat_copy routine
  interface qrm_spmat_copy
     module procedure zqrm_spmat_copy
  end interface

  !> Generic interface for the zqrm_spmat_destroy routine
  interface qrm_spmat_destroy
     module procedure zqrm_spmat_destroy
  end interface

  !> Generic interface for the zqrm_spmat_check routine
  interface qrm_spmat_check
     module procedure zqrm_spmat_check
  end interface

  !> Generic interface for the zqrm_spmat_convert routine
  interface qrm_spmat_convert
     subroutine zqrm_spmat_convert(in_mat, out_mat, fmt, values, dups, uplo, info)
       import zqrm_spmat_type
       type(zqrm_spmat_type), intent(in)  :: in_mat
       type(zqrm_spmat_type)              :: out_mat
       character, intent(in)              :: fmt*(*)
       logical, optional                  :: values, dups
       character, optional                :: uplo
       integer, optional                  :: info
     end subroutine zqrm_spmat_convert
  end interface qrm_spmat_convert
  

  interface qrm_matmul
     procedure :: zqrm_spmat_mv_2d, zqrm_spmat_mv_1d
  end interface qrm_matmul
  
  interface qrm_spmat_mv
     procedure :: zqrm_spmat_mv_2d, zqrm_spmat_mv_1d
  end interface qrm_spmat_mv
  
  !> Generic interface for the zqrm_spmat_mv routine
  interface zqrm_spmat_mv
     subroutine zqrm_spmat_mv_2d(qrm_mat, transp, alpha, x, beta, y)
       use qrm_parameters_mod
       import zqrm_spmat_type
       type(zqrm_spmat_type)   :: qrm_mat
       complex(r64), intent(out)  :: y(:,:)
       complex(r64), intent(in)   :: x(:,:)
       complex(r64), intent(in)   :: alpha, beta
       character(len=*)        :: transp
     end subroutine zqrm_spmat_mv_2d
     subroutine zqrm_spmat_mv_1d(qrm_mat, transp, alpha, x, beta, y)
       use qrm_parameters_mod
       import zqrm_spmat_type
       type(zqrm_spmat_type)   :: qrm_mat
       complex(r64), intent(out)  :: y(:)
       complex(r64), intent(in)   :: x(:)
       complex(r64), intent(in)   :: alpha, beta
       character(len=*)        :: transp
     end subroutine zqrm_spmat_mv_1d
  end interface zqrm_spmat_mv

  !> Generic interface for the zqrm_spmat_nrm routine
  interface qrm_spmat_nrm
     subroutine zqrm_spmat_nrm(qrm_mat, ntype, nrm, info)
       use qrm_parameters_mod
       import zqrm_spmat_type
       type(zqrm_spmat_type), intent(in) :: qrm_mat
       real(r64)                         :: nrm
       character                         :: ntype
       integer, optional                 :: info
     end subroutine zqrm_spmat_nrm
  end interface qrm_spmat_nrm
  

  !> Generic interface for the zqrm_spmat_nrm routine
  interface qrm_spmat_transpose
     subroutine zqrm_spmat_transpose_inplace(qrm_mat, info)
       import zqrm_spmat_type
       type(zqrm_spmat_type), intent(inout) :: qrm_mat
       integer, optional                    :: info
     end subroutine zqrm_spmat_transpose_inplace
  end interface qrm_spmat_transpose
  

contains

  !> This subroutine allocates memory for a sparse matrix.
  !!
  !! @param[in,out] qrm_spmat A @link zqrm_spmat_mod::zqrm_spmat_type data
  !!           structure. The memory for storing the matrix is
  !!           allocated according to the storage format. Also
  !!           qrm_spmat%nz, qrm_spmat%m and qrm_spmat%n are set to
  !!           nz, m and n respectively.
  !!           These are the sizes of the arrays in output
  !!           * coo: irn(nz), jcn(nz), val(nz)
  !!           * csr: iptr(m+1), jcn(nz), val(nz)
  !!           * csc: irn(nz), jptr(n+1), val(nz)
  !!
  !! @param[in] nz  The number of nonzeroes contained in the matrix
  !!
  !! @param[in] m   The number of rows in the matrix
  !!
  !! @param[in] n   The number of columns in the matrix
  !!
  !! @param[in] fmt The matrix storage format. Can be either "coo" or "csr"
  !!           or "csc"
  !!
  subroutine zqrm_spmat_alloc(qrm_spmat, nz, m, n, fmt, info)
    use qrm_mem_mod
    use qrm_error_mod
    implicit none

    class(zqrm_spmat_type), intent(inout) :: qrm_spmat
    integer, intent(in)                   :: nz, m, n
    character, intent(in)                 :: fmt*(*)
    integer, optional                     :: info

    integer                               :: err
    character(len=*), parameter           :: name='qrm_spmat_alloc'

    err = 0

#if defined(debug)
    __QRM_PRNT_DBG('("Allocating Matrix")')
#endif

    if(fmt .eq. 'coo') then
       if(err.eq.0) call qrm_alloc(qrm_spmat%irn, nz, info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%jcn, nz, info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%val, nz, info=err)
       __QRM_INFO_CHECK(err, name, 'qrm_alloc', 9999)
    else if(fmt .eq. 'csr') then
       if(err.eq.0) call qrm_alloc(qrm_spmat%iptr, m+1, info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%jcn , nz,  info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%val , nz,  info=err)
       __QRM_INFO_CHECK(err, name, 'qrm_alloc', 9999)
    else if(fmt .eq. 'csc') then
       if(err.eq.0) call qrm_alloc(qrm_spmat%irn , nz,  info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%jptr, n+1, info=err)
       if(err.eq.0) call qrm_alloc(qrm_spmat%val , nz,  info=err)
       __QRM_INFO_CHECK(err, name, 'qrm_alloc', 9999)
    else
       ! format not known. set info and jump to 9999
       err = 1
       call qrm_error_print(err, name, aed=fmt)
       goto 9999
    end if

    qrm_spmat%m  = m
    qrm_spmat%n  = n
    qrm_spmat%nz = nz
    
    qrm_spmat%sym = 0

9999 continue
    if(present(info)) info = err
    return

  end subroutine zqrm_spmat_alloc


  !> This subroutine deallocates memory of a sparse matrix.
  !!
  !! @param[in,out] qrm_spmat A zqrm_spmat_type data
  !!           structure to be deallocated.
  subroutine zqrm_spmat_dealloc(qrm_spmat, info)
    use qrm_mem_mod
    use qrm_error_mod
    implicit none

    class(zqrm_spmat_type), intent(inout) :: qrm_spmat
    integer, optional                     :: info

    integer                               :: err
    character(len=*), parameter           :: name='qrm_spmat_dealloc'

    err = 0

    call qrm_dealloc(qrm_spmat%irn,      info=err)
    call qrm_dealloc(qrm_spmat%jcn,      info=err)
    call qrm_dealloc(qrm_spmat%iptr,     info=err)
    call qrm_dealloc(qrm_spmat%jptr,     info=err)
    call qrm_dealloc(qrm_spmat%val,      info=err)
    __QRM_INFO_CHECK(err, name, 'qrm_dealloc', 9999)
    
    qrm_spmat%n       = 0
    qrm_spmat%m       = 0
    qrm_spmat%nz      = 0
    qrm_spmat%fmt     = ''

9999 continue
    if(present(info)) info = err
    return
  end subroutine zqrm_spmat_dealloc
    
  
  !> This subroutine initializes a qrm_spmat_type instance setting
  !! default values into the control parameters
  !!
  !! @param[in,out] qrm_spmat The matrix to be initialized
  !!
  subroutine zqrm_spmat_init(qrm_spmat, info)
#if defined (have_starpu)
    use zqrm_starpu_mod
#endif
    implicit none

    type(zqrm_spmat_type), intent(inout) :: qrm_spmat
    integer, optional                    :: info

#if defined(have_starpu)
  call zqrm_starpu_init_codelets()
#endif

    nullify(qrm_spmat%iptr, qrm_spmat%jptr, qrm_spmat%irn, qrm_spmat%jcn, &
         & qrm_spmat%val)

    qrm_spmat%sym = 0

    qrm_spmat%m  = 0
    qrm_spmat%n  = 0
    qrm_spmat%nz = 0
    
    if(present(info)) info = 0
    return

  end subroutine zqrm_spmat_init






  !> This subroutine makes a copy of a matrix. Optionally the values
  !! may be ignored (this comes handy during the analysis)
  !!
  !! @param[in] in_mat      the input matrix
  !!
  !! @param[in,out] out_mat the output matrix in fmt format
  !!
  !! @param[in]  values      (optional) if values=.true. the output matrix will include
  !!                        numerical values, otherwise only the structure
  !!
  subroutine zqrm_spmat_copy(in_mat, out_mat, values, info)

    use qrm_error_mod
    use qrm_mem_mod
    implicit none

    type(zqrm_spmat_type), intent(in) :: in_mat
    type(zqrm_spmat_type)             :: out_mat
    logical, optional                 :: values
    integer, optional                 :: info

    logical                           :: ivalues=.true.
    ! error management
    integer                           :: err
    character(len=*), parameter       :: name='zqrm_spmat_copy'

    err = 0

    if(present(values)) then
       ivalues=values
    else
       ivalues=.true.
    end if

    select case(in_mat%fmt)
    case('csc')
       if(ivalues)  call qrm_realloc(out_mat%val,  in_mat%nz,  err)
       if(err.eq.0) call qrm_realloc(out_mat%jptr, in_mat%n+1, err)
       if(err.eq.0) call qrm_realloc(out_mat%irn,  in_mat%nz,  err)
       __QRM_INFO_CHECK(err, name,'qrm_realloc',9999)

       if(ivalues) out_mat%val(1:in_mat%nz) = in_mat%val(1:in_mat%nz)
       out_mat%jptr(1:in_mat%n+1)           = in_mat%jptr(1:in_mat%n+1)
       out_mat%irn(1:in_mat%nz)             = in_mat%irn(1:in_mat%nz)
    case('coo')
       if(ivalues)  call qrm_realloc(out_mat%val, in_mat%nz, err)
       if(err.eq.0) call qrm_realloc(out_mat%jcn, in_mat%nz, err)
       if(err.eq.0) call qrm_realloc(out_mat%irn, in_mat%nz, err)
       __QRM_INFO_CHECK(err, name,'qrm_realloc',9999)

       if(ivalues) out_mat%val(1:in_mat%nz) = in_mat%val(1:in_mat%nz)
       out_mat%jcn(1:in_mat%nz)             = in_mat%jcn(1:in_mat%nz)
       out_mat%irn(1:in_mat%nz)             = in_mat%irn(1:in_mat%nz)
    case default
       ! format not known. set info and jump to 9999
       info = 1
       call qrm_error_print(info, name, aed=in_mat%fmt)
       goto 9999
    end select

    out_mat%n        = in_mat%n
    out_mat%m        = in_mat%m
    out_mat%nz       = in_mat%nz
    out_mat%fmt      = in_mat%fmt

9999 continue
    ! cleanup and return
    if(err.ne.0) call zqrm_spmat_destroy(out_mat)

    if(present(info)) info = err
    return

  end subroutine zqrm_spmat_copy

  !> This subroutine destroyes a qrm_spmat instance
  !!
  !! @param[in,out] qrm_spmat the matrix to be destroyed
  !!
  subroutine zqrm_spmat_destroy(qrm_spmat, info)

    use qrm_error_mod
    use qrm_mem_mod
    implicit none

    type(zqrm_spmat_type)           :: qrm_spmat
    integer, optional               :: info

    ! error management
    integer                         :: err
    character(len=*), parameter     :: name='zqrm_spmat_destroy'

    err = 0

    qrm_spmat%n       = 0
    qrm_spmat%m       = 0
    qrm_spmat%nz      = 0
    qrm_spmat%fmt     = ''
    call zqrm_spmat_dealloc(qrm_spmat, info)
    
9999 continue

    if(present(info)) info = err
    return

  end subroutine zqrm_spmat_destroy



  subroutine zqrm_spmat_check(qrm_spmat, info)

    use qrm_error_mod
    implicit none

    type(zqrm_spmat_type) :: qrm_spmat
    integer, optional     :: info

    ! error management
    integer                         :: err
    character(len=*), parameter     :: name='zqrm_spmat_check'

    err = 0
    
    if((qrm_spmat%m .lt. 0) .or. (qrm_spmat%n .lt. 0) .or. &
         & (qrm_spmat%nz .lt. 0) .or. &
         & (qrm_spmat%nz .gt. (int(qrm_spmat%n,kind=8)*int(qrm_spmat%m,kind=8)))) then
       err = 29
       call qrm_error_print(err, name,ied=(/qrm_spmat%m,qrm_spmat%n,qrm_spmat%nz/))
       goto 9999
    end if

9999 continue

    if(present(info)) info = err
    return
    
  end subroutine zqrm_spmat_check


  !> This subroutine sorts the entries of a graph in order of
  !> increasing indexes
  !!
  !! in  a     the input spmat to be pruned
  !! out b     the pruned output spmat
  !! in  list  the list of variables that have to be pruned
  !! in  nlist the length of list
  !!
  subroutine zqrm_spmat_prune(a, b, list, nlist, info)

    use qrm_error_mod
    implicit none

    type(zqrm_spmat_type)       :: a, b
    integer                     :: list(:), nlist
    integer, optional           :: info
    
    ! error management
    integer                     :: err
    character(len=*), parameter :: name='qrm_graph_prune'

    err = 0

    select case(a%fmt)
    case('csc')
       call zqrm_spmat_prune_csc(a, b, list, nlist, info)
       __QRM_INFO_CHECK(err, name,'qrm_graph_prunt_csc',9999)
    case('csr')
    case('coo')
    end select

9999 continue
    if(present(info)) info = err
    return
  end subroutine zqrm_spmat_prune


  subroutine zqrm_spmat_prune_csc(a, b, list, nlist, info)

    use qrm_mem_mod
    use qrm_error_mod
    implicit none

    type(zqrm_spmat_type)       :: a, b
    integer                     :: list(:), nlist
    integer, optional           :: info

    logical, allocatable        :: tag(:)
    integer                     :: i, j, iptr

    
    allocate(tag(a%n))

    tag = .false.

    tag(list(1:nlist)) = .true.

    call qrm_alloc(b%jptr, size(a%jptr))
    call qrm_alloc(b%irn , size(a%irn))

    ! write(*,'("========= A =========")')
    ! do j=1, a%n
    !    write(*,'("col ",i3,":  ")', advance='no')j
    !    do iptr=a%jptr(j), a%jptr(j+1)-1
    !       i = a%irn(iptr)
    !       write(*,'(i3,", ")', advance='no')i
    !    end do
    !    write(*,'(" ")')
    ! end do

    
    b%nz      = 0
    b%n       = a%n
    b%m       = a%m
    b%fmt     = a%fmt
    b%jptr(1) = 1
    
    do j=1, a%n
       if(tag(j)) then
          b%nz = b%nz+1
          b%irn(b%nz) = j
       else
          do iptr=a%jptr(j), a%jptr(j+1)-1
             i = a%irn(iptr)
             if(.not.tag(i)) then
                b%nz = b%nz+1
                b%irn(b%nz) = i
             end if
          end do
       end if
       b%jptr(j+1) = b%nz+1
    end do

    ! write(*,'("========= b =========")')
    ! do j=1, b%n
    !    write(*,'("col ",i3,":  ")', advance='no')j
    !    do iptr=b%jptr(j), b%jptr(j+1)-1
    !       i = b%irn(iptr)
    !       write(*,'(i3,", ")', advance='no')i
    !    end do
    !    write(*,'(" ")')
    ! end do

    return
    
  end subroutine zqrm_spmat_prune_csc

  
end module zqrm_spmat_mod
