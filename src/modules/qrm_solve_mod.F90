!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!!
!! This file contains a module with generic interfaces for all the solve typed routines
!!
!! ##############################################################################################


!> @brief This module contains all the interfaces for the typed routines
!! in the solve phase
module zqrm_solve_mod

  interface qrm_apply
     procedure                              :: zqrm_spfct_unmqr2d, zqrm_spfct_unmqr1d
  end interface qrm_apply

  !> Generic interface for the zqrm_spfct_oldunmqr2d and zqrm_spfct_oldunmqr1d routines
  !!
  interface qrm_spfct_unmqr
     procedure                              :: zqrm_spfct_unmqr2d, zqrm_spfct_unmqr1d
  end interface qrm_spfct_unmqr

  interface qrm_apply_async
     procedure                              :: zqrm_spfct_unmqr_async
  end interface qrm_apply_async
  
  interface qrm_spfct_unmqr_async
     subroutine zqrm_spfct_unmqr_async(qrm_dscr, qrm_spfct, transp, sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                  :: qrm_dscr
       type(zqrm_spfct_type), intent(in)    :: qrm_spfct
       type(zqrm_sdata_type)                :: sdata
       character(len=*), intent(in)         :: transp
     end subroutine zqrm_spfct_unmqr_async
  end interface qrm_spfct_unmqr_async

  interface zqrm_spfct_unmqr
     subroutine zqrm_spfct_unmqr2d(qrm_spfct, transp, b, x, info)
       use zqrm_spfct_mod
       type(zqrm_spfct_type), target       :: qrm_spfct
       character(len=*)                    :: transp
       complex(r64), target                :: b(:,:)
       complex(r64), optional, target      :: x(:,:)
       integer, optional                   :: info
     end subroutine zqrm_spfct_unmqr2d
     subroutine zqrm_spfct_unmqr1d(qrm_spfct, transp, b, x, info)
       use zqrm_spfct_mod
       type(zqrm_spfct_type), target  :: qrm_spfct
       character(len=*)               :: transp
       complex(r64), target           :: b(:)
       complex(r64), optional, target :: x(:)
       integer, optional              :: info
     end subroutine zqrm_spfct_unmqr1d
  end interface zqrm_spfct_unmqr


  !> Generic interface for the zqrm_spfct_trsm and zqrm_spfct_trsm1d  routines
  interface qrm_solve
     procedure                              :: zqrm_spfct_trsm1d, zqrm_spfct_trsm2d
  end interface qrm_solve

  interface qrm_solve_async
     procedure                              :: zqrm_spfct_trsm_async
  end interface qrm_solve_async

  !> Generic interface for the zqrm_spfct_trsm and zqrm_spfct_trsm1d  routines
  interface qrm_spfct_trsm
     procedure                              :: zqrm_spfct_trsm1d, zqrm_spfct_trsm2d
  end interface qrm_spfct_trsm

  interface qrm_spfct_trsm_async
     subroutine zqrm_spfct_trsm_async(qrm_dscr, qrm_spfct, transp, qrm_sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                  :: qrm_dscr
       type(zqrm_spfct_type), intent(in)    :: qrm_spfct
       type(zqrm_sdata_type)                :: qrm_sdata
       character(len=*), intent(in)         :: transp
     end subroutine zqrm_spfct_trsm_async
  end interface qrm_spfct_trsm_async

  interface qrm_spfct_trsm_bu_async
     subroutine zqrm_spfct_trsm_bu_async(qrm_dscr, qrm_spfct, transp, sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                        :: qrm_dscr
       type(zqrm_spfct_type), intent(in),  target :: qrm_spfct
       character(len=*), intent(in)               :: transp
       type(zqrm_sdata_type), target              :: sdata
     end subroutine zqrm_spfct_trsm_bu_async
  end interface qrm_spfct_trsm_bu_async

  interface qrm_spfct_trsm_td_async
     subroutine zqrm_spfct_trsm_td_async(qrm_dscr, qrm_spfct, transp, sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                        :: qrm_dscr
       type(zqrm_spfct_type), intent(in),  target :: qrm_spfct
       character(len=*), intent(in)               :: transp
       type(zqrm_sdata_type), target              :: sdata
     end subroutine zqrm_spfct_trsm_td_async
  end interface qrm_spfct_trsm_td_async

  interface qrm_spfct_unmqr_bu_async
     subroutine zqrm_spfct_unmqr_bu_async(qrm_dscr, qrm_spfct, transp, sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                        :: qrm_dscr
       type(zqrm_spfct_type), intent(in),  target :: qrm_spfct
       character(len=*), intent(in)               :: transp
       type(zqrm_sdata_type), target              :: sdata
     end subroutine zqrm_spfct_unmqr_bu_async
  end interface qrm_spfct_unmqr_bu_async

  interface qrm_spfct_unmqr_td_async
     subroutine zqrm_spfct_unmqr_td_async(qrm_dscr, qrm_spfct, transp, sdata)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       use qrm_dscr_mod
       type(qrm_dscr_type)                        :: qrm_dscr
       type(zqrm_spfct_type), intent(in),  target :: qrm_spfct
       character(len=*), intent(in)               :: transp
       type(zqrm_sdata_type), target              :: sdata
     end subroutine zqrm_spfct_unmqr_td_async
  end interface qrm_spfct_unmqr_td_async


  !> Generic interface for the zqrm_spfct_trsm2d and zqrm_spfct_trsm1d routines
  interface zqrm_spfct_trsm
     subroutine zqrm_spfct_trsm2d(qrm_spfct, transp, b, x, info)
       use zqrm_spfct_mod
       type(zqrm_spfct_type), target       :: qrm_spfct
       complex(r64), target                :: b(:,:)
       complex(r64), optional, target      :: x(:,:)
       character(len=*)                    :: transp
       integer, optional                   :: info
     end subroutine zqrm_spfct_trsm2d
     subroutine zqrm_spfct_trsm1d(qrm_spfct, transp, b, x, info)
       use zqrm_spfct_mod
       type(zqrm_spfct_type), target  :: qrm_spfct
       complex(r64), target           :: b(:)
       complex(r64), optional, target :: x(:)
       character(len=*)               :: transp
       integer, optional              :: info
     end subroutine zqrm_spfct_trsm1d
  end interface zqrm_spfct_trsm

  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Kernels
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  interface qrm_spfct_trsm_subtree
     subroutine zqrm_spfct_trsm_subtree(qrm_spfct, root, sdata, transp, info)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type), target  :: qrm_spfct
       type(zqrm_sdata_type), target  :: sdata
       integer                        :: root
       character(len=*), intent(in)   :: transp
       integer, optional              :: info
     end subroutine zqrm_spfct_trsm_subtree
  end interface qrm_spfct_trsm_subtree

  interface qrm_spfct_unmqr_subtree
     subroutine zqrm_spfct_unmqr_subtree(qrm_spfct, root, sdata, transp, work, info)
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type), target  :: qrm_spfct
       type(zqrm_sdata_type), target  :: sdata
       integer                        :: root
       character(len=*), intent(in)   :: transp
       type(zqrm_ws_type)             :: work
       integer, optional              :: info
     end subroutine zqrm_spfct_unmqr_subtree
  end interface qrm_spfct_unmqr_subtree

  interface qrm_spfct_trsm_activate_front
     subroutine zqrm_spfct_trsm_activate_front(qrm_spfct, front, front_rhs, nb, info)
       use zqrm_dsmat_mod
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type) :: qrm_spfct
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       integer               :: nb
       integer               :: info
     end subroutine zqrm_spfct_trsm_activate_front
  end interface qrm_spfct_trsm_activate_front

  interface qrm_spfct_unmqr_activate_front
     subroutine zqrm_spfct_unmqr_activate_front(qrm_spfct, front, front_rhs, nb, info)
       use zqrm_dsmat_mod
       use zqrm_spfct_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type) :: qrm_spfct
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       integer               :: nb
       integer               :: info
     end subroutine zqrm_spfct_unmqr_activate_front
  end interface qrm_spfct_unmqr_activate_front


  interface qrm_spfct_trsm_init_front
     subroutine zqrm_spfct_trsm_init_front(qrm_spfct, front, front_rhs, info)
       use qrm_dscr_mod
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type) :: qrm_spfct
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       integer               :: info
     end subroutine zqrm_spfct_trsm_init_front
  end interface qrm_spfct_trsm_init_front

  interface qrm_spfct_unmqr_init_front
     subroutine zqrm_spfct_unmqr_init_front(qrm_spfct, front, front_rhs, info)
       use qrm_dscr_mod
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_spfct_type) :: qrm_spfct
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       integer               :: info
     end subroutine zqrm_spfct_unmqr_init_front
  end interface qrm_spfct_unmqr_init_front



  interface qrm_spfct_trsm_clean_front
     subroutine zqrm_spfct_trsm_clean_front(front, front_rhs, lhs, transp, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       complex(r64)          :: lhs(:,:)
       character(len=*)      :: transp
       integer               :: info
     end subroutine zqrm_spfct_trsm_clean_front
  end interface qrm_spfct_trsm_clean_front
  
  interface qrm_spfct_unmqr_clean_front
     subroutine zqrm_spfct_unmqr_clean_front(front, front_rhs, lhs, transp, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       complex(r64)          :: lhs(:,:)
       character(len=*)      :: transp
       integer               :: info
     end subroutine zqrm_spfct_unmqr_clean_front
  end interface qrm_spfct_unmqr_clean_front
  
  interface qrm_spfct_trsm_init_block
     subroutine zqrm_spfct_trsm_init_block(front, front_rhs, transp, br, bc, rhs, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       character(len=*)      :: transp
       integer               :: br, bc
       complex(r64)          :: rhs(:,:)
       integer               :: info
     end subroutine zqrm_spfct_trsm_init_block
  end interface qrm_spfct_trsm_init_block

  interface qrm_spfct_unmqr_init_block
     subroutine zqrm_spfct_unmqr_init_block(front, front_rhs, transp, br, bc, rhs, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       character(len=*)      :: transp
       integer               :: br, bc
       complex(r64)          :: rhs(:,:)
       integer               :: info
     end subroutine zqrm_spfct_unmqr_init_block
  end interface qrm_spfct_unmqr_init_block
  

  interface qrm_spfct_trsm_clean_block
     subroutine zqrm_spfct_trsm_clean_block(front, front_rhs, transp, br, bc, lhs, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       character(len=*)      :: transp
       integer               :: br, bc
       complex(r64)          :: lhs(:,:)
       integer               :: info
     end subroutine zqrm_spfct_trsm_clean_block
  end interface qrm_spfct_trsm_clean_block

  interface qrm_spfct_unmqr_clean_block
     subroutine zqrm_spfct_unmqr_clean_block(front, front_rhs, transp, br, bc, lhs, info)
       use zqrm_spfct_mod
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       character(len=*)      :: transp
       integer               :: br, bc
       complex(r64)          :: lhs(:,:)
       integer               :: info
     end subroutine zqrm_spfct_unmqr_clean_block
  end interface qrm_spfct_unmqr_clean_block

  interface qrm_spfct_trsm_assemble_front
     subroutine zqrm_spfct_trsm_assemble_front(qrm_dscr, front, front_rhs, parent, parent_rhs, transp)
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(qrm_dscr_type)   :: qrm_dscr
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       type(zqrm_front_type) :: parent
       type(zqrm_rhs_type)   :: parent_rhs
       character(len=*)      :: transp
     end subroutine zqrm_spfct_trsm_assemble_front
  end interface qrm_spfct_trsm_assemble_front
  
  interface qrm_spfct_unmqr_assemble_front
     subroutine zqrm_spfct_unmqr_assemble_front(qrm_dscr, front, front_rhs, parent, parent_rhs, transp)
       use zqrm_fdata_mod
       use zqrm_sdata_mod
       type(qrm_dscr_type)   :: qrm_dscr
       type(zqrm_front_type) :: front
       type(zqrm_rhs_type)   :: front_rhs
       type(zqrm_front_type) :: parent
       type(zqrm_rhs_type)   :: parent_rhs
       character(len=*)      :: transp
     end subroutine zqrm_spfct_unmqr_assemble_front
  end interface qrm_spfct_unmqr_assemble_front
  
end module zqrm_solve_mod
