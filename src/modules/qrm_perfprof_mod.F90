!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################

#include "qrm_common.h"

module qrm_perfprof_mod

  use qrm_parameters_mod
  
  integer, parameter :: nitems=1000
  integer(kind=8)    :: qrm_perfprof_times(nitems)

  integer, parameter :: qrm_prof_factinit_          = 1
  integer, parameter :: qrm_prof_potrf_task_        = 2
  integer, parameter :: qrm_prof_gemm_task_         = 11
  integer, parameter :: qrm_prof_herk_task_         = 12
  integer, parameter :: qrm_prof_trsm_task_         = 13
  integer, parameter :: qrm_prof_lacpy_task_        = 14
  integer, parameter :: qrm_prof_nrm_task_          = 15
  integer, parameter :: qrm_prof_geqrt_task_        = 16
  integer, parameter :: qrm_prof_gemqrt_task_       = 17
  integer, parameter :: qrm_prof_tpqrt_task_        = 18
  integer, parameter :: qrm_prof_tpmqrt_task_       = 19
  integer, parameter :: qrm_prof_trmm_task_         = 20
  integer, parameter :: qrm_prof_fill_task_         = 21
  integer, parameter :: qrm_prof_addi_task_         = 22
  integer, parameter :: qrm_prof_sytrf_task_        = 23
  integer, parameter :: qrm_prof_syrk_task_         = 24
  integer, parameter :: qrm_prof_activate_          = 3
  integer, parameter :: qrm_prof_axpy_task_         = 4
  integer, parameter :: qrm_prof_init_block_task_   = 5
  integer, parameter :: qrm_prof_init_front_task_   = 6
  integer, parameter :: qrm_prof_clean_block_task_  = 7
  integer, parameter :: qrm_prof_clean_front_task_  = 8
  integer, parameter :: qrm_prof_do_subtree_        = 9
  integer, parameter :: qrm_prof_extadd_task_       = 10
  integer, parameter :: qrm_prof_activate1_         = 500
  integer, parameter :: qrm_prof_activate2_         = 501
  integer, parameter :: qrm_prof_activate3_         = 502
  integer, parameter :: qrm_prof_factinit1_         = 600
  integer, parameter :: qrm_prof_factinit2_         = 601
  integer, parameter :: qrm_prof_factinit3_         = 602
  integer, parameter :: qrm_prof_colmap1_           = 610
  integer, parameter :: qrm_prof_colmap2_           = 611
  integer, parameter :: qrm_prof_colmap3_           = 612
  

contains

  subroutine qrm_perfprof_init()

    times = 0.d0

    return
  end subroutine qrm_perfprof_init


  subroutine qrm_perfprof_finalize()
    implicit none

    integer :: i
    integer(kind=8) :: ts, tr

    call system_clock(ts, tr)


    __QRM_PRNT_MSG('("   ")')
    __QRM_PRNT_MSG('("==============================================================================")')
    __QRM_PRNT_MSG('("=========                 Performance profile report                ==========")')
    __QRM_PRNT_MSG('("   ")')
    
    do i=1, nitems
       if(qrm_perfprof_times(i).ne.0.d0) then
          select case (i)
          case (qrm_prof_factinit_)
             __QRM_PRNT_MSG('("    - Factorization init       :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_potrf_task_)
             __QRM_PRNT_MSG('("    - POTRF task               :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_gemm_task_)
             __QRM_PRNT_MSG('("    - GEMM task                :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_herk_task_)
             __QRM_PRNT_MSG('("    - HERK task                :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_trsm_task_)
             __QRM_PRNT_MSG('("    - TRSM task                :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_activate_)
             __QRM_PRNT_MSG('("    - Activate node            :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_axpy_task_)
             __QRM_PRNT_MSG('("    - AXPY task                :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_lacpy_task_)
             __QRM_PRNT_MSG('("    - LACPY task               :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_nrm_task_)
             __QRM_PRNT_MSG('("    - NRM task                 :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_geqrt_task_)
             __QRM_PRNT_MSG('("    - GEQRT task               :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_gemqrt_task_)
             __QRM_PRNT_MSG('("    - GEMQRT task              :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_tpqrt_task_)
             __QRM_PRNT_MSG('("    - TPQRT task               :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_TPMQRT_task_)
             __QRM_PRNT_MSG('("    - TPMQRT task              :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_TRMM_task_)
             __QRM_PRNT_MSG('("    - TRMM task                :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_extadd_task_)
             __QRM_PRNT_MSG('("    - EXTADD task              :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_init_block_task_)
             __QRM_PRNT_MSG('("    - Init block task          :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_init_front_task_)
             __QRM_PRNT_MSG('("    - Init node task           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_clean_block_task_)
             __QRM_PRNT_MSG('("    - Clean block task         :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_clean_front_task_)
             __QRM_PRNT_MSG('("    - Clean node task          :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_do_subtree_)
             __QRM_PRNT_MSG('("    - Do dubtree task          :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_activate1_)
             __QRM_PRNT_MSG('("    - Activate1 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_activate2_)
             __QRM_PRNT_MSG('("    - Activate2 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_activate3_)
             __QRM_PRNT_MSG('("    - Activate3 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_factinit1_)
             __QRM_PRNT_MSG('("    - Factinit1 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_factinit2_)
             __QRM_PRNT_MSG('("    - Factinit2 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_factinit3_)
             __QRM_PRNT_MSG('("    - Factinit3 node           :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_colmap1_)
             __QRM_PRNT_MSG('("    - Colmap1 node             :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_colmap2_)
             __QRM_PRNT_MSG('("    - Colmap2 node             :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          case (qrm_prof_colmap3_)
             __QRM_PRNT_MSG('("    - Colmap3 node             :",f10.4)') &
                  real(qrm_perfprof_times(i),kind(1.d0)) / real(tr,kind(1.d0))
          end select
       end if
    end do
    
  end subroutine qrm_perfprof_finalize
  
end module qrm_perfprof_mod
