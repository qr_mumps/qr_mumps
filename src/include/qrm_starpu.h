/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
** Copyright 2013-2015 UPS
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


#ifndef __QRM_STARPU_H__
#define __QRM_STARPU_H__

 /* These two types have to be interoperable. Always make sure they
    are aligned with the F90 counterparts in qrm_pthreads_mod.F90 and
    qrm_dscr_mod.F90 */

typedef struct {
  void *m;
} qrm_pthread_mutex;

typedef struct {
  int info;
  int ncpu;
  int ngpu;
  int seq; 
  int pr; 
  qrm_pthread_mutex mutex;
  int ctx;
} qrm_dscr_type;


#endif /* __QRM_STARPU_H__ */
