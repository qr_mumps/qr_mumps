! /* ##############################################################################################
! **
! ** Copyright 2012 CNRS, INPT
! **  
! ** This file is part of qr_mumps.
! **  
! ** qr_mumps is free software: you can redistribute it and/or modify
! ** it under the terms of the GNU Lesser General Public License as 
! ** published by the Free Software Foundation, either version 3 of 
! ** the License, or (at your option) any later version.
! **  
! ** qr_mumps is distributed in the hope that it will be useful,
! ** but WITHOUT ANY WARRANTY; without even the implied warranty of
! ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! ** GNU Lesser General Public License for more details.
! **  
! ** You can find a copy of the GNU Lesser General Public License
! ** in the qr_mumps/doc directory.
! **
! ** ##############################################################################################*/
!  
!  
! /*##############################################################################################*/
! /** @file qrm_common.h
!  * Common header file
!  *
!  */
! /* ############################################################################################## */

#define __QRM_PRNT_ERR(X)  if(qrm_eunit.gt.0) write(qrm_eunit,X)
#define __QRM_PRNT_MSG(X)  if(qrm_ounit.gt.0) write(qrm_ounit,X)
#define __QRM_PRNT_DBG(X)  if(qrm_dunit.gt.0) write(qrm_dunit,X)


#define QRM_VER_MAJOR 3
#define QRM_VER_MINOR 1
#define QRM_VER_PATCH 1

#define __QRM_INFO_CHECK(INFO,NAME,STR,GOTO)if(INFO.ne.0)then;\
call qrm_error_print(17,NAME,aed=STR,ied=(/INFO/));\
goto GOTO;\
endif

#define __QRM_INFO_SET(INFO,CODE,NAME,GOTO)  INFO=CODE; \
 call qrm_error_print(CODE,NAME); \
goto GOTO;

#define __QRM_INFO_SET_I(INFO,CODE,NAME,IED,GOTO)  INFO=CODE;   \
 call qrm_error_print(CODE,NAME,ied=IED); \
goto GOTO;

#define __QRM_INFO_SET_A(INFO,CODE,NAME,AED,GOTO)  INFO=CODE;   \
 call qrm_error_print(CODE,NAME,aed=AED); \
goto GOTO;

#define __QRM_IFPRESENT3(IA, A, DEF) if(present(A)) then;\
IA=A; else; IA=DEF; end if

#define __QRM_IFPRESENT4(IA, A, B, DEF) if(present(A)) then;\
IA=B; else; IA=DEF; end if


#if defined(perfprof)
#define __QRM_PROF_START(TS) call system_clock(TS) 
#define __QRM_PROF_STOP(EVENT,TS,TE) call system_clock(TE); qrm_perfprof_times(EVENT)=qrm_perfprof_times(EVENT)+TE-TS
#else
#define __QRM_PROF_START(TS)
#define __QRM_PROF_STOP(EVENT,TS,TE)
#endif
