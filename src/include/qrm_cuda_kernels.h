#include <cuda_runtime.h>
#include "cublas_v2.h"
#include <starpu.h>
#include <starpu_cublas_v2.h>
#if defined (starpu_ver14) && defined(have_cusolver)
#include <starpu_cusolver.h>
#include <cusolverDn.h>
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


#if defined (starpu_ver14) && defined(have_cusolver)
void zqrm_cuda_potrf_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        char uplo, int m, int k,
                        cuDoubleComplex *A, int lda,
                        cuDoubleComplex *W, int lw);


#if defined (have_cuda_qr_panel)
void zqrm_cuda_geqrt_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        int m, int n, int nb,
                        int *stair, 
                        cuDoubleComplex *A, int lda,
                        cuDoubleComplex *T, int ldt,
                        cuDoubleComplex *W, int lw);


void zqrm_cuda_tpqrt_in(cublasHandle_t cublas_handle,
                        cusolverDnHandle_t cusolver_handle, 
                        int m, int n, int l, int nb,
                        int *stair, 
                        cuDoubleComplex *A,  int lda,
                        cuDoubleComplex *B,  int ldb,
                        cuDoubleComplex *T,  int ldt,
                        cuDoubleComplex *W,  int lw);

void zqrm_cuda_laset(
    char uplo, int m, int n,
    cuDoubleComplex offdiag, cuDoubleComplex diag,
    cuDoubleComplex *dA, int ldda,
    cudaStream_t stream);

void zqrm_cuda_lastrow(
    int m, int n,
    cuDoubleComplex all, cuDoubleComplex last,
    cuDoubleComplex *dA, int ldda,
    cudaStream_t stream);
#endif
#endif

void  zqrm_cuda_gemqrt_gemm(cublasHandle_t handle,
                            cublasSideMode_t side,
                            cublasOperation_t trans,
                            int m, int n, int k, int nb,
                            int *stair, int ofsv,
                            const cuDoubleComplex *V,  int ldv,
                            const cuDoubleComplex *T,  int ldt,
                                  cuDoubleComplex *C,  int ldc,
                                  cuDoubleComplex *Wd, int lwd);


void  zqrm_cuda_tpmqrt_gemm(cublasHandle_t handle,
                            cublasSideMode_t side,
                            cublasOperation_t trans,
                            int m, int n, int k, int l, int nb,
                            int *stair,
                            const cuDoubleComplex *V,  int ldv,
                            const cuDoubleComplex *T,  int ldt,
                                  cuDoubleComplex *A,  int lda,
                                  cuDoubleComplex *B,  int ldb,
                                  cuDoubleComplex *Wd, int lwd);


void  zqrm_cuda_tprfb_gemm(cublasHandle_t handle,
                           cublasSideMode_t side,
                           cublasOperation_t trans,
                           int m, int n, int k,
                           const cuDoubleComplex *V,   int ldv,
                           const cuDoubleComplex *T,   int ldt,
                                 cuDoubleComplex *A,   int lda,
                                 cuDoubleComplex *B,   int ldb,
                                 cuDoubleComplex *Wd,  int lwd);


