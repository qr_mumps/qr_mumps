/* ##############################################################################################
**
** Copyright 2012 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


/**##############################################################################################
* @file qrm_test_solve_c.F90
* This file contains coverage tests for the C interface
*
##############################################################################################*/

#include "zqrm_mumps.h"
#include <string.h>
#include <stdio.h>

int zqrm_test_solve_c(struct zqrm_spmat_type_c *qrm_spmat, double _Complex *b, 
                      double _Complex *x, double _Complex *r, double eps, double *err){
}



int zqrm_test_methods_c(struct zqrm_spmat_type_c *qrm_spmat, double _Complex *b, 
                        double _Complex *x, double _Complex *r, double eps, double *err){

  double xnrm, bnrm, anrm, rnrm;
  int i, info;

  for(i=0; i<qrm_spmat->m; i++)
    r[i] = b[i];
  info = zqrm_vecnrm(b, qrm_spmat->m, 1, '2', bnrm); 
  if(info > 0) return info;

  if(qrm_spmat->m >= qrm_spmat->n) {
    info  = zqrm_least_squares_c(qrm_spmat, b, x, 1);
  } else {
    info = zqrm_min_norm_c(qrm_spmat, b, x, 1);
  }
  if(info > 0) return info;
  
  info = zqrm_residual_norm_c(qrm_spmat, r, x, 1, &rnrm);
  info = zqrm_spmat_nrm(qrm_spmat, 'f', &anrm); 
  info = zqrm_vecnrm(x, qrm_spmat->n, 1, '2', xnrm); 
  if(info > 0) return info;

  if(qrm_spmat->m >= qrm_spmat->n) {
    if(rnrm < eps) {
      *err = rnrm;
    } else {
      info = zqrm_residual_orth_c(qrm_spmat, r, 1, &onrm);
      *err = onrm;
    }
  } else {
    *err = rnrm;
  }
  
  if(err < err1)
    err = err1;

  return info;

}
