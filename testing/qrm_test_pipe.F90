!! ##############################################################################################
!!
!! Copyright 2012-2024 CNRS, INPT
!! Copyright 2013-2015 UPS
!!  
!! This file is part of qr_mumps.
!!  
!! qr_mumps is free software: you can redistribute it and/or modify
!! it under the terms of the GNU Lesser General Public License as 
!! published by the Free Software Foundation, either version 3 of 
!! the License, or (at your option) any later version.
!!  
!! qr_mumps is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU Lesser General Public License for more details.
!!  
!! You can find a copy of the GNU Lesser General Public License
!! in the qr_mumps/doc directory.
!!
!! ##############################################################################################


!! ##############################################################################################
!> @file qrm_test_pipe.F90
!! This file contains coverage tests for the factorization
!!
!!
!! ##############################################################################################

function zqrm_test_pipe(c, m) result(ok)
  use zqrm_testing_mod, protect => zqrm_test_pipe
  implicit none

  integer :: c, m
  logical :: ok

  integer, parameter :: ncases=15
  logical :: cases(ncases)

  if(c .eq. -1) then
     cases = .true.
  else if (c .le. ncases) then
     cases = .false.
     cases(c) = .true.
  end if

  ok = .true.
  
  if(cases(1 )) ok = case1 (m) .and. ok 
  if(cases(2 )) ok = case2 (m) .and. ok 

  return

contains

  !> @brief Test the factorization with different
  !! numbers of threads
  function case1(m) result(ok)
    use zqrm_mod
    implicit none

    integer :: m
    logical :: ok

    type(zqrm_spmat_type), pointer :: qrm_spmat
    type(zqrm_spfct_type)          :: qrm_spfct
    type(qrm_dscr_type)            :: qrm_dscr
    integer                        :: info
    complex(r64), allocatable         :: x(:), b(:), r(:)
    type(zqrm_sdata_type)          :: fwd, bwd
    real(r64)                      :: err, anrm, rnrm, bnrm, xnrm, onrm
    integer                        :: i, j
    character                      :: transp

    if((m.ne.-1) .and. &
         & (m.lt.11) .and. &
         & (m.gt.size(matrices))) then
       write(*,'("Matrix ",i2," is not available for this test")')m
       return
    end if

    ok = .true.
    
    ! loop over all the file-matrices
    do i=11, size(matrices)
       if((m.ne.-1) .and. &
            & (m.ne.i)) cycle

       info = 0

       ! get the data
       qrm_spmat => zqrm_get_test_mat(i)
       if(info.eq.0) call qrm_alloc(b, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(r, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(x, qrm_spmat%n, info)
       if(info.ne.0) goto 9999

       call qrm_vec_larnv(b)
       x = qrm_zzero
       r = b

       if(qrm_spmat%m .ge. qrm_spmat%n) then
          transp=qrm_no_transp
       else
          transp=qrm_conj_transp
       end if

       call qrm_dscr_init(qrm_dscr)
       call qrm_spfct_init(qrm_spfct, qrm_spmat, info)
       if(info.ne.0) goto 9999

       ! TODO: change error handling
       call qrm_analyse_async(qrm_dscr, qrm_spmat, qrm_spfct, transp)
       if(qrm_dscr%info.ne.0) goto 9998
       call qrm_factorize_async(qrm_dscr, qrm_spmat, qrm_spfct, transp)
       if(qrm_dscr%info.ne.0) goto 9998

       if(qrm_spmat%sym.eq.0) then
          if(transp .eq. qrm_no_transp) then
             call qrm_sdata_init(fwd, qrm_spfct, b, b)
             call qrm_sdata_init(bwd, qrm_spfct, x, b, fwd%front_slv)

             call qrm_apply_async(qrm_dscr, qrm_spfct, qrm_conj_transp, fwd)
             if(qrm_dscr%info.ne.0) goto 9998
             call qrm_solve_async(qrm_dscr, qrm_spfct, qrm_no_transp,   bwd)
             if(qrm_dscr%info.ne.0) goto 9998
          else if(transp .eq. qrm_conj_transp) then
             call qrm_sdata_init(fwd, qrm_spfct, x, b)
             call qrm_sdata_init(bwd, qrm_spfct, x, x, fwd%front_slv)

             call qrm_solve_async(qrm_dscr, qrm_spfct, qrm_conj_transp, fwd)
             if(qrm_dscr%info.ne.0) goto 9998
             call qrm_apply_async(qrm_dscr, qrm_spfct, qrm_no_transp,   bwd)
             if(qrm_dscr%info.ne.0) goto 9998
          end if
       else
          call qrm_sdata_init(fwd, qrm_spfct, x, b)
          call qrm_sdata_init(bwd, qrm_spfct, x, x, fwd%front_slv)

          call qrm_solve_async(qrm_dscr, qrm_spfct, qrm_conj_transp, fwd)
          if(qrm_dscr%info.ne.0) goto 9998
          call qrm_solve_async(qrm_dscr, qrm_spfct, qrm_no_transp,   bwd)
          if(qrm_dscr%info.ne.0) goto 9998
       end if

       
9998   continue
       call qrm_barrier(qrm_dscr)
       info = qrm_dscr%info
       if(info.ne.0) goto 9999


       call qrm_dscr_destroy(qrm_dscr)
       call zqrm_sdata_destroy(fwd)
       call zqrm_sdata_destroy(bwd)

       call qrm_residual_norm(qrm_spmat, r, x, rnrm)
       call qrm_vecnrm(x, size(x,1), '2', xnrm)
       call qrm_vecnrm(b, size(b,1), '2', bnrm)
       call qrm_spmat_nrm(qrm_spmat, 'f', anrm)

       if(transp .eq. qrm_no_transp) then
          if(rnrm.lt.eps) then
             err = rnrm
          else
             call qrm_residual_orth(qrm_spmat, r, onrm)
             err = onrm
          end if
       else if(transp .eq. qrm_conj_transp) then
             err = rnrm
       end if

9999   continue
       ! print message
       call zqrm_prnt_testmesg(7, "pipe.", 1, 1, i, (info .eq. 0) .and. (err .lt. eps))
       ok = ok .and. (info .eq. 0) .and. (err .lt. eps)

       ! destroy the sparse factorization object
       call zqrm_spfct_destroy(qrm_spfct)

       call qrm_dealloc(b)
       call qrm_dealloc(r)
       call qrm_dealloc(x)

    end do

    return

  end function case1

  !> @brief Test the factorization with different
  !! numbers of threads
  function case2(m) result(ok)
    use zqrm_mod
    implicit none

    integer :: m
    logical :: ok

    type(qrm_dscr_type)            :: qrm_dscr1, qrm_dscr2
    type(zqrm_spmat_type), pointer :: qrm_spmat
    type(zqrm_spfct_type)          :: qrm_spfct1, qrm_spfct2
    integer                        :: info
    complex(r64), allocatable      :: x1(:), b1(:), r1(:), x2(:), b2(:), r2(:)
    type(zqrm_sdata_type)          :: fwd1, fwd2, bwd1, bwd2, &
         x1_rhs, b1_rhs, r1_rhs, x2_rhs, b2_rhs, r2_rhs
    real(r64)                      :: err, anrm, rnrm, bnrm, xnrm, onrm
    integer                        :: i, j
    character                      :: transp

    if((m.ne.-1) .and. &
         & (m.lt.11) .and. &
         & (m.gt.size(matrices))) then
       write(*,'("Matrix ",i2," is not available for this test")')m
       return
    end if
    err = huge(qrm_ione)

    ok = .true.
    
    ! loop over all the file-matrices
    do i=11, size(matrices)
       if((m.ne.-1) .and. &
            & (m.ne.i)) cycle

       info = 0

       ! get the data
       qrm_spmat => zqrm_get_test_mat(i)
       if(info.eq.0) call qrm_alloc(b1, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(r1, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(x1, qrm_spmat%n, info)
       if(info.ne.0) goto 9999
       call qrm_vec_larnv(b1)
       x1 = qrm_zzero
       r1 = b1

       if(info.eq.0) call qrm_alloc(b2, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(r2, qrm_spmat%m, info)
       if(info.eq.0) call qrm_alloc(x2, qrm_spmat%n, info)
       if(info.ne.0) goto 9999
       call qrm_vec_larnv(b2)
       x2 = qrm_zzero
       r2 = b2

       call qrm_dscr_init(qrm_dscr1)
       call qrm_dscr_init(qrm_dscr2)

       call qrm_spfct_init(qrm_spfct1, qrm_spmat, info)
       if(info.ne.0) goto 9999

       call qrm_spfct_init(qrm_spfct2, qrm_spmat, info)
       if(info.ne.0) goto 9999

       
       if(qrm_spmat%m .ge. qrm_spmat%n) then
          transp=qrm_no_transp
       else
          transp=qrm_conj_transp
       end if

       call qrm_analyse_async(qrm_dscr1, qrm_spmat, qrm_spfct1, transp)
       if(info.ne.0) goto 9998
       call qrm_analyse_async(qrm_dscr2, qrm_spmat, qrm_spfct2, transp)
       if(info.ne.0) goto 9998

       call qrm_factorize_async(qrm_dscr1, qrm_spmat, qrm_spfct1, transp)
       if(info.ne.0) goto 9998
       call qrm_factorize_async(qrm_dscr2, qrm_spmat, qrm_spfct2, transp)
       if(info.ne.0) goto 9998

       
       if(qrm_spmat%sym.eq.0) then
          if(transp .eq. qrm_no_transp) then
             call zqrm_sdata_init(fwd1, qrm_spfct1, b1, b1)
             call zqrm_sdata_init(bwd1, qrm_spfct1, x1, b1, fwd1%front_slv)
             call zqrm_sdata_init(fwd2, qrm_spfct2, b2, b2)
             call zqrm_sdata_init(bwd2, qrm_spfct2, x2, b2, fwd2%front_slv)

             call qrm_apply_async(qrm_dscr1, qrm_spfct1, qrm_conj_transp, fwd1)
             if(qrm_dscr1%info.ne.0) goto 9998
             call qrm_solve_async(qrm_dscr1, qrm_spfct1, qrm_no_transp,   bwd1)
             if(qrm_dscr1%info.ne.0) goto 9998
             call qrm_apply_async(qrm_dscr2, qrm_spfct2, qrm_conj_transp, fwd2)
             if(qrm_dscr2%info.ne.0) goto 9998
             call qrm_solve_async(qrm_dscr2, qrm_spfct2, qrm_no_transp,   bwd2)
             if(qrm_dscr2%info.ne.0) goto 9998
          else if(transp .eq. qrm_conj_transp) then
             call zqrm_sdata_init(fwd1, qrm_spfct1, x1, b1)
             call zqrm_sdata_init(bwd1, qrm_spfct1, x1, x1, fwd1%front_slv)
             call zqrm_sdata_init(fwd2, qrm_spfct2, x2, b2)
             call zqrm_sdata_init(bwd2, qrm_spfct2, x2, x2, fwd2%front_slv)

             call qrm_solve_async(qrm_dscr1, qrm_spfct1, qrm_conj_transp, fwd1)
             if(qrm_dscr1%info.ne.0) goto 9998
             call qrm_apply_async(qrm_dscr1, qrm_spfct1, qrm_no_transp,   bwd1)
             if(qrm_dscr1%info.ne.0) goto 9998
             call qrm_solve_async(qrm_dscr2, qrm_spfct2, qrm_conj_transp, fwd2)
             if(qrm_dscr2%info.ne.0) goto 9998
             call qrm_apply_async(qrm_dscr2, qrm_spfct2, qrm_no_transp,   bwd2)
             if(qrm_dscr2%info.ne.0) goto 9998
          end if
       else
          call zqrm_sdata_init(fwd1, qrm_spfct1, x1, b1)
          call zqrm_sdata_init(bwd1, qrm_spfct1, x1, x1, fwd1%front_slv)
          call zqrm_sdata_init(fwd2, qrm_spfct2, x2, b2)
          call zqrm_sdata_init(bwd2, qrm_spfct2, x2, x2, fwd2%front_slv)
          
          call qrm_solve_async(qrm_dscr1, qrm_spfct1, qrm_conj_transp, fwd1)
          if(qrm_dscr1%info.ne.0) goto 9998
          call qrm_solve_async(qrm_dscr1, qrm_spfct1, qrm_no_transp,   bwd1)
          if(qrm_dscr1%info.ne.0) goto 9998
          call qrm_solve_async(qrm_dscr2, qrm_spfct2, qrm_conj_transp, fwd2)
          if(qrm_dscr2%info.ne.0) goto 9998
          call qrm_solve_async(qrm_dscr2, qrm_spfct2, qrm_no_transp,   bwd2)
          if(qrm_dscr2%info.ne.0) goto 9998
       end if

9998   continue
       call qrm_barrier(qrm_dscr1)
       call qrm_barrier(qrm_dscr2)

       info = merge(info, qrm_dscr1%info, info.ne.0)
       info = merge(info, qrm_dscr2%info, info.ne.0)
       if(info.ne.0) goto 9999

       call qrm_dscr_destroy(qrm_dscr1)
       call qrm_dscr_destroy(qrm_dscr2)

       call zqrm_sdata_destroy(fwd1)
       call zqrm_sdata_destroy(bwd1)
       call zqrm_sdata_destroy(fwd2)
       call zqrm_sdata_destroy(bwd2)

       call qrm_residual_norm(qrm_spmat, r1, x1, rnrm)
       call qrm_vecnrm(x1, size(x1,1), '2', xnrm)
       call qrm_vecnrm(b1, size(b1,1), '2', bnrm)
       call qrm_spmat_nrm(qrm_spmat, 'f', anrm)

       if(transp .eq. qrm_no_transp) then
          if(rnrm.lt.eps) then
             err = rnrm
          else
             call qrm_residual_orth(qrm_spmat, r1, onrm)
             err = onrm
          end if
       else if(transp .eq. qrm_conj_transp) then
             err = rnrm
       end if

       call qrm_residual_norm(qrm_spmat, r2, x2, rnrm)
       call qrm_vecnrm(x2, size(x2,1), '2', xnrm)
       call qrm_vecnrm(b2, size(b2,1), '2', bnrm)

       if(transp .eq. qrm_no_transp) then
          if(rnrm.lt.eps) then
             err = max(rnrm,err)
          else
             call qrm_residual_orth(qrm_spmat, r2, onrm)
             err = max(onrm,err)
          end if
       else if(transp .eq. qrm_conj_transp) then
             err = max(rnrm,err)
       end if

9999   continue
       ! print message
       call zqrm_prnt_testmesg(7, "pipe.", 2, 1, i, (info .eq. 0) .and. (err .lt. eps))
       ok = ok .and. (info .eq. 0) .and. (err .lt. eps)

       ! destroy the sparse factorization objects
       call zqrm_spfct_destroy(qrm_spfct1)
       call zqrm_spfct_destroy(qrm_spfct2)
       

       call qrm_dealloc(b1)
       call qrm_dealloc(r1)
       call qrm_dealloc(x1)
       call qrm_dealloc(b2)
       call qrm_dealloc(r2)
       call qrm_dealloc(x2)

    end do

    return

  end function case2


end function zqrm_test_pipe
