/* ##############################################################################################
**
** Copyright 2012-2024 CNRS, INPT
**  
** This file is part of qr_mumps.
**  
** qr_mumps is free software: you can redistribute it and/or modify
** it under the terms of the GNU Lesser General Public License as 
** published by the Free Software Foundation, either version 3 of 
** the License, or (at your option) any later version.
**  
** qr_mumps is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Lesser General Public License for more details.
**  
** You can find a copy of the GNU Lesser General Public License
** in the qr_mumps/doc directory.
**
** ##############################################################################################*/


/**##############################################################################################
* @file qrm_test_aux.F90
* This file contains coverage tests for the C interface
*
##############################################################################################*/

#include "zqrm_c.h"
#include <string.h>
#include <stdio.h>

int zqrm_test_solve_c(int m, int n, int nz, int sym,
                      int *irn, int *jcn, double _Complex *val,
                      double _Complex *b, double _Complex *x, double _Complex *r,
                      double eps, double *err){

  struct zqrm_spmat_type_c qrm_spmat;
  struct zqrm_spfct_type_c qrm_spfct;
  double xnrm, bnrm, anrm, rnrm, onrm;
  int i, info;
  char transp;

  qrm_spmat.m   = m;
  qrm_spmat.n   = n;
  qrm_spmat.nz  = nz;
  qrm_spmat.irn = irn;
  qrm_spmat.jcn = jcn;
  qrm_spmat.val = val;
  qrm_spmat.sym = sym;

  info = zqrm_spmat_init_c(&qrm_spmat);
  if(info > 0) return info;
  
  info = zqrm_spfct_init_c(&qrm_spfct, &qrm_spmat);
  if(info > 0) return info;

  for(i=0; i<qrm_spmat.m; i++)
    r[i] = b[i];
  info = zqrm_vecnrm_c(b, qrm_spmat.m, 1, '2', &bnrm);
  if(info > 0) return info;

  if(qrm_spmat.m >= qrm_spmat.n) {
    transp = qrm_no_transp_c;
  } else {
    transp = qrm_conj_transp_c;
  }

  info = zqrm_analyse_c(&qrm_spmat, &qrm_spfct, transp);
  info = zqrm_factorize_c(&qrm_spmat, &qrm_spfct, transp);

  if(qrm_spmat.m >= qrm_spmat.n && qrm_spmat.sym>0) {
    info = zqrm_spfct_trsm_c(&qrm_spfct, qrm_conj_transp_c, r, b, 1);
    info = zqrm_spfct_trsm_c(&qrm_spfct, qrm_no_transp_c, b, x, 1);
  } else if(qrm_spmat.m >= qrm_spmat.n) {
    info = zqrm_spfct_unmqr_c(&qrm_spfct, qrm_conj_transp_c, b, 1);
    info = zqrm_spfct_trsm_c(&qrm_spfct, qrm_no_transp_c, b, x, 1);
  } else {
    info = zqrm_spfct_trsm_c(&qrm_spfct, qrm_conj_transp_c, b, x, 1);
    info = zqrm_spfct_unmqr_c(&qrm_spfct, qrm_no_transp_c, x, 1);
  }
  if(info > 0) return info;
  
  info = zqrm_residual_norm_c(&qrm_spmat, r, x, 1, &rnrm, 'n');
  info = zqrm_residual_orth_c(&qrm_spmat, r, 1, &onrm, 'n');
  info = zqrm_spmat_nrm_c(&qrm_spmat, 'f', &anrm);
  info = zqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  if(info > 0) return info;

  if(qrm_spmat.m >= qrm_spmat.n) {
    if(rnrm < eps) {
      *err = rnrm;
    } else {
      *err = onrm;
    }
  } else {
    *err = rnrm;
  }

  info = zqrm_spfct_destroy_c(&qrm_spfct);
  if(info > 0) return info;
  
  return info;


}


int zqrm_test_methods_c(int m, int n, int nz, int sym,
                        int *irn, int *jcn, double _Complex *val,
                        double _Complex *b, double _Complex *x, double _Complex *r,
                        double eps, double *err){


  struct zqrm_spmat_type_c qrm_spmat;
  double xnrm, bnrm, anrm, rnrm, onrm;
  int i, info;

  qrm_spmat.m   = m;
  qrm_spmat.n   = n;
  qrm_spmat.nz  = nz;
  qrm_spmat.irn = irn;
  qrm_spmat.jcn = jcn;
  qrm_spmat.val = val;
  qrm_spmat.sym = sym;
  
  info = zqrm_spmat_init_c(&qrm_spmat);
  if(info > 0) return info;

  for(i=0; i<qrm_spmat.m; i++)
    r[i] = b[i];
  info = zqrm_vecnrm_c(b, qrm_spmat.m, 1, '2', &bnrm);
  if(info > 0) return info;

  if(qrm_spmat.m >= qrm_spmat.n && qrm_spmat.sym>0) {
    info = zqrm_spposv_c(&qrm_spmat, b, x, 1);
  } else if(qrm_spmat.m >= qrm_spmat.n) {
    info  = zqrm_least_squares_c(&qrm_spmat, b, x, 1, 'n');
  } else {
    info = zqrm_min_norm_c(&qrm_spmat, b, x, 1, 'n');
  }
  if(info > 0) return info;
  
  info = zqrm_residual_norm_c(&qrm_spmat, r, x, 1, &rnrm, 'n');
  info = zqrm_residual_orth_c(&qrm_spmat, r, 1, &onrm, 'n');
  info = zqrm_spmat_nrm_c(&qrm_spmat, 'f', &anrm);
  info = zqrm_vecnrm_c(x, qrm_spmat.n, 1, '2', &xnrm);
  if(info > 0) return info;

  if(qrm_spmat.m >= qrm_spmat.n) {
    if(rnrm < eps) {
      *err = rnrm;
    } else {
      *err = onrm;
    }
  } else {
    *err = rnrm;
  }
  
  return info;

}
