#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT
##  
## This file is part of qr_mumps.
##  
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as 
## published by the Free Software Foundation, either version 3 of 
## the License, or (at your option) any later version.
##  
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##  
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################


# This file is used to generate the double-precision, complex version
# of all the typed files/routines
use strict;
use warnings;
use 5.010;

my @ariths = ('', 'z', 'c', 'd', 's');

my ($idx) = grep { $ariths[$_] eq $ARGV[1] } (0 .. @ariths-1);
# say defined $idx ? $idx : -1;

open my $fh, $ARGV[0] or die "Could not open $ARGV[0]: $!";


my @prefs = (
    ['zqrm_'            , 'zqrm_'            , 'cqrm_'           , 'dqrm_'         , 'sqrm_'       ]
    );


my @names = (
    # BLAS stuff
    ['zgemm'            , 'zgemm'            , 'cgemm'           , 'dgemm'         , 'sgemm'       ],
    ['ztrsm'            , 'ztrsm'            , 'ctrsm'           , 'dtrsm'         , 'strsm'       ],
    ['ztrmm'            , 'ztrmm'            , 'ctrmm'           , 'dtrmm'         , 'strmm'       ],
    ['zpotrf'           , 'zpotrf'           , 'cpotrf'          , 'dpotrf'        , 'spotrf'      ],
    ['zsytrf_nopiv'     , 'zsytrf_nopiv'     , 'csytrf_nopiv'    , 'dsytrf_nopiv'  , 'ssytrf_nopiv'],
    ['zscal'            , 'zscal'            , 'cscal'           , 'dscal'         , 'sscal'       ],
    ['zherk'            , 'zherk'            , 'cherk'           , 'dsyrk'         , 'ssyrk'       ],
    ['zsyrk'            , 'zsyrk'            , 'csyrk'           , 'dsyrk'         , 'ssyrk'       ],
    ['ztpmqrt'          , 'ztpmqrt'          , 'ctpmqrt'         , 'dtpmqrt'       , 'stpmqrt'     ],
    ['ztprfb'           , 'ztprfb'           , 'ctprfb'          , 'dtprfb'        , 'stprfb'      ],
    ['zgeqrt3'          , 'zgeqrt3'          , 'cgeqrt3'         , 'dgeqrt3'       , 'sgeqrt3'     ],
    ['zgeqrt2'          , 'zgeqrt2'          , 'cgeqrt2'         , 'dgeqrt2'       , 'sgeqrt2'     ],
    ['zgeqrt'           , 'zgeqrt'           , 'cgeqrt'          , 'dgeqrt'        , 'sgeqrt'      ],
    ['zgemqrt'          , 'zgemqrt'          , 'cgemqrt'         , 'dgemqrt'       , 'sgemqrt'     ],
    ['zlarfb'           , 'zlarfb'           , 'clarfb'          , 'dlarfb'        , 'slarfb'      ],
    ['ztpqrt'           , 'ztpqrt'           , 'ctpqrt'          , 'dtpqrt'        , 'stpqrt'      ],
    ['ztmpqrt'          , 'ztmpqrt'          , 'ctpmqrt'         , 'dtpmqrt'       , 'stpmqrt'     ],
    ['ztpqrt2'          , 'ztpqrt2'          , 'ctpqrt2'         , 'dtpqrt2'       , 'stpqrt2'     ],
    ['zlassq'           , 'zlassq'           , 'classq'          , 'dlassq'        , 'slassq'      ],
    ['zlarnv'           , 'zlarnv'           , 'clarnv'          , 'dlarnv'        , 'slarnv'      ],
    ['zlaset'           , 'zlaset'           , 'claset'          , 'dlaset'        , 'slaset'      ],
    ['zlastrow'         , 'zlastrow'         , 'clastrow'        , 'dlastrow'      , 'slastrow'    ],
    ['dznrm2'           , 'dznrm2'           , 'scnrm2'          , 'dnrm2'         , 'snrm2'       ],
                                                                                                   
    # Fortran stuff                                                                                
    ['qrm_zzero'        , 'qrm_zzero'        , 'qrm_czero'       , 'qrm_dzero'     , 'qrm_szero'   ],
    ['qrm_zmone'        , 'qrm_zmone'        , 'qrm_cmone'       , 'qrm_dmone'     , 'qrm_smone'   ],
    ['qrm_zone'         , 'qrm_zone'         , 'qrm_cone'        , 'qrm_done'      , 'qrm_sone'    ],
    ['qrm_ztwo'         , 'qrm_ztwo'         , 'qrm_ctwo'        , 'qrm_dtwo'      , 'qrm_stwo'    ],
    ['qrm_dzero'        , 'qrm_dzero'        , 'qrm_szero'       , 'qrm_dzero'     , 'qrm_szero'   ],
    ['qrm_done'         , 'qrm_done'         , 'qrm_sone'        , 'qrm_done'      , 'qrm_sone'    ],
    ['qrm_dmone'        , 'qrm_dmone'        , 'qrm_smone'       , 'qrm_dmone'     , 'qrm_smone'   ],
    ['qrm_dtwo'         , 'qrm_dtwo'         , 'qrm_stwo'        , 'qrm_dtwo'      , 'qrm_stwo'    ],
    ['complex\(r64\)'   , 'complex(r64)'     , 'complex(r32)'    , 'real(r64)'     , 'real(r32)'   ],
    ['real\(r64\)'      , 'real(r64)'        , 'real(r32)'       , 'real(r64)'     , 'real(r32)'   ],
    ['zqrm_sizeof_data' , 'qrm_sizeof_z'     , 'qrm_sizeof_c'    , 'qrm_sizeof_d'  , 'qrm_sizeof_s'],
                                                                                                   
    # CUDA                                                                                         
    ['cublasZherk'      , 'cublasZherk'      , 'cublasCherk'     , 'cublasDsyrk'      , 'cublasSsyrk'      ],
    ['cublasZsyrk'      , 'cublasZsyrk'      , 'cublasCsyrk'     , 'cublasDsyrk'      , 'cublasSsyrk'      ],
    ['cublasZgemm'      , 'cublasZgemm'      , 'cublasCgemm'     , 'cublasDgemm'      , 'cublasSgemm'      ],
    ['cublasZtrsm'      , 'cublasZtrsm'      , 'cublasCtrsm'     , 'cublasDtrsm'      , 'cublasStrsm'      ],
    ['cusolverDnZgeqrf' , 'cusolverDnZgeqrf' , 'cusolverDnCgeqrf', 'cusolverDnDgeqrf' , 'cusolverDnSgeqrf' ],
    ['cusolverDnZlarft' , 'cusolverDnZlarft' , 'cusolverDnClarft', 'cusolverDnDlarft' , 'cusolverDnSlarft' ],
    ['cusolverDnZpotrf' , 'cusolverDnZpotrf' , 'cusolverDnCpotrf', 'cusolverDnDpotrf' , 'cusolverDnSpotrf' ],
    ['cuDoubleComplex'  , 'cuDoubleComplex'  , 'cuFloatComplex'  , 'double'           , 'float'            ],
    ['cublasZ'          , 'cublasZ'          , 'cublasC'         , 'cublasD'          , 'cublasS'          ],
    ['CUBLAS_OP_C'      , 'CUBLAS_OP_C'      , 'CUBLAS_OP_C'     , 'CUBLAS_OP_T'      , 'CUBLAS_OP_T'      ],
                                                                                                   
    # C stuff                                                                                      
    ['qrm_zone_c'       , 'qrm_zone_c'       , 'qrm_cone_c'      , 'qrm_done_c'    , 'qrm_sone_c'  ],
    ['qrm_zzero_c'      , 'qrm_zzero_c'      , 'qrm_czero_c'     , 'qrm_dzero_c'   , 'qrm_szero_c' ],
    ['qrm_zmone_c'      , 'qrm_zmone_c'      , 'qrm_cmone_c'     , 'qrm_dmone_c'   , 'qrm_smone_c' ],
    ['double _Complex'  , 'double _Complex'  , 'float _Complex'  , 'double'        , 'float'       ],
    ['double'           , 'double'           , 'float'           , 'double'        , 'float'       ],
                                                                                                   
    # Various stuff                                                                                
    ['qrm_conj_transp_c', 'qrm_conj_transp_c', 'qrm_conj_transp_c', 'qrm_transp_c' , 'qrm_transp_c'],
    ['conjg'            , 'conjg'            , 'conjg'           , ''              , ''            ],
    ['qrm_conj_transp'  , 'qrm_conj_transp'  , 'qrm_conj_transp' , 'qrm_transp'    , 'qrm_transp'  ]
    );

my @codes = (
    ['cmplx\(([a-z0-9]*\.?[a-z0-9]*),([a-z0-9]*\.?[a-z0-9]*),(\w+)\)', '"cmplx($1,$2,r64)"' , '"cmplx($1,$2,r32)"' , '"real($1,r64)"' , '"real($1,r32)"'        ]
    );
    
while (<$fh>) {
    foreach my $name (@names) {
        s/$name->[0]/$name->[$idx]/g;
    }

    foreach my $pref (@prefs) {
        s/$pref->[0]/$pref->[$idx]/g;
    }

    foreach my $code (@codes) {
        s/$code->[0]/$code->[$idx]/gee;
    }
    print;
}
