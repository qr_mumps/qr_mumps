#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT
##  
## This file is part of qr_mumps.
##  
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as 
## published by the Free Software Foundation, either version 3 of 
## the License, or (at your option) any later version.
##  
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##  
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################

my $intmpl = 0;
my $tmpl   = "";
my $key    = "";
my %templates = ();

while (<>) {
    my $line = $_; # or simply "print;"

    if ($intmpl eq 0) {
        if (index($line, "!beg_template:") != -1) {
            my @words = split / /, $line;
            $key = @words[1];
            $key =~ s/\R//g;
            # print "Found template for ",@words[1];
            $intmpl = 1;
            $tmpl = "";
        } 
    } else {
        if (index($line, "!end_template") != -1) {
            $intmpl = 0;
            # print "\n\nHere is a template for ",$key,"\n";
            # print $tmpl;
            $templates{$key} .= $tmpl;
        } else {
            $tmpl = $tmpl.$line;
        }
    } 
}



@arrays = (

    { pref    => "p",
      check   => "associated",
      array   => "pointer"    ,
      tgt     => "",
      move    => "dst => src; nullify(src)" ,
      alloc   => "allocate(a(#sizes#), stat=err)",
      pinnedalloc => "if(present(pin)) then\n".
                     "     if(pin) then\n".
                     "        call qrm_starpu_pinalloc(ptr, int(#mult#*qrm_sizeof_#arith#, c_size_t), err)\n".
                     "        call c_f_pointer(ptr, a, (/#sizes#/))\n".
                     "     else\n".
                     "        allocate(a(#sizes#), stat=err)\n".
                     "     end if\n".
                     "  else\n".
                     "     allocate(a(#sizes#), stat=err)\n".
                     "  end if",
      dealloc => "deallocate(a, stat=err); nullify(a)",
      pinneddealloc => "if(present(pin)) then\n".
                       "   if(pin) then\n".
                       "      call qrm_starpu_pinfree(c_loc(a), int(size(a)*qrm_sizeof_#arith#, c_size_t), err)\n".
                       "   else\n".
                       "      deallocate(a, stat=err)\n".
                       "   end if\n".
                       "else\n".
                       "    deallocate(a, stat=err)\n".
                       "end if\n".
                       "nullify(a)"
    },

    { pref    => "a",
      check   => "allocated",
      array   => "allocatable",
      tgt     => "target,",
      move    => "call move_alloc(src, dst)",
      alloc   => "allocate(a(#sizes#), stat=err)",
      pinnedalloc   => "allocate(a(#sizes#), stat=err)\n".
                       "  if(present(pin)) then\n".
                       "     if(pin) then\n".
                       "        call qrm_pin_mem(c_loc(a), int(#mult#*qrm_sizeof_#arith#, c_size_t))\n".
                       "     end if\n".
                       "  end if",
      dealloc => "deallocate(a, stat=err)",
      pinneddealloc => "if(present(pin)) then\n".
                       "   if(pin) then\n".
                       "      call qrm_unpin_mem(c_loc(a))\n".
                       "   end if\n".
                       "end if\n".
                       "deallocate(a, stat=err)"
    }
    );

@ariths = (
    { arith => "s" , type => "real",    prec => "(r32)" },
    { arith => "d" , type => "real",    prec => "(r64)" },
    { arith => "c" , type => "complex", prec => "(r32)" },
    { arith => "z" , type => "complex", prec => "(r64)" },
    { arith => "i" , type => "integer", prec => "(i32)" },
    { arith => "i8", type => "integer", prec => "(i64)" }
    );

@sizes = (
    { rank => "1", dim => ":"    , sizes => "m"   ,    mult => "m"    , checksize => "m .le. 0" },
    { rank => "2", dim => ":,:"  , sizes => "m, n",    mult => "m*n"  , checksize => "(m .le. 0) .or. (n .le. 0)" },
    { rank => "3", dim => ":,:,:", sizes => "m, n, k", mult => "m*n*k", checksize => "(m .le. 0) .or. (n .le. 0) .or. (k .le. 0)" }
    );

# my @infaces = ("alloc", "dealloc", "move_alloc", "size", "realloc" );
# my %templates = (alloc => $talloc, dealloc => $tdealloc, move_alloc => $tmovealloc, size => $tsize, realloc => $trealloc);

print "module qrm_mem_mod\n"  ;
# print "  use iso_c_binding\n"  ;
# print "  use qrm_error_mod\n"  ;
# print "  use qrm_parameters_mod\n"  ;
# print "  use qrm_memhandling_mod\n";
# print "  use qrm_pthread_mod\n";
# print "#if defined (have_cuda)\n";
# print "  use qrm_starpu_common_mod\n\n";
# print "#endif\n";


my $ns = scalar(@sizes);
    
foreach my $key ( keys %templates )
{
    print "  interface qrm_",$key,"\n";
    foreach my $arr (@arrays)
    {
        foreach my $ari (@ariths)
        {
            print "    module procedure ";
            my $cnt = 0;
            foreach my $size (@sizes)
            {
                my %hash = (%{$arr}, %{$ari}, %{$size});
                print " qrm_",$hash{"pref"},$key,"_",$hash{"rank"},$hash{"arith"};
                if($key eq "realloc")
                {
                    last;
                }
                if($cnt < $ns-1)
                {
                    print ",";
                }
                $cnt = $cnt+1;
            }
            print "\n";
        }
    }
    print "  end interface\n\n";

    foreach my $arr (@arrays)
    {
        my %arrhash = (%{$arr});
        print "  interface qrm_",$arrhash{pref},$key,"\n";
        foreach my $ari (@ariths)
        {
            print "    module procedure ";
            my $cnt = 0;
            foreach my $size (@sizes)
            {
                my %hash = (%{$arr}, %{$ari}, %{$size});
                print " qrm_",$hash{"pref"},$key,"_",$hash{"rank"},$hash{"arith"};
                if($key eq "realloc")
                {
                    last;
                }
                if($cnt < $ns-1)
                {
                    print ",";
                }
                $cnt = $cnt+1;
            }
            print "\n";
        }
        print "  end interface\n\n";
    }

}




print "\n\ncontains\n\n";


foreach my $key ( keys %templates )
{
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"; 
    print "!                     ",$key," routines                           !\n"; 
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"; 
    foreach my $arr (@arrays)
    {
        foreach my $ari (@ariths)
        {
            foreach my $size (@sizes)
            {
                my %hash = (%{$arr}, %{$ari}, %{$size});
                # print Dumper(\%hash), "\n";
                $template = $templates{$key};
                if($hash{"pref"} eq "p")
                {
                    $template =~ s/\#ifp(.+?)\#fip/$1/sg;
                } else {
                    $template =~ s/\#ifp(.+?)\#fip//sg;
                }
                $template =~ s/\#(.*?)\#/$hash{"\L$1"}/g;
                $template =~ s/\#(.*?)\#/$hash{"\L$1"}/g; # second pass
                print $template, "\n";
                if($key eq "realloc")
                {
                    last;
                }
            }
        }
    }
}

print "\nend module qrm_mem_mod"
