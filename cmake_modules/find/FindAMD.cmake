#################################################################################################
##
## Copyright 2012-2024 CNRS, INPT, Inria
##
## This file is part of qr_mumps.
##
## qr_mumps is free software: you can redistribute it and/or modify
## it under the terms of the GNU Lesser General Public License as
## published by the Free Software Foundation, either version 3 of
## the License, or (at your option) any later version.
##
## qr_mumps is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Lesser General Public License for more details.
##
## You can find a copy of the GNU Lesser General Public License
## in the qr_mumps/doc directory.
##
#################################################################################################

#.rst:
# FindSuiteSparse
# -------------
#
#
#
# $SUITESPARSE_DIR is an environment variable
# Created by Alfredo Buttari.

if(AMD_LIB)
  set(amd_libs ${AMD_LIB})
elseif(DEFINED ENV{AMD_LIB})
  message(STATUS "Using the AMD_LIB environment variable")
  set(amd_libs $ENV{AMD_LIB})
else()
  set(amd_libs "amd;colamd")
endif()


set(ENV_AMD_DIR     "$ENV{AMD_DIR}")
set(ENV_AMD_LIBDIR "$ENV{AMD_LIBDIR}")
set(ENV_AMD_INCDIR "$ENV{AMD_INCDIR}")

# First look for header files

unset(_inc_env)
if(ENV_AMD_INCDIR)
  list(APPEND _inc_env "${ENV_AMD_INCDIR}")
elseif(ENV_AMD_DIR)
  list(APPEND _inc_env "${ENV_AMD_DIR}")
  list(APPEND _inc_env "${ENV_AMD_DIR}/include")
else()
  if(WIN32)
    string(REPLACE ":" ";" _inc_env "$ENV{INCLUDE}")
  else()
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{C_INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{CPATH}")
    list(APPEND _inc_env "${_path_env}")
    string(REPLACE ":" ";" _path_env "$ENV{INCLUDE_PATH}")
    list(APPEND _inc_env "${_path_env}")
  endif()
  list(APPEND _inc_env "${_path_env}")
endif()
list(APPEND _inc_env "${CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES}")
list(REMOVE_DUPLICATES _inc_env)


if(AMD_INCDIR)
  find_path(AMD_INCLUDE_DIRS amd.h colamd.h
    HINTS ${AMD_INCDIR}
    PATH_SUFFIXES suitesparse)
else()
  if(AMD_DIR)
    find_path(AMD_INCLUDE_DIRS amd.h colamd.h
      HINTS ${AMD_DIR}
      PATH_SUFFIXES include include/suitesparse)
  else()
    find_path(AMD_INCLUDE_DIRS amd.h colamd.h
      HINTS ${_inc_env}
      PATH_SUFFIXES suitesparse)
  endif()
endif()


# Second look lib files
unset(_lib_env)
if(ENV_AMD_LIBDIR)
  list(APPEND _lib_env "${ENV_AMD_LIBDIR}")
elseif(ENV_AMD_DIR)
  list(APPEND _lib_env "${ENV_AMD_DIR}")
  list(APPEND _lib_env "${ENV_AMD_DIR}/lib")
else()
  list(APPEND _lib_env "$ENV{LIBRARY_PATH}")
  if(WIN32)
    string(REPLACE ":" ";" _lib_env2 "$ENV{LIB}")
  elseif(APPLE)
    string(REPLACE ":" ";" _lib_env2 "$ENV{DYLD_LIBRARY_PATH}")
  else()
    string(REPLACE ":" ";" _lib_env2 "$ENV{LD_LIBRARY_PATH}")
  endif()
  list(APPEND _lib_env "${_lib_env2}")
  list(APPEND _lib_env "${CMAKE_C_IMPLICIT_LINK_DIRECTORIES}")
endif()
list(REMOVE_DUPLICATES _lib_env)

if(AMD_LIBDIR)
  foreach(amd_lib ${amd_libs})
    set(AMD_${amd_lib}_LIBRARY "AMD_${amd_lib}_LIBRARY-NOTFOUND")
    find_library(AMD_${amd_lib}_LIBRARY
      NAMES ${amd_lib}
      HINTS ${AMD_LIBDIR}
      )
  endforeach()
else()
  if(AMD_DIR)
    foreach(amd_lib ${amd_libs})
      set(AMD_${amd_lib}_LIBRARY "AMD_${amd_lib}_LIBRARY-NOTFOUND")
      find_library(${amd_lib}_LIBRARY
        NAMES ${amd_lib}
        HINTS ${AMD_DIR}
        PATH_SUFFIXES lib lib32 lib64
        )
    endforeach()
  else()
    foreach(amd_lib ${amd_libs})
      set(AMD_${amd_lib}_LIBRARY "AMD_${amd_lib}_LIBRARY-NOTFOUND")
      find_library(AMD_${amd_lib}_LIBRARY
        NAMES ${amd_lib}
        HINTS ${_lib_env}
        )
    endforeach()
  endif()
endif()

set(AMD_LIBRARIES "")
foreach(amd_lib ${amd_libs})
  if (AMD_${amd_lib}_LIBRARY)
    get_filename_component(${amd_lib}_lib_path "${AMD_${amd_lib}_LIBRARY}" PATH)
    list(APPEND AMD_LIBRARIES "${AMD_${amd_lib}_LIBRARY}")
    list(APPEND AMD_LIBRARY_DIRS "${${amd_lib}_lib_path}")
  else ()
    list(APPEND AMD_LIBRARIES "${AMD_${amd_lib}_LIBRARY}")
    if (NOT AMD_FIND_QUIETLY)
      message(STATUS "Looking for AMD -- lib ${amd_lib} not found")
    endif()
  endif ()
  mark_as_advanced(AMD_${amd_lib}_LIBRARY)
endforeach()


# Finally, check for amd/colamd in the found libraries
if(AMD_LIBRARIES)

  set(REQUIRED_INCDIRS)
  set(REQUIRED_LIBDIRS)
  set(REQUIRED_LIBS)

  if (AMD_INCLUDE_DIRS)
    set(REQUIRED_INCDIRS  "${AMD_INCLUDE_DIRS}")
  endif()
  if (AMD_LIBRARY_DIRS)
    set(REQUIRED_LIBDIRS "${AMD_LIBRARY_DIRS}")
  endif()
  set(REQUIRED_LIBS "${AMD_LIBRARIES}")

  set(CMAKE_REQUIRED_INCLUDES "${AMD_INCLUDE_DIRS}")
  set(CMAKE_REQUIRED_LIBRARIES)
  list(APPEND CMAKE_REQUIRED_LIBRARIES "${AMD_LIBRARIES}")

  unset(AMD_WORKS CACHE)
  include(CheckFunctionExists)
  check_function_exists(amd_order AMD_WORKS)
  mark_as_advanced(AMD_WORKS)

  unset(COLAMD_WORKS CACHE)
  include(CheckFunctionExists)
  check_function_exists(colamd COLAMD_WORKS)
  mark_as_advanced(COLAMD_WORKS)

endif()



include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(AMD DEFAULT_MSG
  AMD_LIBRARIES
  AMD_INCLUDE_DIRS
  AMD_LIBRARY_DIRS
  AMD_WORKS
  COLAMD_WORKS)

# Add imported target
if (AMD_FOUND)
  morse_create_imported_target(AMD)
endif()
